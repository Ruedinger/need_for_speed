from pathlib import Path
from random import randint
from math import isclose
from collections import deque
import csv
from ..util.logging_config import logger
from ..util.interfaces import Observer, PeriodicTask
from typing import List, Tuple, Union, Callable, Iterable
from statistics import mean 
from ..util.parameters import data_dir, seconds_per_meter, mode, ApplicationMode
from ..util.matcher import Matcher
from .timer import Timer
from .model import Model
from .application_state import ApplicationState, first_state
from .notification import Reset, Pause, OutOfTime, ParticipantMoved, StateUpdate
from ..util.interfaces import Subject
from .instruction import Instruction
from ..util.constants import n_to_base
from .skeleton_kinematics import angle_between_joints

matcher = Matcher()

# Possible accuracy functions from related work:
# 1. mean accuracy over frames, only two states per frame, movement performed correct or error
# 2. same as 1., but with delta times 
# 3. use distance to the optimal trajectory

def class_based_accuracy_without_dt(measurements: Iterable[Tuple[List[float], int]]) -> Tuple[float, float]:
    """Implements method 1. described above"""
    if measurements:
        accuracies_times_dt_left = [1 if measurement[0][0] == 1 else 0 for measurement in measurements]
        accuracies_times_dt_right = [1 if measurement[0][1] == 1 else 0 for measurement in measurements]
        number_of_measurements = len(accuracies_times_dt_left)
        return sum(accuracies_times_dt_left)/number_of_measurements, sum(accuracies_times_dt_right)/number_of_measurements
    else:
        logger.critical("Measurements empty")
        return 0, 0

def class_based_accuracy_with_dt(measurements: Iterable[Tuple[List[float], int]]) -> Tuple[float, float]:
    """Implements method 2. described above"""
    if measurements:
        accuracies_times_dt_left = [measurement[1] if measurement[0][0] == 1 else 0 for measurement in measurements]
        accuracies_times_dt_right = [measurement[1] if measurement[0][1] == 1 else 0 for measurement in measurements]
        total_time = sum([measurement[1] for measurement in measurements])
        if isclose(total_time, 0):
            return 0, 0
        return sum(accuracies_times_dt_left)/total_time, sum(accuracies_times_dt_right)/total_time
    else:
        logger.critical("Measurements empty")
        return 0, 0

def distance_based_accuracy_with_dt(measurements: Iterable[Tuple[List[float], int]]) -> Tuple[float, float]:
    """Implements method 3. described above"""
    if measurements:
        accuracies_times_dt_left = [measurement[0][0]*measurement[1] for measurement in measurements]
        accuracies_times_dt_right = [measurement[0][1]*measurement[1] for measurement in measurements]
        total_time = sum([measurement[1] for measurement in measurements])
        if isclose(total_time, 0):
            return 0, 0
        return sum(accuracies_times_dt_left)/total_time, sum(accuracies_times_dt_right)/total_time
    else:
        logger.critical("Measurements empty")
        return 0, 0

def distance_based_accuracy_without_dt(measurements: Iterable[Tuple[List[float], int]]) -> Tuple[float, float]:
    """Completes the three identified options canonical."""
    if measurements:
        distances_left = [measurement[0][0] for measurement in measurements]
        distances_right = [measurement[0][1] for measurement in measurements]
        number_of_measurements = len(measurements)
        return sum(distances_left)/number_of_measurements, sum(distances_right)/number_of_measurements
    else:
        logger.critical("Measurements empty")
        return 0, 0

def accumulate_angles(angles: Iterable[float]) -> float:
    """Used to get differences of head and torso angles over multiple frames"""
    res = 0
    if angles:
        for i in range(len(angles)-1):
            res += abs(angles[i] - angles[i+1])
        return res
    else:
        logger.critical("angles empty when accumulating")
        return 0

class DataCollector(Observer, PeriodicTask):
    """Class for saving needed data to csv. Saves immediately after a repetition is done."""
    def __init__(
        self,
        movement_time_notification: OutOfTime,
        model: Model,
        directory_str: str
    ) -> None:
        self._model: Model = model
        # for notifying other classes when participants take too much time
        self._movement_time_notification = movement_time_notification
        # number of successful repetitions in the coordination test
        self.num_repetitions: int = 0
        # Iterable of head angles, one element per frame
        self._head_angles: Iterable[float] = deque()
        # Iterable of torso angles, one element per frame
        self._torso_angles: Iterable[float] = deque()
        # last frustration level as int, see Frustration enum
        self.frustration: int = 0
        # accuracies at this instance, cumulative over the whole movement
        self._accuracies: Iterable[Tuple[List[float], int]] = deque()
        # timer for updating the accuracy
        self._accuracy_timer: Timer = Timer(0)
        # id of the participant
        self._ident: int = randint(0, 999999)
        # index of the last seen instruction
        self._last_instruction_index: int = 0
        # needs to remember last state so saving when going to the next state is successful
        self._last_state: ApplicationState = first_state
        # selecting filenames to save to
        directory = Path(directory_str)
        if not directory.exists():
            directory.mkdir()
        coordination_filename = directory / "coordination.csv"
        condition_filename = directory / "condition.csv"
        questionnaire_filename = directory / "questionnaire.csv"
        # preparing files
        self.coordination_file = open(coordination_filename, 'a', newline='')
        coordination_fields = ['id', 'movement', 'time', 'accuracy', 'head bend', 'torso bend', 'completions']
        self.coordination_writer = csv.DictWriter(self.coordination_file, fieldnames=coordination_fields)
        self.coordination_writer.writeheader()

        self.condition_file = open(condition_filename, 'a', newline='')
        condition_fields = ['id', 'movement', 'latency', 'difficulty', 'time', 'accuracy']
        self.condition_writer = csv.DictWriter(self.condition_file, fieldnames=condition_fields)
        self.condition_writer.writeheader()

        self.questionnaire_file = open(questionnaire_filename, 'a', newline='')
        questionnaire_fields = ['id', 'frustration']
        self.questionnaire_writer = csv.DictWriter(self.questionnaire_file, fieldnames=questionnaire_fields)
        self.questionnaire_writer.writeheader()

    @property
    def time_in_movement(self) -> float:
        """Returns the time since the start of the movement in nano seconds."""
        delta_times = [measurement[1] for measurement in self._accuracies]
        return sum(delta_times)

    @property
    def accuracies(self) -> Tuple[float, float]:
        """Returns the accuracy of the last frame of both hands."""
        if self._accuracies:
            return distance_based_accuracy_with_dt(self._accuracies)
        else:
            return 1, 1

    @accuracies.setter
    def accuracies(self, new_accuracies: List[float]) -> None:
        """Adds new accuracy values to list of all accuracies for the current movement."""
        delta_time = self._accuracy_timer.passed_time
        self._accuracies.append((new_accuracies, delta_time))
        self._accuracy_timer.start()
    
    def save_coordination_data(self, accuracy_function: Callable):
        """For saving measurements from the coordination test."""
        logger.info("saving coordination data")
        self._accuracy_timer.stop()
        head_bend = accumulate_angles(self._head_angles)
        torso_bend = accumulate_angles(self._torso_angles)

        self.coordination_writer.writerow({'id': self._ident, 'movement': self._last_instruction_index, 'time': self.time_in_movement*n_to_base, 'accuracy': mean(accuracy_function(self._accuracies)), 'head bend': head_bend, 'torso bend': torso_bend, 'completions': self.num_repetitions})
        self.coordination_file.flush()
        self._accuracies = deque()
        self.num_repetitions = 0

    def save_condition_data(self, accuracy_function: Callable) -> None:
        """For saving measurements from one movement in the condition phase."""
        logger.info("saving condition data")
        self._accuracy_timer.stop()
        self.condition_writer.writerow({'id': self._ident, 'movement': self._last_instruction_index, 'latency': self._last_instruction.latency, 'difficulty': self._last_instruction.difficulty, 'time': self.time_in_movement*n_to_base, 'accuracy': mean(accuracy_function(self._accuracies))})
        self.condition_file.flush()
        self._accuracies = deque()

    def save_questionnaire_data(self) -> None:
        """For saving questionnaire data."""
        logger.info("saving questionnaire data")
        self.questionnaire_writer.writerow({'id': self._ident, 'frustration': self.frustration})
        self.questionnaire_file.flush()

    @matcher.base_case
    def update(self, subject: Subject) -> None:
        """Base update method for notifications."""
        raise ValueError(f"Expected subject of type StateUpdate, ParticipantMoved, Reset or Pause. Got {type(subject)}")
    
    def process(self) -> None:
        """Manages head and torso angle lists. Updates instance state when needed."""
        if self._model.state == ApplicationState.COORDINATION_TEST:
            keypoints = self._model.keypoints
            self._head_angles.append(angle_between_joints(keypoints, "pelvis", "neck", "head"))
            self._torso_angles.append(angle_between_joints(keypoints, "left_hip", "pelvis", "neck"))

        if self._model.state in [ApplicationState.COORDINATION_TEST, ApplicationState.CONDITION]:
            self._last_instruction_index = self._model.instruction_index
            self._last_instruction: Instruction = self._model.current_instruction

        time_in_movement = self.time_in_movement*n_to_base
        if self._model.state == ApplicationState.CONDITION and time_in_movement > self._model.real_world_movement_size*seconds_per_meter:
            self._movement_time_notification.badness = min(max(0, time_in_movement/(self._model.real_world_movement_size*seconds_per_meter) - 1), 1)
            self._movement_time_notification.notify()

    @matcher.case(lambda self, subject: isinstance(subject, Reset))
    def reset_update(self, subject: Reset) -> None:
        """Resets the state."""
        self.num_repetitions: int = 0
        self._head_angles = deque()
        self._torso_angles = deque()
        self.frustration = 0
        self._accuracies = deque()
        self._accuracy_timer.stop()
        self._ident = randint(0, 999999)
        self._last_instruction_index: int = 0
        self._last_state: ApplicationState = first_state

    @matcher.case(lambda self, subject: isinstance(subject, Pause))
    def pause_update(self, subject: Pause) -> None:
        """Pauses time-related measurement components."""
        if subject.paused:
            if self._last_state in [ApplicationState.COORDINATION_TEST, ApplicationState.CONDITION]:
                self._accuracy_timer.stop()
        else:
            if self._last_state in [ApplicationState.COORDINATION_TEST, ApplicationState.CONDITION]:
                self._accuracy_timer.start()

    @matcher.case(lambda self, state: isinstance(state, StateUpdate))
    def state_update(self, state: StateUpdate) -> None:
        """Saves data if necessary i.e. if there is data to be saved from the last instruction."""
        self._movement_time_notification.badness = 0
        self._movement_time_notification.notify()

        match self._last_state:
            case ApplicationState.CALIBRATION:
                if self._model.state == ApplicationState.COORDINATION_TEST:
                    self._accuracy_timer.start()
            case ApplicationState.COORDINATION_TEST:
                self.save_coordination_data(distance_based_accuracy_with_dt)
            case ApplicationState.CONDITION:
                self.save_condition_data(distance_based_accuracy_with_dt)
            case ApplicationState.QUESTIONNAIRE:
                self.save_questionnaire_data()
        
        self._last_state = self._model.state

    @matcher.case(lambda self, subject: isinstance(subject, ParticipantMoved))
    def participant_update(self, subject: ParticipantMoved) -> None:
        """For timing the time taken to perform a movement correctly."""
        self._accuracy_timer.start()

    def __str__(self) -> str:
        return "data_collector"