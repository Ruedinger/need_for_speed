# This file contain utility functions for computing angles, distances for the custom skeleton
from math import acos, pi
from statistics import mean
from typing import List, Tuple
from pyglet.math import Vec2
from ..logic.instruction import MovementType

# map joint names to indices
keypoints_to_indices = {
    "head": 0,
    "left_shoulder": 2,
    "right_shoulder": 3,
    "neck": 1,
    "left_elbow": 4,
    "right_elbow": 5,
    "left_hand": 6,
    "right_hand": 7,
    "left_hip": 9,
    "right_hip": 10,
    "pelvis": 8,
    "left_knee": 11,
    "right_knee": 12,
    "left_ankle": 13,
    "right_ankle": 14,
}

num_of_keypoints = len(keypoints_to_indices)

def is_angle_in_range(angle: float, start_angle: float, stop_angle: float) -> bool:
    """Tests whether angle is between start_angle and stop_angle. Only works for progressing angles, but with modulo (2π == 0)."""
    # Normalize angles to the range [0, 2π)
    angle = angle % (2 * pi)
    start_angle = start_angle % (2 * pi)
    stop_angle = stop_angle % (2 * pi)
    
    if start_angle <= stop_angle:
        # Standard interval without wrap-around
        return start_angle <= angle <= stop_angle
    else:
        # Interval with wrap-around
        return angle >= start_angle or angle <= stop_angle

def shoulder_between_angles(keypoints: List[Vec2], shoulder: str, start_angle: float, end_angle: float) -> bool:
    """
    Test whether the angle of the specified shoulder is between start_angel and end_angle.
    """
    assert start_angle <= end_angle, "start_angle must not be bigger than end_angle"
    if shoulder == "left_shoulder":
        shoulder_angle = angle_between_joints(keypoints, "left_hip", "left_shoulder", "left_elbow")
        elbow_angle = angle_between_joints(keypoints, "left_shoulder", "left_elbow", "left_hand")
        return is_angle_in_range(shoulder_angle, start_angle, end_angle) and is_angle_in_range(elbow_angle, pi/2, pi)
    elif shoulder == "right_shoulder":
        shoulder_angle = angle_between_joints(keypoints, "right_hip", "right_shoulder", "right_elbow")
        elbow_angle = angle_between_joints(keypoints, "right_shoulder", "right_elbow", "right_hand")
        return is_angle_in_range(shoulder_angle, start_angle, end_angle) and is_angle_in_range(elbow_angle, pi/2, pi)
    else:
        raise ValueError(f"Invalid value for computing parameter shoulder: {shoulder}")
    
def angle_between_joints(keypoints: List[Vec2], joint1: str, joint2: str, joint3: str) -> float:
    """
    Returns the angle between the keypoints specified by joint1, joint2, joint3.
    """
    index1 = keypoints_to_indices[joint1]
    index2 = keypoints_to_indices[joint2]
    index3 = keypoints_to_indices[joint3]
    v1: Vec2 = keypoints[index2] - keypoints[index1]
    v2: Vec2 = keypoints[index2] - keypoints[index3]
    if abs(v1)*abs(v2) > 0:
        return acos(v1.dot(v2)/(abs(v1)*abs(v2)))
    else:
        raise ValueError("Invalid skeleton for computing angle")

def get_body_half(keypoints: List[Vec2]) -> float:
    """Computes half of body in x-direction."""
    return mean([keypoints[keypoints_to_indices["right_shoulder"]].x, keypoints[keypoints_to_indices["left_shoulder"]].x, keypoints[keypoints_to_indices["right_hip"]].x, keypoints[keypoints_to_indices["left_hip"]].x])

def get_movement_position(
    keypoints: List[Vec2], 
    movement_type: MovementType, 
    body_side: int, 
    horizontal_size: int, 
    vertical_size: int, 
    diagonal_size: int
) -> Tuple[Vec2, Vec2]:
    """Computes start and end position for a given movement on a given skeleton."""
    # 0 represents left body half
    # 1 represents right body half
    assert body_side in [0, 1]
    left_shoulder: Vec2 = keypoints[keypoints_to_indices["left_shoulder"]]
    right_shoulder: Vec2 = keypoints[keypoints_to_indices["right_shoulder"]]
    body_half: float = get_body_half(keypoints)
    match movement_type:
        case MovementType.ZIGZAG:
            if body_side == 0:
                bottom_left = left_shoulder - Vec2(1, 1)*diagonal_size
                top_right = Vec2(body_half, left_shoulder.y + diagonal_size)
            else:
                bottom_left = Vec2(body_half, right_shoulder.y - diagonal_size)
                top_right = right_shoulder + Vec2(1, 1)*diagonal_size
        case MovementType.ELLIPSE:
            if body_side == 0:
                bottom_left = left_shoulder - Vec2(1, 1)*diagonal_size
                top_right = Vec2(body_half, left_shoulder.y + diagonal_size)
            else:
                bottom_left = Vec2(body_half, right_shoulder.y - diagonal_size)
                top_right = right_shoulder + Vec2(1, 1)*diagonal_size
        case MovementType.HORIZONTAL_LINE:
            if body_side == 0:
                bottom_left = left_shoulder - Vec2(horizontal_size, 0)
                top_right = Vec2(body_half, left_shoulder.y)
            else:
                bottom_left = Vec2(body_half, right_shoulder.y)
                top_right = right_shoulder + Vec2(horizontal_size, 0)
        case MovementType.VERTICAL_LINE:
            if body_side == 0:
                x_pos = mean([body_half, left_shoulder.x - horizontal_size])
                bottom_left = Vec2(x_pos, left_shoulder.y - vertical_size)
                top_right = Vec2(x_pos, left_shoulder.y + vertical_size)
            else:
                x_pos = mean([body_half, right_shoulder.x + horizontal_size])
                bottom_left = Vec2(x_pos, right_shoulder.y - vertical_size)
                top_right = Vec2(x_pos, right_shoulder.y + vertical_size)
        case _:
            raise ValueError(f"Invalid movement type for positioning: {movement_type}")
    return bottom_left, top_right
    

def get_preparation_position(
    keypoints: List[Vec2], 
    movement_type: MovementType, 
    body_side: int, 
    horizontal_size: int, 
    vertical_size: int, 
    diagonal_size: int, 
    mirrored: bool
) -> Vec2:
    """Computes the position of movement preparations."""
    assert body_side in [0, 1]
    left_shoulder: Vec2 = keypoints[keypoints_to_indices["left_shoulder"]]
    right_shoulder: Vec2 = keypoints[keypoints_to_indices["right_shoulder"]]
    body_half: float = get_body_half(keypoints)
    match movement_type:
        case MovementType.ZIGZAG:
            if body_side == 0:
                if mirrored:
                    preparation_position = Vec2(body_half, left_shoulder.y - diagonal_size)
                else:
                    preparation_position = left_shoulder - Vec2(1, 1)*diagonal_size
            else:
                if mirrored:
                    preparation_position = right_shoulder + Vec2(1, -1)*diagonal_size
                else:
                    preparation_position = Vec2(body_half, right_shoulder.y - diagonal_size)
        case MovementType.ELLIPSE:
            if body_side == 0:
                x_pos = mean([body_half, left_shoulder.x - diagonal_size])
                preparation_position = Vec2(x_pos, left_shoulder.y - diagonal_size)
            else:
                x_pos = mean([body_half, right_shoulder.x + diagonal_size])
                preparation_position = Vec2(x_pos, right_shoulder.y - diagonal_size)
        case MovementType.HORIZONTAL_LINE:
            if body_side == 0:
                if mirrored:
                    preparation_position = Vec2(body_half, left_shoulder.y)
                else:
                    preparation_position = left_shoulder - Vec2(horizontal_size, 0)
            else:
                if mirrored:
                    preparation_position = right_shoulder + Vec2(horizontal_size, 0)
                else:
                    preparation_position = Vec2(body_half, right_shoulder.y)
        case MovementType.VERTICAL_LINE:
            if body_side == 0:
                x_pos = mean([body_half, left_shoulder.x - horizontal_size])
                preparation_position = Vec2(x_pos, left_shoulder.y - vertical_size)
            else:
                x_pos = mean([body_half, right_shoulder.x + horizontal_size])
                preparation_position = Vec2(x_pos, right_shoulder.y - vertical_size)
        case _:
            raise ValueError(f"Invalid movement type for positioning: {movement_type}")
    return preparation_position