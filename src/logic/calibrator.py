from collections import deque
from math import sqrt, pi, sin, cos
from statistics import fmean
from typing import List, Callable, Iterable
from pyglet.math import Vec2
import numpy as np
from .instruction import Instruction, Calibration
from ..util.logging_config import logger
from ..util.interfaces import Subject, Observer, PeriodicTask
from .model import Model
from .application_state import ApplicationState
from .notification import Reset, Pause, StateUpdate
from .timer import Timer
from ..util.matcher import Matcher
from ..util.parameters import movement_size_m, movement_width_m, mode, ApplicationMode
from .skeleton_kinematics import keypoints_to_indices

left_shoulder = keypoints_to_indices["left_shoulder"]
right_shoulder = keypoints_to_indices["right_shoulder"]
neck = keypoints_to_indices["neck"]
pelvis = keypoints_to_indices["pelvis"]

matcher = Matcher()

class Calibrator(Observer, PeriodicTask):
    """
    For calibrating the display according to the distance of the participant to the camera.
    Assuming the display is positioned in the same depth as the camera.
    """
    def __init__(
        self,
        model: Model
    ) -> None:
        self._model: Model = model
        # list of wait times in ns, used for calibration
        self._wait_times: Iterable[int] = deque()
        # index to wait_times
        self._wait_times_index: int = 0
        # for estimating passed time since measurement initialization
        self._measurement_timer: Timer = Timer(0)
        # here the results of calibration are saved
        # format: width, height
        self._intermediate_results: np.array = np.zeros((0,2), dtype=float)
        self._world_intermediate_results: np.array = np.zeros((0,2), dtype=float)
        # factors for translating real to virtual sizes
        self._dist_real_dist_virtual_x: float = None
        self._dist_real_dist_virtual_y: float = None
        # movement sizes computed from calibrations
        self._horizontal_movement_size_px: int = None
        self._vertical_movement_size_px: int = None
        self._diagonal_movement_size_px: int = None
        self._movement_width: int = None

    def measure(self, keypoints: List[Vec2], world_keypoints: np.array) -> None:
        """Takes one measurement for calibrating the system."""
        logger.info(f"conducting measurement {self._wait_times_index+1}/{len(self._wait_times)}")
        self._measurement_results[4*self._wait_times_index] = np.array(keypoints[left_shoulder], dtype=float) 
        self._measurement_results[4*self._wait_times_index+1] = np.array(keypoints[right_shoulder], dtype=float)
        self._measurement_results[4*self._wait_times_index+2] = np.array(keypoints[neck], dtype=float) 
        self._measurement_results[4*self._wait_times_index+3] = np.array(keypoints[pelvis], dtype=float)
        self._world_measurement_results[4*self._wait_times_index] = world_keypoints[left_shoulder][:-1]
        self._world_measurement_results[4*self._wait_times_index+1] = world_keypoints[right_shoulder][:-1]
        self._world_measurement_results[4*self._wait_times_index+2] = world_keypoints[neck][:-1]
        self._world_measurement_results[4*self._wait_times_index+3] = world_keypoints[pelvis][:-1]

    def init_measurement(self, instruction: Instruction):
        """Starts a timer and prepares variables for conducting the next len(wait_time) measurements."""
        wait_times = instruction.wait_times
        if wait_times:
            logger.info("initializing measurement")
            self._wait_times = deque(wait_times)
            self._wait_times_index = 0
            self._measurement_timer.run_time = self._wait_times[self._wait_times_index]
            self._measurement_timer.start()
            # measurement results for the calibration
            # in image space
            self._measurement_results = np.zeros((len(wait_times)*4, 2), dtype=float)
            # in world space
            self._world_measurement_results = np.zeros((len(wait_times)*4, 2), dtype=float)

    def calibrate(self, aggregation: Callable) -> None:
        """Aggregates the measurement results to estimate a ratio of pixels to meter."""
        logger.info("Calibrating")
        aggregated_left_shoulder = aggregation(self._intermediate_results[::4], axis=0)
        aggregated_right_shoulder = aggregation(self._intermediate_results[1::4], axis=0)
        aggregated_neck = aggregation(self._intermediate_results[2::4], axis=0)
        aggregated_pelvis = aggregation(self._intermediate_results[3::4], axis=0)

        aggregated_world_left_shoulder = aggregation(self._world_intermediate_results[::4], axis=0)
        aggregated_world_right_shoulder = aggregation(self._world_intermediate_results[1::4], axis=0)
        aggregated_world_neck = aggregation(self._world_intermediate_results[2::4], axis=0)
        aggregated_world_pelvis = aggregation(self._world_intermediate_results[3::4], axis=0)

        self._dist_real_dist_virtual_x = np.linalg.norm(aggregated_left_shoulder - aggregated_right_shoulder)/np.linalg.norm(aggregated_world_left_shoulder - aggregated_world_right_shoulder)
        self._dist_real_dist_virtual_y = np.linalg.norm(aggregated_neck - aggregated_pelvis)/np.linalg.norm(aggregated_world_neck - aggregated_world_pelvis)

        self._horizontal_movement_size_px = int(self._dist_real_dist_virtual_x*movement_size_m)
        self._vertical_movement_size_px = int(self._dist_real_dist_virtual_y*movement_size_m)

        # diagonal movements are at a pi/4 angle between horizontal and vertical movements
        # use ellipse equation to correct for non-square pixels
        k = self._horizontal_movement_size_px*self._vertical_movement_size_px/sqrt((self._vertical_movement_size_px*cos(pi/4))**2 + (self._horizontal_movement_size_px*sin(pi/4))**2)
        x = k*cos(pi/4)
        y = k*sin(pi/4)
        self._diagonal_movement_size_px = int(abs(Vec2(x, y)))
        # defines width of the instructions
        self._movement_width = int(fmean([self._dist_real_dist_virtual_x*movement_width_m , self._dist_real_dist_virtual_y*movement_width_m]))
        logger.info(f"Calibrated horizontal movement size in px: {self._horizontal_movement_size_px}\nVertical movement size in px: {self._vertical_movement_size_px}\nDiagonal movement size in px: {self._diagonal_movement_size_px}\nMovement width: {self._movement_width}")

    @matcher.base_case
    def update(self, subject: Subject) -> None:
        """Base update method for notifications."""
        raise ValueError(f"Expected subject of type StateUpdate, Reset or Pause. Got {type(subject)}")

    def process(self) -> None:
        """Takes measurements and pre-processes measurement results."""
        instruction: Calibration = self._model.current_instruction
        
        if self._measurement_timer.done:
            self.measure(self._model.keypoints, self._model.world_keypoints)
            if self._wait_times_index < len(self._wait_times) - 1:
                self._wait_times_index += 1 
                self._measurement_timer.run_time = self._wait_times[self._wait_times_index]
                self._measurement_timer.start()
            else:
                if (self._measurement_results != 0.0).all:
                    aggregation = instruction.aggregation
                    tmp = np.zeros((4, 2), dtype=float)
                    tmp[0] = aggregation(self._measurement_results[::4], axis=0)
                    tmp[1] = aggregation(self._measurement_results[1::4], axis=0)
                    tmp[2] = aggregation(self._measurement_results[2::4], axis=0)
                    tmp[3] = aggregation(self._measurement_results[3::4], axis=0)
                    self._intermediate_results = np.append(self._intermediate_results, tmp, axis=0)
                    tmp = np.zeros((4, 2), dtype=float)
                    tmp[0] = aggregation(self._world_measurement_results[::4], axis=0)
                    tmp[1] = aggregation(self._world_measurement_results[1::4], axis=0)
                    tmp[2] = aggregation(self._world_measurement_results[2::4], axis=0)
                    tmp[3] = aggregation(self._world_measurement_results[3::4], axis=0)
                    self._world_intermediate_results = np.append(self._world_intermediate_results, tmp, axis=0)
                            
                    if instruction.last_calibration:
                        self.calibrate(aggregation)
                        self._intermediate_results = []
                        self._world_intermediate_results = []

                self._measurement_timer.stop()

    @matcher.case(lambda self, subject: isinstance(subject, Pause))
    def pause_update(self, subject: Pause) -> None:
        """For adjusting timers when the application is paused during the calibration phase."""
        if subject.paused and self._measurement_timer.is_running:
            self._measurement_timer.run_time -= self._measurement_timer.passed_time
            self._measurement_timer.stop()
        elif self._wait_times_index < len(self._wait_times) - 1:
            self._measurement_timer.start()

    @matcher.case(lambda self, subject: isinstance(subject, Reset))
    def reset_update(self, subject: Reset) -> None:
        """For resetting variables in case of a reset event."""
        self._wait_times = deque()
        self._wait_times_index = 0
        self._measurement_timer.stop()
        self._measurement_results = 0
        self._world_measurement_results = 0
        self._intermediate_results: np.array = np.zeros((0,2), dtype=float)
        self._world_intermediate_results: np.array = np.zeros((0,2), dtype=float)
        self._dist_real_dist_virtual_x: float = None
        self._dist_real_dist_virtual_y: float = None
        self._horizontal_movement_size_px: int = None
        self._vertical_movement_size_px: int = None
        self._diagonal_movement_size_px: int = None
        self._movement_width: int = None

    @matcher.case(lambda self, state: isinstance(state, StateUpdate))
    def state_update(self, state: StateUpdate) -> None:
        """For activating the calibrator when the model enters calibration phase"""
        if self._model.state == ApplicationState.CALIBRATION:
            self.init_measurement(self._model.current_instruction)

    def real_world_size(self, virtual_vector: Vec2) -> float:
        """For calculating real world sizes based on vectors in pixel 2d space."""
        real_vector = Vec2(self._dist_real_dist_virtual_x**-1*virtual_vector.x, self._dist_real_dist_virtual_y**-1*virtual_vector.y)
        return abs(real_vector)

    @property
    def horizontal_movement_size_px(self) -> int:
        """Maximal horizontal movement size in pixels"""
        return self._horizontal_movement_size_px

    @property
    def vertical_movement_size_px(self) -> int:
        """Maximal vertical movement size in pixels"""
        return self._vertical_movement_size_px

    @property
    def diagonal_movement_size_px(self) -> int:
        """Maximal diagonal movement size in pixels"""
        return self._diagonal_movement_size_px

    @property
    def movement_width(self) -> int:
        """Width of movements in pixels"""
        return self._movement_width