# inspired by https://github.com/PyImageSearch/imutils/blob/master/imutils/video/webcamvideostream.py
# the original file does not handle threads correctly, which results in an exception when exiting the program
# I should mention that the library was not updates for 6 years, so it probably worked at some point in time
from threading import Thread
from typing import Tuple, Union
import cv2
from numpy import array
from ..util.logging_config import logger

class WebcamVideoStream:
    """A class for reading video frames concurrently."""
    def __init__(
        self, 
        source: Union[str, int], 
        frame_width: int,
        frame_height: int,
        frame_rate: float,
        ) -> None:
        # initialize the video camera stream and set resolution and fps
        self._stream = cv2.VideoCapture(source)
        self._stream.set(cv2.CAP_PROP_FRAME_WIDTH, frame_width)
        self._stream.set(cv2.CAP_PROP_FRAME_HEIGHT, frame_height)
        self._stream.set(cv2.CAP_PROP_FPS, frame_rate)
        logger.info("Initializing webcam")
        logger.info(f"Video resolution: {self.width} x {self.height}")
        logger.info(f"Frame rate: {self.fps}")

        # read the first frame from the stream
        self._grabbed, self._frame = self._stream.read()
        self._last_frame_time: int = 0

        # initialize the variable used to indicate if the thread should be stopped
        self._stopped = False
        self._thread: Thread = None

    def start(self) -> None:
        """Start a daemon to read frames from the video stream."""
        self._thread = Thread(target=self.update, args=())
        self._thread.daemon = True
        self._thread.start()

    def update(self) -> None:
        """Continuously read frames from source."""
        while True:
            # if the thread indicator variable is set, stop the thread
            if self._stopped:
                return

            # otherwise, read the next frame from the stream
            self._grabbed, self._frame = self._stream.read()

    def stop(self) -> None:
        """Stops WebcamVideoStream."""
        self._stopped = True
        self._thread.join()

    def read(self) -> Tuple[bool, array, int]:
        """Returns the most recently frame."""
        time = int(self._stream.get(cv2.CAP_PROP_POS_MSEC))
        last_time = self._last_frame_time
        self._last_frame_time = time
        return self._grabbed and last_time != time, self._frame, time

    @property
    def height(self) -> int:
        """Return the height of the video stream."""
        return int(self._stream.get(cv2.CAP_PROP_FRAME_HEIGHT))

    @property
    def width(self) -> int:
        """Return the width of the video stream."""
        return int(self._stream.get(cv2.CAP_PROP_FRAME_WIDTH))

    @property
    def fps(self) -> float:
        """Returns the frame rate of the video stream."""
        return self._stream.get(cv2.CAP_PROP_FPS)