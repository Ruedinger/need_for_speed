# this file hosts the classes responsible for controlling the movements the participants have to perform
import random
import pickle
from itertools import product
from enum import Enum, IntEnum
from dataclasses import dataclass
from typing import List, Tuple, Callable, Iterable
import numpy as np
from src.util.constants import base_to_n

# define latency levels in ms
latencies: List[int] = [0, 40, 90, 140]
# number of movements per condition
num_movements = 5
# how often each condition is tested
num_repetitions = 2

class Difficulty(IntEnum):
    """Indicates the difficulty of an instruction. If both movements of the instruction do the same, is is a simple instruction. Otherwise it is a complex instruction."""
    SIMPLE = 0
    COMPLEX = 1

# expertise is not needed here, as this variable is determined in analyses

class MovementType(IntEnum):
    """Defines all groups of movements. Note that some movements can also be mirrored."""
    ZIGZAG = 0
    ELLIPSE = 1
    HORIZONTAL_LINE = 2
    VERTICAL_LINE = 3
    DOWN = 4
    SIDE = 5
    UP = 6

@dataclass
class Instruction:
    """Gives the participant the instruction to make a movement for a given time. Time dependence can be deactivated."""
    _movements: Tuple[MovementType, MovementType] # first element is the movement of the left hand, second element is the movement of the right hand. All further elements will be discarded.
    _timer: float # time in nano seconds for which this instruction has to be executed, -1 if no timer is set
    _mirrored: bool # whether the movement should be mirrored. Only has an effect for ZIGZAG, ELLIPSE, HORIZONTAL_LINE, and VERTICAL_LINE

    @property
    def movements(self) -> Tuple[MovementType, MovementType]:
        """Returns the movements of this instructions. First element for the left movement, second element for the right movement. All further elements will be discarded."""
        return self._movements

    @property
    def timer(self) -> float:
        """Returns time in nano seconds for which this instruction has to be executed, -1 if no timer is set"""
        return self._timer

    @property
    def mirrored(self) -> bool:
        """Returns whether the movements in this instruction should be mirrored."""
        return self._mirrored

@dataclass
class Condition(Instruction):
    """Identifies an individual condition including which movements are to be executed and the independent variables."""
    _latency: int # latency in ns
    _difficulty: Difficulty
    
    @property
    def latency(self) -> int:
        """Latency getter."""
        return self._latency

    @property
    def difficulty(self) -> Difficulty:
        """Difficulty getter."""
        return self._difficulty

@dataclass
class Calibration(Instruction):
    """Holds information needed to calibrate the software."""
    _wait_times: Iterable[int]  # in nano seconds
    _aggregation: Callable
    last_calibration: bool = False

    @property
    def wait_times(self) -> Iterable[int]:
        """Returns with which intervals measurements are performed in nano seconds."""
        return self._wait_times

    @property
    def aggregation(self) -> Callable:
        """Return the aggregation method of this calibration."""
        return self._aggregation

def generate_familiarizations() -> List[Instruction]:
    """For generating the familiarization instructions."""
    instruction_sequence: List[Instruction] = []
    for _ in range(2):
        instruction_sequence.append(Instruction([MovementType.VERTICAL_LINE, MovementType.VERTICAL_LINE], _timer=-1, _mirrored=False))
        instruction_sequence.append(Instruction([MovementType.HORIZONTAL_LINE, MovementType.HORIZONTAL_LINE], _timer=-1, _mirrored=False))
        instruction_sequence.append(Instruction([MovementType.ELLIPSE, MovementType.ELLIPSE], _timer=-1, _mirrored=False))
        instruction_sequence.append(Instruction([MovementType.ZIGZAG, MovementType.ZIGZAG], _timer=-1, _mirrored=False))
    return instruction_sequence

def generate_calibrations() -> List[Instruction]:
    """For generating the calibration instructions. Needed to calibrate the system."""
    instruction_sequence: List[Instruction] = []
    instruction_sequence.append(Calibration(
        _movements=(MovementType.SIDE, MovementType.SIDE), 
        _timer=-1, 
        _wait_times=[], 
        _aggregation=np.mean, 
        _mirrored=False
        ))
    instruction_sequence.append(Calibration(
        _movements=(MovementType.DOWN, MovementType.DOWN), 
        _timer=-1, 
        _wait_times=[], 
        _aggregation=np.mean, 
        _mirrored=False
        ))
    instruction_sequence.append(Calibration(
        _movements=(MovementType.DOWN, MovementType.DOWN), 
        _timer=5*base_to_n, 
        _wait_times=[0.4*base_to_n]*10, 
        _aggregation=np.mean, 
        _mirrored=False
        ))
    instruction_sequence[-1].last_calibration = True
    return instruction_sequence

def generate_coordination_tests() -> List[Instruction]:
    """For generating the motor coordination test instructions."""
    instruction_sequence: List[Instruction] = []
    instruction_sequence.append(Instruction([MovementType.VERTICAL_LINE, MovementType.HORIZONTAL_LINE], _timer=30*base_to_n, _mirrored=False))
    instruction_sequence.append(Instruction([MovementType.HORIZONTAL_LINE, MovementType.VERTICAL_LINE], _timer=30*base_to_n, _mirrored=False))
    return instruction_sequence

def generate_conditions() -> List[Instruction]:
    """For generating experimental condition instructions."""
    instruction_sequence: List[Instruction] = []
    test_movements = [MovementType.HORIZONTAL_LINE, MovementType.VERTICAL_LINE, MovementType.ZIGZAG, MovementType.ELLIPSE]
    condition_candidates = list(product(latencies, set(Difficulty)))*num_repetitions
    random.shuffle(condition_candidates)

    for condition in condition_candidates:
        for _ in range(num_movements):
            movements: List[MovementType] = [MovementType.HORIZONTAL_LINE, MovementType.HORIZONTAL_LINE]
            if condition[1] == Difficulty.SIMPLE:
                movement = random.choice(test_movements)
                instruction_sequence.append(Condition(_movements=(movement, movement), _latency=condition[0], _difficulty=condition[1], _mirrored=bool(random.getrandbits(1)), _timer=-1))
            else:
                while True:
                    movements[0] = random.choice(test_movements)
                    movements[1] = random.choice(test_movements)
                    if movements[0] != movements[1]:
                        break
                instruction_sequence.append(Condition(_movements=(movements[0], movements[1]), _latency=condition[0], _difficulty=condition[1], _mirrored=bool(random.getrandbits(1)), _timer=-1))
    return instruction_sequence

if __name__ == "__main__":
    # Movements are generated before executing the main application.
    # Movements are then loaded when needed by the main application.
    with open('instructions/familiarize', 'wb') as fd:
        pickle.dump(generate_familiarizations(), fd)
    with open('instructions/calibration', 'wb') as fd:
        pickle.dump(generate_calibrations(), fd)
    with open('instructions/coordination_test', 'wb') as fd:
        pickle.dump(generate_coordination_tests(), fd)
    with open('instructions/condition', 'wb') as fd:
        pickle.dump(generate_conditions(), fd)