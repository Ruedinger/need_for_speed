from collections import deque
from cProfile import Profile
from itertools import chain
from typing import Set, Dict, Iterable
from pyglet.clock import schedule_interval_soft, unschedule
from ..util.interfaces import PeriodicTask
from .application_state import ApplicationState, first_state
from ..util.parameters import maximal_frame_rate

class TaskScheduler:
    """Schedules PeriodicTasks according to the current state and registered PeriodicTasks."""
    def __init__(
        self
    ) -> None:
        self._previous_state: ApplicationState = first_state
        # for each model state there is a set of tasks to notify in that state
        # the idea is that it is unnecessary to update all tasks in every state
        self._periodic_tasks: Iterable[Set[PeriodicTask]] = deque()
        for state in ApplicationState:
            self._periodic_tasks.append(set())

    def schedule_tasks(self, state: ApplicationState) -> None:
        """Update which tasks are scheduled during the next state."""
        if state != self._previous_state:
            
            previous_tasks = self._periodic_tasks[self._previous_state]
            current_tasks = self._periodic_tasks[state]
            for task in previous_tasks.difference(current_tasks):
                unschedule(task.dt_update)
            for task in current_tasks.difference(previous_tasks):
                schedule_interval_soft(task.dt_update, 1/maximal_frame_rate)

            self._previous_state = state

    @property
    def task_execution_times(self) -> Dict[str, Iterable[int]]:
        """Returns delta times of the process function from all registered PeriodicTasks."""
        res = {}
        for task in chain.from_iterable(self._periodic_tasks):
            res[type(task).__name__] = task.execution_times
        return res

    @property
    def task_profiles(self) -> Dict[str, Profile]:
        """Returns profiles of the process function from all registered PeriodicTasks."""
        res = {}
        for task in chain.from_iterable(self._periodic_tasks):
            res[type(task).__name__] = task.profile
        return res

    def add_task(self, task: PeriodicTask, states: Iterable[ApplicationState]) -> None:
        """Registers new PeriodicTask."""
        for state in states:
            self._periodic_tasks[state].add(task)
        if self._previous_state in states:
            schedule_interval_soft(task.dt_update, 1/maximal_frame_rate)

    def remove_task(self, task: PeriodicTask, states: Iterable[ApplicationState]) -> None:
        """Removed PeriodicTask from states."""
        for state in states:
            self._periodic_tasks[state].remove(task)
        if self._previous_state in states:
            unschedule(task.dt_update)