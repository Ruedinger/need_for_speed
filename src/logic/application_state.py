from enum import IntEnum

class ApplicationState(IntEnum):
    """
    The possible applications states.
    Each state represents a part of the experiment.
    """
    CALIBRATION = 0
    FAMILIARIZE = 1
    COORDINATION_TEST = 2
    CONDITION = 3
    CLOSING = 4
    PREPARATION = 5
    QUESTIONNAIRE = 6

first_state = ApplicationState.CALIBRATION
instruction_states = [ApplicationState.CALIBRATION, ApplicationState.FAMILIARIZE, ApplicationState.COORDINATION_TEST, ApplicationState.CONDITION]