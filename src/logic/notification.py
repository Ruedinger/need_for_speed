# This file specifies notifications as subjects of the observer
# There may be other subjects, but the functional form of the notification has been established as a good baseline
from overrides import override
from ..util.interfaces import Subject, Observer
from ..util.logging_config import logger

class Notification(Subject):
    """General implementation of a notification."""
    def __init__(self) -> None:
        self._observers = []

    @override
    def attach(self, observer: Observer) -> None:
        self._observers.append(observer)

    @override
    def detach(self, observer: Observer) -> None:
        self._observers.remove(observer)

    @override
    def notify(self) -> None:
        for observer in self._observers:
            observer.update(self)

# but there are also other notifications
class Reset(Notification):
    """Used to send an explicit reset signal to all components which need it."""
    def __init__(self) -> None:
        super().__init__()

class Pause(Notification):
    """Used to pause/resume the application."""
    def __init__(self) -> None:
        super().__init__()
        # whether the application is paused
        self._paused = False

    def switch(self) -> None:
        """For switching between the application paused and unpaused."""
        self._paused = not self._paused
        if self._paused:
            logger.info("pausing")
        else:
            logger.info("resuming")
        self.notify()

    @property
    def paused(self) -> bool:
        """Returns whether the application is paused."""
        return self._paused
    
class MovementSync(Notification):
    """For changing the synchronization between movements."""
    def __init__(self) -> None:
        super().__init__()
        self._sync = 0.1

    @property
    def sync(self) -> float:
        """Return the synchronization between movements."""
        return self._sync

    def decrease_synchronization(self) -> None:
        """Decreases the synchronization by lifting the threshold."""
        self._sync = min(1, self._sync+0.05)
        self.notify()

    def increase_synchronization(self) -> None:
        """Increases the synchronization by lowering the threshold."""
        self._sync = max(0, self._sync-0.05)
        self.notify()

class OutOfTime(Notification):
    """For displaying/changing certain UI elements if user takes too long to complete a movement."""
    def _init__(self) -> None:
        super().__init__()
        # value between 0 and 1
        # 0 indicates that the user is still in time
        # 1 indicates that the user takes way too long to perform a movement
        # badness == 1 means that the accuracy will be 0
        self.badness = 0

class ParticipantMoved(Notification):
    """
    For only measuring when the participant has actually moved.
    This is done so that the reaction time of the participant is not measured.
    """
    def _init__(self) -> None:
        super().__init__()

class StateUpdate(Notification):
    """Only notifies if the state has actually changed/is about to change"""
    def __init__(self) -> None:
        super().__init__()