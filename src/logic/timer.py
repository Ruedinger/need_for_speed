import time
from ..util.constants import n_to_base

class Timer:
    """A custom timer class. Used for measuring how long participants take to complete a movement etc."""
    def __init__(self, run_time: int) -> None:
        self._start_time: int = None
        self.run_time: int = run_time

    def start(self) -> None:
        """Starts this timer."""
        self._start_time = time.perf_counter_ns()

    def stop(self) -> None:
        """Stops the timer."""
        self._start_time = None

    @property
    def is_running(self) -> bool:
        """Returns whether timer is running."""
        return self._start_time is not None

    @property
    def passed_time(self) -> int:
        """Returns time since the timer started in ns."""
        if self._start_time is None:
            return 0
        else:
            return time.perf_counter_ns() - self._start_time

    @property
    def done(self) -> bool:
        """Returns whether timer is done."""
        return self._start_time is not None and time.perf_counter_ns() - self._start_time >= self.run_time

    @property
    def remaining_time_s(self) -> int:
        """Returns the remaining time in the current configuration."""
        if self._start_time is None:
            return self.run_time*n_to_base
        else:
            return (self.run_time + self._start_time - time.perf_counter_ns())*n_to_base