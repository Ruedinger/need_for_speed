from time import perf_counter_ns
from collections import deque
import pickle
from math import pi
from typing import List, Tuple, Iterable
from pyglet.math import Vec2
from numpy import array, mean, delete, zeros  
import mediapipe as mp
from .application_state import ApplicationState, first_state, instruction_states
from ..util.parameters import model_path, mode, ApplicationMode, window_width, window_height
from ..util.logging_config import logger
from ..util.interfaces import Subject, Observer, PeriodicTask, Completable
from .instruction import Instruction, MovementType
from .timer import Timer
from ..util.matcher import Matcher
from .notification import Reset, Pause, ParticipantMoved, StateUpdate
from .skeleton_kinematics import num_of_keypoints, shoulder_between_angles
from ..util.constants import base_to_m
from .task_scheduler import TaskScheduler
from resources.resources import success_sound

matcher = Matcher()

# mediapipe aliases
BaseOptions = mp.tasks.BaseOptions
PoseLandmarker = mp.tasks.vision.PoseLandmarker
PoseLandmarkerOptions = mp.tasks.vision.PoseLandmarkerOptions
PoseLandmarkerResult = mp.tasks.vision.PoseLandmarkerResult
VisionRunningMode = mp.tasks.vision.RunningMode
GPU = BaseOptions.Delegate.GPU
CPU = BaseOptions.Delegate.CPU

class Model(Observer, PeriodicTask):
    """For accessing pose estimations and scheduling instructions."""
    def __init__(
        self,
        state_notifier: StateUpdate,
        task_scheduler: TaskScheduler,
    ) -> None:
        self._state_notifier = state_notifier
        self._task_scheduler = task_scheduler
        # current delay in ms of the keypoints, for introducing latency
        self._current_delay: int = 0
        # buffer for the keypoints
        # format: time stamp in ms, list of keypoints at that time
        self._keypoint_buffer: List[Tuple[int, List[Vec2]]] = []
        # buffer for world keypoints
        # do not need latency, so no timestamp is needed
        self._world_keypoint_buffer: array = zeros((num_of_keypoints, 3), dtype=float)
        # instructions for all the required states
        with open('instructions/calibration', 'rb') as fd:
            self._calibration_instructions = pickle.load(fd)
        with open('instructions/familiarize', 'rb') as fd:
            self._familiarization_instructions = pickle.load(fd)
        with open('instructions/coordination_test', 'rb') as fd:
            self._coordination_instructions = pickle.load(fd)
        with open('instructions/condition', 'rb') as fd:
            self._condition_instructions = pickle.load(fd)
        # index of the current instruction in the instruction sequence
        self._instruction_index: int = 0
        # timer regulates when next instruction is to be loaded
        self.instruction_timer: Timer = Timer(30)
        # whether mediapipe detects a skeleton
        self._skeleton_detected: bool = False
        # init mediapipe
        options = PoseLandmarkerOptions(
                base_options=BaseOptions(model_asset_path=model_path, delegate=GPU),
                running_mode=VisionRunningMode.LIVE_STREAM,
                result_callback=self.update_keypoint_buffer,
                num_poses=1,
            )

        self._ml_model = PoseLandmarker.create_from_options(options)

        # define first state of the application and tell scheduler
        self._state: ApplicationState = first_state
        self._task_scheduler.schedule_tasks(self._state)

        # for storing the real world size of movements
        self.real_world_movement_size: float = 1.0

        # contains objects which will be used to trigger the next state update
        self._completable_tasks: Iterable[Completable] = deque()

        # whether the model is paused
        self._paused = False

        # for profiling
        if mode == ApplicationMode.DELTA_TIMES:
            self.inference_times: Iterable[float] = deque()

    def add_completable_task(self, task: Completable) -> None:
        """Adds a Completable and raises an error should task not be Completable."""
        if isinstance(task, Completable):
            self._completable_tasks.append(task)
        else:
            raise ValueError(f"task is not Completable, but {type(task)}") 

    def remove_completable_task(self, task: Completable) -> None:
        """Removes task from the model."""
        self._completable_tasks.remove(task)

    def update_state(self) -> None:
        """Manages the state transitions of the model class."""
        logger.info(f"Old state: {ApplicationState(self._state).name}\nOld instruction index: {self._instruction_index}")
        success_sound.play()

        # set new state depending on old state
        match self._state:
            case ApplicationState.CALIBRATION:
                self._instruction_index += 1
                if self._instruction_index == len(self._calibration_instructions):
                    self._instruction_index = 0
                    self._previous_state = ApplicationState.FAMILIARIZE
                    self._state = ApplicationState.PREPARATION
            case ApplicationState.FAMILIARIZE:
                self._instruction_index += 1
                if self._instruction_index == len(self._familiarization_instructions):
                    self._instruction_index = 0
                    self._previous_state = ApplicationState.COORDINATION_TEST
                    self._state = ApplicationState.PREPARATION
                else:
                    self._previous_state = self._state
                    self._state = ApplicationState.PREPARATION
            case ApplicationState.COORDINATION_TEST:
                self._instruction_index += 1
                if self._instruction_index == len(self._coordination_instructions):
                    self._instruction_index = 0
                    self._previous_state = ApplicationState.CONDITION
                    self._state = ApplicationState.PREPARATION
                else:
                    self._previous_state = self._state
                    self._state = ApplicationState.PREPARATION
            case ApplicationState.CONDITION:
                self._instruction_index += 1
                if self._instruction_index % 5 == 0:
                    self._previous_state = self._state
                    self._state = ApplicationState.QUESTIONNAIRE
                else:
                    self._previous_state = self._state
                    self._state = ApplicationState.PREPARATION
            case ApplicationState.CLOSING:
                self._current_delay = 0
            case ApplicationState.PREPARATION:
                self._state = self._previous_state
            case ApplicationState.QUESTIONNAIRE:
                self._state = ApplicationState.PREPARATION
                if self._instruction_index == len(self._condition_instructions):
                    self._state = ApplicationState.CLOSING

        if self._state in instruction_states:
            # set instruction timer if needed
            if self.current_instruction.timer != -1:
                self.instruction_timer.run_time = self.current_instruction.timer
            # only start instruction timer when no participant moved notification is send
            if self.current_instruction.movements[0] in [MovementType.UP, MovementType.SIDE, MovementType.DOWN] and self.current_instruction.movements[1] in [MovementType.UP, MovementType.SIDE, MovementType.DOWN] and self.current_instruction.timer != -1:
                self.instruction_timer.start()
            else:
                self.instruction_timer.stop()
        else:
            self.instruction_timer.stop()

        if self._state == ApplicationState.CONDITION:
            self._current_delay = self.current_instruction.latency
        else:
            self._current_delay = 0

        self._task_scheduler.schedule_tasks(self._state)

        # notify other components of new state
        self._state_notifier.notify()

        logger.info(f"Set landmark estimation latency to {self._current_delay}")
        logger.info(f"New state: {ApplicationState(self._state).name}\nNew instruction index: {self._instruction_index}")

    @property
    def state(self) -> ApplicationState:
        """Returns the current state of the model."""
        return self._state

    @property
    def current_instruction(self) -> Instruction:
        """Returns the current instruction. Only possible in the right state."""
        return self.current_instructions(self._state)[self._instruction_index]

    def current_instructions(self, state: ApplicationState) -> List[Instruction]:
        """Returns the list of all instructions of the specified state. Look at the paper to see how states of the model map to phases of the experiment."""
        match state:
            case ApplicationState.FAMILIARIZE:
                return self._familiarization_instructions
            case ApplicationState.CALIBRATION:
                return self._calibration_instructions
            case ApplicationState.COORDINATION_TEST:
                return self._coordination_instructions
            case ApplicationState.CONDITION:
                return self._condition_instructions
            case ApplicationState.PREPARATION:
                return self.current_instructions(self._previous_state)
            case ApplicationState.QUESTIONNAIRE:
                return self.current_instructions(self._previous_state)
            case _:
                raise AttributeError(f"Current instruction not available in {ApplicationState(self._state).name}")
    @property
    def instruction_index(self) -> int:
        """Returns the current instruction index, depending on the ApplicationState."""
        return self._instruction_index

    @property
    def current_delay(self) -> int:
        """Return the current delay of the pose estimation algorithm."""
        return self._current_delay

    @current_delay.setter
    def current_delay(self, value: int):
        """For setting the delay of the pose estimation algorithm."""
        self._current_delay = value

    @property
    def keypoints(self) -> List[Vec2]:
        """For accessing the most recent (according to the current delay) pose estimation results."""
        # discard old keypoints, so the buffer does not get too big
        current_time = perf_counter_ns() // 10**6
        max_delay_threshold: int = current_time - base_to_m
        last_valid_index: int = 0
        for i in range(len(self._keypoint_buffer)):
            if self._keypoint_buffer[i][0] >= max_delay_threshold:
                last_valid_index = i
                break
        del self._keypoint_buffer[:last_valid_index]
        # return dummy skeleton if no skeletons are in the buffer
        if not self._keypoint_buffer:
            logger.warning("no landmarks in buffer")
            return [Vec2(0, 0)]*num_of_keypoints

        # search for keypoint with valid latency
        min_delay_threshold: int = current_time - self._current_delay
        for i in range(len(self._keypoint_buffer)-1, -1, -1):
            if self._keypoint_buffer[i][0] < min_delay_threshold:
                return self._keypoint_buffer[i][1]
        # return oldest available skeleton if no skeleton with the right latency is available
        return self._keypoint_buffer[0][1]
            
    @property
    def world_keypoints(self) -> array:
        return self._world_keypoint_buffer

    def process_frame(self, frame: array, time: int) -> None:
        """
        For precessing a new frame async with Mediapipe pose.
        The frame provided by cv2 is actually in BGR color space, to the channels do not match.
        However, in the MediaPipe documentation they also read from a cv2 video capture without explicit conversion.
        If I convert the color spaces, performance drops drastically.
        """
        mp_image = mp.Image(image_format=mp.ImageFormat.SRGB, data=frame)
        self._ml_model.detect_async(mp_image, time)

    @staticmethod
    def modify_keypoints(landmark_array: array) -> array:
        """Calculate custom skeleton from MediaPipe skeleton."""
        # calculate middle of hand
        landmark_array[16] = mean(landmark_array[18:24:2], axis=0)
        landmark_array[15] = mean(landmark_array[17:23:2], axis=0)
        # calculate head position as mean of head keypoints
        landmark_array[0] = mean(landmark_array[0:11], axis=0)
        # calculate neck position
        landmark_array[3] = mean(landmark_array[11:13], axis=0)
        # calculate point between hips
        landmark_array[22] = mean(landmark_array[23:25], axis=0)
        landmark_array = delete(landmark_array, (1, 2, 4, 5, 6, 7, 8, 9, 10, 17, 18, 19, 20, 21), axis=0)
        return landmark_array

    def update_keypoint_buffer(self, result: PoseLandmarkerResult, output_image: mp.Image, timestamp_ms: int) -> None:
        """
        Updates the keypoint buffers with new skeleton data.
        If no skeleton is detected in the frame, the respective variable will be set to false.
        """
        if mode == ApplicationMode.DELTA_TIMES:
            self.inference_times.append(perf_counter_ns() // 10**6 - timestamp_ms)

        # first process relative landmarks in pixel space
        if result.pose_landmarks:
            landmarks = result.pose_landmarks[0]
            # all needed keypoints
            indices = range(0, 29)
            landmark_array = zeros(shape=(len(indices), 2), dtype=float)
            for i in indices:
                landmark_array[i] = array([landmarks[i].x, landmarks[i].y], dtype=float)
            landmark_array[:, 0] = (1-landmark_array[:, 0])*window_width
            landmark_array[:, 1] = (1-landmark_array[:, 1])*window_height
            # customize skeleton
            landmark_array = self.modify_keypoints(landmark_array)
            # convert in simple format
            res = []
            for i in range(landmark_array.shape[0]):
                res.append(Vec2(landmark_array[i][0], landmark_array[i][1]))
            self._keypoint_buffer.append((timestamp_ms, res))
        # similar for world keypoints
        if result.pose_world_landmarks:
            world_landmarks = result.pose_world_landmarks[0]
            # all needed keypoints
            indices = range(0, 29)
            world_landmark_array = zeros(shape=(len(indices), 3), dtype=float)
            for i in indices:
                world_landmark_array[i] = array((world_landmarks[i].x, world_landmarks[i].y, world_landmarks[i].z), dtype=float)
            # customize skeleton
            self._world_keypoint_buffer = self.modify_keypoints(world_landmark_array)
        self._skeleton_detected = bool(result.pose_landmarks)

    def process(self) -> None:
        """Triggers update_state when needed to transition to the next state and/or instruction."""
        if not self._paused:
            if not self.instruction_timer.is_running:
                for completable in self._completable_tasks:
                    if completable.done:
                        completable.done = False
                        self.update_state()
                if self._skeleton_detected and self._state in instruction_states:
                    movement = self.current_instruction.movements[0]
                    keypoints = self.keypoints
                    if movement == MovementType.UP:
                        if shoulder_between_angles(keypoints, "left_shoulder", pi*2/3, pi) and shoulder_between_angles(keypoints, "right_shoulder", pi*2/3, pi):
                            self.update_state()
                    elif movement == MovementType.SIDE:
                        if shoulder_between_angles(keypoints, "left_shoulder", pi/3, pi*2/3) and shoulder_between_angles(keypoints, "right_shoulder", pi/3, pi*2/3):
                            self.update_state()
                    elif movement == MovementType.DOWN:
                        if shoulder_between_angles(keypoints, "left_shoulder", 0, pi/3) and shoulder_between_angles(keypoints, "right_shoulder", 0, pi/3):
                            self.update_state()
            elif self.instruction_timer.done:
                self.update_state()

    @matcher.base_case
    def update(self, subject: Subject) -> None:
        """Base update method for notifications."""
        raise ValueError(f"Expected subject of type ParticipantMoved, Reset or Pause. Got {type(subject)}")

    @matcher.case(lambda self, subject: isinstance(subject, Reset))
    def reset_update(self, subject: Reset) -> None:
        """Resets the state of the model."""
        self._current_delay = 0
        self._instruction_index = 0
        self.instruction_timer.stop()
        self._state = first_state
        self._task_scheduler.schedule_tasks(self._state)

    @matcher.case(lambda self, subject: isinstance(subject, Pause))
    def pause_update(self, subject: Pause) -> None:
        """Pauses time-related components."""
        self._paused = subject.paused
        if subject.paused:
            if self._state in [ApplicationState.CALIBRATION, ApplicationState.COORDINATION_TEST]:
                self.instruction_timer.run_time -= self.instruction_timer.passed_time
                self.instruction_timer.stop()
            elif self._state == ApplicationState.CONDITION:
                self._current_delay = 0
        else:
            if self._state in [ApplicationState.CALIBRATION, ApplicationState.COORDINATION_TEST]:
                self.instruction_timer.start()
            if self._state == ApplicationState.CONDITION:
                self._current_delay = self.current_instruction.latency

    @matcher.case(lambda self, subject: isinstance(subject, ParticipantMoved))
    def participant_update(self, subject: ParticipantMoved) -> None:
        """For stating the instruction timer only when the participant has actually started moving."""
        if self._state == ApplicationState.COORDINATION_TEST:
            self.instruction_timer.start()
        else:
            self.instruction_timer.stop()

    @property
    def skeleton_detected(self) -> None:
        """Returns whether a skeleton was detected in the last frame. Does not take delay into account to notify participants quickly when they move out of the field of view of the camera."""
        return self._skeleton_detected