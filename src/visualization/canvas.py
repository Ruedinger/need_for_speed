from collections import deque
from typing import Iterable
import numpy as np
from pyglet.window import Window, key, FPSDisplay
from pyglet.graphics import Batch, Group
from pyglet.gl import Config
from pyglet.text import Label
from pyglet.app import exit
from pyglet.image import ImageData
from cv2 import useOptimized
from .display import MovementDisplay, QuestionDisplay, PreparationDisplay
from ..logic.model import Model
from ..logic.application_state import ApplicationState
from ..logic.task_scheduler import TaskScheduler
from .ui_elements import Skeleton, Panel, AccuracyDisplay, TitleScreen, FinalScreen, CenterLabel, DebugInfo
from ..util.parameters import fullscreen, window_height, window_width, margin, font_color, border_radius, ui_color, mode, ApplicationMode
from ..util.logging_config import logger
from ..logic.notification import Reset, Pause, MovementSync, OutOfTime, ParticipantMoved, StateUpdate
from ..logic.calibrator import Calibrator
from ..logic.data_collector import DataCollector

class Canvas(Window):
    """
    Manages key press events contains all graphical elements.
    """
    def __init__(
        self,
        task_scheduler: TaskScheduler,
        model: Model,
        reset: Reset,
        pause: Pause,
        sync: MovementSync,
        movement_time: OutOfTime,
        participant_moved: ParticipantMoved,
        state_update: StateUpdate,
        data_collector: DataCollector,
        video_width: float,
        video_height: float
    ) -> None:
        self._reset = reset
        self._pause = pause
        self._sync = sync
        self._state_update = state_update

        gl_config = Config(double_buffer=True, depth_size=0, sample_buffers=1, samples=2, stencil_size=0, aux_buffers=0)

        if fullscreen:
            super().__init__(fullscreen=True, config=gl_config)
        else:
            super().__init__(width=window_width, height=window_height, caption="Need for Speed", config=gl_config)
        
        if mode == ApplicationMode.DELTA_TIMES:
            self.window_draw_delta_times: Iterable[float] = deque()

        # pyglet stuff for rendering
        self._batch = Batch()
        background = Group(order=0)
        foreground = Group(order=1)
        ui = Group(order=2)

        # 3*video_width is the number of bytes per row
        self.background: ImageData = ImageData(width=video_width, height=video_height, fmt="BGR", data=np.zeros((video_height, video_width, 3)), pitch=-3*video_width)

        # log some system information
        logger.info("Initializing canvas")
        logger.info(f"Window resolution: {self.width} x {self.height}")
        config = self.context.config
        logger.info(f"double_buffer: {config.double_buffer}")
        logger.info(f"depth_size: {config.depth_size}")
        logger.info(f"sample_buffers: {config.sample_buffers}")
        logger.info(f"samples: {config.samples}")
        logger.info(f"stencil_size: {config.stencil_size}")
        logger.info(f"aux_buffers: {config.aux_buffers}")
        logger.info(f"Application mode: {mode}")
        logger.info(f"Optimized cv2: {useOptimized()}")

        # instantiate and attach views
        panel_width: float = (self.width -2*margin)/6
        panel_height: float = self.height - 2*margin

        title_screen = TitleScreen(
            x=margin, 
            y=margin, 
            width=self.width - 2*margin, 
            height=self.height - 2*margin, 
            border_radius=border_radius, 
            model=model,
            color=ui_color, 
            batch=self._batch, 
            group=foreground,
            display_group=ui
            )
        self._state_update.attach(title_screen)

        final_screen = FinalScreen(
            x=margin,
            y=margin,
            width=self.width - 2*margin, 
            height=self.height - 2*margin, 
            border_radius=border_radius, 
            model=model,
            color=ui_color, 
            batch=self._batch, 
            group=ui
        )
        self._state_update.attach(final_screen)

        center_label = CenterLabel(
            x=margin+panel_width, 
            y=margin+panel_height/3, 
            width=4*panel_width, 
            height=panel_height/3, 
            radius=border_radius,
            model=model,
            color=ui_color,
            batch=self._batch, 
            group=ui
            )
        self._pause.attach(center_label)
        self._reset.attach(center_label)
        movement_time.attach(center_label)

        panel = Panel(
            x=panel_width*5+margin, 
            y=margin, 
            width=panel_width, 
            height=panel_height, 
            border_radius=border_radius, 
            data_collector=data_collector,
            model=model,
            color=ui_color, 
            batch=self._batch, 
            group=foreground,
            display_group=ui
            )
        panel.visible = False
        self._state_update.attach(panel)

        calibrator = Calibrator(model)
        self._pause.attach(calibrator)
        self._reset.attach(calibrator)
        self._state_update.attach(calibrator)

        movement_display = MovementDisplay(
            x=panel_width+margin, 
            y=margin, 
            width=4*panel_width, 
            height=panel_height, 
            calibrator=calibrator,
            data_collector=data_collector,
            participant_moved=participant_moved,
            model=model,
            batch=self._batch, 
            movement_group = foreground,
            highlight_group = ui
            )
        self._reset.attach(movement_display)
        self._pause.attach(movement_display)
        self._sync.attach(movement_display)
        self._state_update.attach(movement_display)

        preparation_display = PreparationDisplay(
            x=panel_width+margin, 
            y=margin, 
            width=4*panel_width, 
            height=panel_height, 
            calibrator=calibrator,
            model=model,
            batch=self._batch, 
            group=foreground
            )
        self._reset.attach(preparation_display)
        self._pause.attach(preparation_display)
        self._state_update.attach(preparation_display)

        question_display = QuestionDisplay(
            x=margin, 
            y=margin, 
            width=5*panel_width, 
            height=panel_height, 
            data_collector=data_collector,
            model=model,
            batch=self._batch, 
            background_group=foreground,
            image_group=ui
            )
        self._reset.attach(question_display)
        self._pause.attach(question_display)
        self._state_update.attach(question_display)

        accuracy_display = AccuracyDisplay(
            x=margin, 
            y=margin, 
            width=panel_width, 
            height=panel_height, 
            border_radius=border_radius, 
            data_collector=data_collector,
            model=model,
            color=ui_color, 
            batch=self._batch, 
            group=foreground, 
            display_group=ui
            )
        accuracy_display.visible = False
        
        self._reset.attach(accuracy_display)
        movement_time.attach(accuracy_display)
        self._state_update.attach(accuracy_display)

        skeleton = Skeleton(
            x=0, 
            y=0, 
            width=self.width, 
            height=self.height, 
            radius=10, 
            model=model,
            batch=self._batch, 
            point_group=foreground, 
            line_group=background
            )

        task_scheduler.add_task(title_screen, [ApplicationState.CALIBRATION])
        task_scheduler.add_task(center_label, [ApplicationState.FAMILIARIZE, ApplicationState.CALIBRATION, ApplicationState.COORDINATION_TEST, ApplicationState.CONDITION, ApplicationState.PREPARATION, ApplicationState.QUESTIONNAIRE, ApplicationState.CLOSING])
        task_scheduler.add_task(panel, [ApplicationState.CALIBRATION, ApplicationState.FAMILIARIZE, ApplicationState.COORDINATION_TEST, ApplicationState.CONDITION, ApplicationState.PREPARATION, ApplicationState.QUESTIONNAIRE])
        task_scheduler.add_task(calibrator, [ApplicationState.CALIBRATION])
        task_scheduler.add_task(movement_display, [ApplicationState.FAMILIARIZE, ApplicationState.COORDINATION_TEST, ApplicationState.CONDITION])
        task_scheduler.add_task(preparation_display, [ApplicationState.PREPARATION])
        task_scheduler.add_task(question_display, [ApplicationState.QUESTIONNAIRE])
        task_scheduler.add_task(accuracy_display, [ApplicationState.COORDINATION_TEST, ApplicationState.CONDITION])
        task_scheduler.add_task(skeleton, [ApplicationState.FAMILIARIZE, ApplicationState.PREPARATION, ApplicationState.CONDITION, ApplicationState.CALIBRATION, ApplicationState.COORDINATION_TEST, ApplicationState.QUESTIONNAIRE, ApplicationState.CLOSING])
        model.add_completable_task(movement_display)
        model.add_completable_task(preparation_display)
        model.add_completable_task(question_display)

        if mode == ApplicationMode.DEBUG:
            debug_info = DebugInfo(
                x=margin+panel_width,
                y=margin,
                width=panel_width,
                height=panel_height/2,
                border_radius=border_radius,
                model=model,
                color=ui_color,
                batch=self._batch,
                group=background
            )
            self._state_update.attach(debug_info)
            self._fps_display = FPSDisplay(self)
            font_size = 20
            self._fps_display.label = Label("", x=margin, y=self.height-margin-font_size, height=font_size, font_size=font_size, font_name="Roboto", color=font_color)

    def on_key_press(self, symbol, modifier) -> None:
        """For controlling the application using the keyboard."""
        match symbol:
            case key.Q:
                exit()
            case key.R:
                logger.info("resetting")
                self._reset.notify()
                self._state_update.notify()
            case key.SPACE:
                self._pause.switch()
            case key.UP:
                logger.info("increase maximal bounding box offset")
                self._sync.decrease_synchronization()
            case key.DOWN:
                logger.info("decrease maximal bounding box offset")
                self._sync.increase_synchronization()
        
    if mode == ApplicationMode.DELTA_TIMES:
        def on_refresh(self, dt: float) -> None:
            """Draws the next frame."""
            self.clear()
            self.background.blit(0, 0, width=self.width, height=self.height)
            self._batch.draw()
            self.window_draw_delta_times.append(dt)
    elif mode == ApplicationMode.DEBUG:
        def on_refresh(self, dt: float) -> None:
            """Draws the next frame."""
            self.clear()
            self.background.blit(0, 0, width=self.width, height=self.height)
            self._batch.draw()
            self._fps_display.draw()
    else:
        def on_refresh(self, dt: float) -> None:
            """Draws the next frame."""
            self.clear()
            self.background.blit(0, 0, width=self.width, height=self.height)
            self._batch.draw()
