# this file contains classes for displaying instructions, preparations, and questions
from math import pi
from typing import List, Iterable
from pyglet.graphics import Batch, Group
from pyglet.math import Vec2
from pyglet.sprite import Sprite
from pyglet.shapes import RoundedRectangle
from .movement import LineMovement, EllipseMovement, ZigZagMovement, Movement, Preparation, Frustration, QuestionItem
from .rendering import RectangleWithText
from ..logic.instruction import MovementType
from ..logic.skeleton_kinematics import shoulder_between_angles
from ..util.parameters import instruction_colors, border_radius, instruction_highlight_colors, margin, ui_color, mode, ApplicationMode
from ..util.logging_config import logger
from ..util.interfaces import Completable, Subject, Observer
from ..logic.model import Model
from ..logic.application_state import ApplicationState
from ..logic.notification import Reset, Pause, MovementSync, ParticipantMoved, StateUpdate
from ..util.matcher import Matcher
from ..logic.calibrator import Calibrator
from ..logic.data_collector import DataCollector
from ..logic.skeleton_kinematics import keypoints_to_indices, get_movement_position, get_preparation_position
from resources.resources import success_sound, skeleton_down

left_hand_index = keypoints_to_indices['left_hand']
left_shoulder_index = keypoints_to_indices['left_shoulder']

movement_matcher = Matcher()    

class MovementDisplay(Observer, Completable):
    """For displaying and loading movements."""
    def __init__(
        self, 
        x: float,
        y: float,
        width: float,
        height: float,
        calibrator: Calibrator,
        data_collector: DataCollector,
        participant_moved: ParticipantMoved,
        model: Model,
        batch: Batch = None,
        movement_group: Group = None,
        highlight_group: Group = None
    ) -> None:
        super().__init__()
        self._batch = batch
        self._movement_group = movement_group
        self._highlight_group = highlight_group
        self._calibrator = calibrator
        self._data_collector = data_collector
        self._participant_moved = participant_moved
        self._model = model

        # window borders
        self._max = Vec2(x + width, y + height)
        self._min = Vec2(x, y)
        logger.info(f"Initializing MovementDisplay\nMinimal coordinates: {self._min}\nMaximal coordinates: {self._max}")

        # put left and right movement in a list
        # index 0 is for the left movement
        # index 1 is for the right movement
        self._movement_displays: List[Movement] = [Movement(), Movement()]

        # for updating the movements only if the application is not paused
        self._paused: bool = False
        # maximal difference between progresses of movements
        self._sync = 0.1
        # whether there was a state change since the last dt_update call
        self._new_state: bool = False
        # whether the other components where already notified that participant moved
        self._has_participant_moved: bool = False

    @movement_matcher.base_case
    def update(self, subject: Subject) -> None:
        """Base update method for notifications."""
        raise ValueError(f"Expected subject of type StateUpdate, MovementSync, Reset or Pause. Got {type(subject)}")

    def process(self) -> None:
        """
        Updates movement related variables. 
        Also triggers the loading of new movements if old movements are completed.
        """
        keypoints: List[Vec2] = self._model.keypoints
        current_instruction = self._model.current_instruction
        state = self._model.state

        if self._new_state and self._model.skeleton_detected:
            self.load(keypoints, current_instruction.movements, current_instruction.mirrored)
            self._model.real_world_movement_size = max(self._movement_displays[0].calculate_real_world_length(self._calibrator.real_world_size), self._movement_displays[1].calculate_real_world_length(self._calibrator.real_world_size))
            self._has_participant_moved = False
            self._new_state = False
        
        if not self._paused:
            self.visible = True
            distances = []
            for i in range(2):
                hand = keypoints[left_hand_index+i]
                distances.append(self._movement_displays[i].get_accuracy(hand, self._calibrator.real_world_size))
                # determine whether the participant correctly performs the movement
                hand in self._movement_displays[i]
                # update visuals
                self._movement_displays[i].update_progress(self._movement_displays[1-i], self._sync)
                self._movement_displays[i].update_highlight()

                # to only start the movement when the participant did something
                if hand not in self._movement_displays[i].start_point and not self._has_participant_moved:
                    self._participant_moved.notify()
                    self._has_participant_moved = True

            # determine whether both movements are done
            self.done = True
            for display in self._movement_displays:
                self.done &= display.done(self._sync)

            if state == ApplicationState.COORDINATION_TEST and self.done:
                self.done = False
                success_sound.play()
                for i in range(2):
                    self._movement_displays[i].flip()
                self._data_collector.num_repetitions += 1

            if self._has_participant_moved and state != ApplicationState.FAMILIARIZE:
                self._data_collector.accuracies = distances

    @movement_matcher.case(lambda self, subject: isinstance(subject, Reset))
    def reset_update(self, subject: Reset) -> None:
        """Resets the state of this instance so the experiment can be restarted."""
        self.visible = False
        self._movement_displays = [Movement(), Movement()]
        self._new_state = False
        self._has_participant_moved = False

    @movement_matcher.case(lambda self, subject: isinstance(subject, Pause))
    def pause_update(self, subject: Pause) -> None:
        """Pauses movements, i.e. prevents updates and hides movements while paused."""
        self._paused = subject.paused
        self.visible = False

    @movement_matcher.case(lambda self, subject: isinstance(subject, MovementSync))
    def sync_update(self, subject: MovementSync) -> None:
        """Changes how tight the synchronization between movements is."""
        self._sync = subject.sync

    @movement_matcher.case(lambda self, state: isinstance(state, StateUpdate))
    def state_update(self, state: StateUpdate) -> None:
        """For loading new movements on state update."""
        if self._model.state in [ApplicationState.FAMILIARIZE, ApplicationState.COORDINATION_TEST, ApplicationState.CONDITION]:
            self._new_state = True
        else:
            self.visible = False

    def load(self, keypoints: List[Vec2], movements: List[MovementType], mirrored: bool = False) -> None:
        """Loads next movements, position and size of movements depends on current skeleton."""
        logger.info("loading movements: " + str(movements))
        for i in range(2):
            movement_positions = get_movement_position(keypoints, movements[i], i, self._calibrator.horizontal_movement_size_px, self._calibrator.vertical_movement_size_px, self._calibrator.diagonal_movement_size_px)
            cropped_movement_positions = list(map(self.round_to_window_border, movement_positions))
            self._movement_displays[i].delete()
            match movements[i]:
                case MovementType.ZIGZAG:
                    self._movement_displays[i] = ZigZagMovement(
                        bottom_left=cropped_movement_positions[0], 
                        top_right=cropped_movement_positions[1],
                        mirrored=mirrored,
                        num_elements=3, 
                        width=self._calibrator.movement_width, 
                        color=instruction_colors[i], 
                        highlight_color=instruction_highlight_colors[i],
                        batch=self._batch, 
                        movement_group = self._movement_group,
                        highlight_group = self._highlight_group
                    )
                case MovementType.ELLIPSE:
                    self._movement_displays[i] = EllipseMovement(
                        bottom_left=cropped_movement_positions[0], 
                        top_right=cropped_movement_positions[1],
                        mirrored=mirrored,
                        width=self._calibrator.movement_width, 
                        color=instruction_colors[i], 
                        highlight_color=instruction_highlight_colors[i],
                        batch=self._batch, 
                        movement_group = self._movement_group,
                        highlight_group = self._highlight_group
                    )
                case MovementType.HORIZONTAL_LINE:
                    self._movement_displays[i] = LineMovement(
                        bottom_left=cropped_movement_positions[0], 
                        top_right=cropped_movement_positions[1],
                        mirrored=mirrored,
                        width=self._calibrator.movement_width, 
                        color=instruction_colors[i], 
                        highlight_color=instruction_highlight_colors[i],
                        batch=self._batch, 
                        movement_group = self._movement_group,
                        highlight_group = self._highlight_group
                    )
                case MovementType.VERTICAL_LINE:
                    self._movement_displays[i] = LineMovement(
                        bottom_left=cropped_movement_positions[0], 
                        top_right=cropped_movement_positions[1], 
                        width=self._calibrator.movement_width, 
                        color=instruction_colors[i], 
                        highlight_color=instruction_highlight_colors[i],
                        batch=self._batch, 
                        movement_group = self._movement_group,
                        highlight_group = self._highlight_group
                    )
            self._movement_displays[i].visible = False

    def round_to_window_border(self, point: Vec2) -> Vec2:
        """
        So that the instructions do not move outside the specified space.
        The space for instructions is given by the location and dimension of this object.
        """
        for i in range(2):
            if point[i] < self._min[i]:
                point[i] = self._min[i]
            elif point[i] > self._max[i]:
                point[i] = self._max[i]
        return point

    @property
    def visible(self) -> bool:
        """For getting visibility of movements."""
        return self._movement_displays[0].visible

    @visible.setter
    def visible(self, new_value: bool) -> None:
        """For setting visibility of movements."""
        if new_value != self.visible:
            for i in range(2):
                self._movement_displays[i].visible = new_value

preparation_matcher = Matcher()   

class PreparationDisplay(Observer, Completable):
    """
    For displaying preparations to the participants.
    Needed so that the participant can get into position before performing a movement.
    """
    def __init__(
        self, 
        x: float,
        y: float,
        width: float,
        height: float,
        calibrator: Calibrator,
        model: Model,
        batch: Batch = None,
        group: Group = None 
    ) -> None:
        super().__init__()
        self._model = model
        self._batch = batch
        self._group = group
        self._calibrator = calibrator
        # preparations for priming the user for the subsequent movement
        self._preparations: List[Preparation] = []
        for i in range(2):
            self._preparations.append(Preparation(
                    bottom_left=Vec2(0, 0),
                    top_right=Vec2(10, 10),
                    radius=border_radius,
                    left_hand=i==0,
                    color=instruction_colors[i], 
                    highlight_color=instruction_highlight_colors[i],
                    batch=self._batch, 
                    group=self._group
                ))
        # window borders
        self._max = Vec2(x + width, y + height)
        self._min = Vec2(x, y)
        logger.info(f"Initializing PreparationDisplay\nMinimal coordinates: {self._min}\nMaximal coordinates: {self._max}")
        # for updating the preparations only if the application is not paused
        self._paused: bool = False

    @preparation_matcher.base_case
    def update(self, subject: Subject) -> None:
        """Base update method for notifications."""
        raise ValueError(f"Expected subject of type StateUpdate, Reset or Pause. Got {type(subject)}")

    def process(self) -> None:
        """Loads and moves preparations."""
        keypoints: List[Vec2] = self._model.keypoints

        if self._model.skeleton_detected:
            instruction = self._model.current_instruction
            for i in range(2):
                preparation_position = get_preparation_position(keypoints, instruction.movements[i], i, self._calibrator.horizontal_movement_size_px, self._calibrator.vertical_movement_size_px, self._calibrator.diagonal_movement_size_px, instruction.mirrored)                  
                cropped_preparation_position = self.round_to_window_border(preparation_position)
                preparation_positions = cropped_preparation_position - Vec2(1/2, 1/2) * self._calibrator.movement_width, cropped_preparation_position + Vec2(1/2, 1/2) * self._calibrator.movement_width
                cropped_preparation_positions = list(map(self.round_to_window_border, preparation_positions))
                self._preparations[i].bottom_left = cropped_preparation_positions[0]
                self._preparations[i].top_right = cropped_preparation_positions[1]

        if not self._paused:
            self.visible = True
            self.done = True
            for i in range(2):
                self.done &= keypoints[left_hand_index + i] in self._preparations[i]

    @preparation_matcher.case(lambda self, subject: isinstance(subject, Reset))
    def reset_update(self, subject: Reset) -> None:
        """Resets the state of the instance."""
        self.visible = False

    @preparation_matcher.case(lambda self, subject: isinstance(subject, Pause))
    def pause_update(self, subject: Pause) -> None:        
        """Pauses preparations, i.e. prevents updates and hides preparations while paused."""
        self._paused = subject.paused
        self.visible = False

    @preparation_matcher.case(lambda self, state: isinstance(state, StateUpdate))
    def state_update(self, state: StateUpdate) -> None:
        """Tells object that new preparations need to be loaded."""
        if self._model.state == ApplicationState.PREPARATION:
            self._new_state = True
        self.visible = self._model.state == ApplicationState.PREPARATION  
    
    def round_to_window_border(self, point: Vec2) -> Vec2:
        """
        So that the preparations do not move outside the specified space.
        The space for preparations is given by the location and dimension of this object.
        """
        for i in range(2):
            if point[i] < self._min[i]:
                point[i] = self._min[i]
            elif point[i] > self._max[i]:
                point[i] = self._max[i]
        return point

    @property
    def visible(self) -> bool:
        """For getting visibility of preparations."""
        return self._preparations[0].visible

    @visible.setter
    def visible(self, new_value: bool) -> None:
        """For setting visibility of preparations."""
        if new_value != self.visible:
            for i in range(2):
                self._preparations[i].visible = new_value

question_matcher = Matcher()   

class QuestionDisplay(Observer, Completable):
    """Implements the in-session frustration questions."""
    def __init__(
        self, 
        x: float,
        y: float,
        width: float,
        height: float,
        data_collector: DataCollector,
        model: Model,
        batch: Batch = None,
        background_group: Group = None,
        image_group: Group = None,
    ) -> None:
        logger.info("Initializing QuestionDisplay")
        super().__init__()
        self._x = x
        self._y = y
        self._width = width
        self._height = height
        self._model = model
        self._batch = batch
        self._background_group = background_group
        self._image_group = image_group
        self._data_collector = data_collector
        # load question items
        self._questions: List[QuestionItem] = []
        self.load()
        # for displaying the skeleton
        picture_scale = min(self._height - 2*margin, self._width - 2*margin)/2
        self._skeleton = Sprite(skeleton_down, x + margin, y + margin + picture_scale/2, batch=batch, group=background_group)
        # figure has size 243x500
        self._skeleton.update(scale_y=picture_scale/500)
        self._background = RoundedRectangle(x + margin, y + margin + picture_scale/2, picture_scale*243/500, picture_scale, border_radius, color=ui_color, batch=batch, group=background_group)
        # for updating the buttons only if the application is not paused
        self._paused: bool = False
        # whether the questions should be displayed
        self._new_state: bool = False

    @question_matcher.base_case
    def update(self, subject: Subject) -> None:
        """Base update method for notifications."""
        raise ValueError(f"Expected subject of type StateUpdate, Reset or Pause. Got {type(subject)}")

    def process(self) -> None:
        """For updating the question items when needed."""
        if not self._paused:
            keypoints: List[Vec2] = self._model.keypoints
            if self._new_state:
                if shoulder_between_angles(keypoints, "left_shoulder", 0, pi/3) and shoulder_between_angles(keypoints, "right_shoulder", 0, pi/3):
                    self.skeleton_visible = False
                    self._new_state = False
                else:
                    self.skeleton_visible = True
            else:
                self.questions_visible = True
                self.done = self.select(keypoints)

    @question_matcher.case(lambda self, subject: isinstance(subject, Reset))
    def reset_update(self, subject: Reset) -> None:
        """Resets the state of this instance."""
        for question in self._questions:
            question.delete()
        self.load()
        self._new_state = False

    @question_matcher.case(lambda self, subject: isinstance(subject, Pause))
    def pause_update(self, subject: Pause) -> None:
        """For pausing the application."""
        self._paused = subject.paused
        self.visible = False

    @question_matcher.case(lambda self, state: isinstance(state, StateUpdate))
    def state_update(self, state: StateUpdate) -> None:
        """Tells this instance that new question items need to be loaded."""
        if self._model.state == ApplicationState.QUESTIONNAIRE:
            self.skeleton_visible = True
            self._new_state = True
        else:
            self.visible = False

    def load(self) -> None:
        """For loading new question items."""
        logger.info("loading questions")
        button_size = (self._width - 6*margin)/5
        self._questions = []
        for frustration in Frustration:
            self._questions.append(QuestionItem(
                bottom_left=Vec2(self._x + margin*(frustration+1) + button_size*frustration, self._y + self._height/2 - button_size*7/10), 
                top_right=Vec2(self._x + margin*(frustration+1) + button_size*(frustration+1), self._y + self._height/2 + button_size/2), 
                radius=border_radius, 
                frustration_level=frustration, 
                batch=self._batch, 
                background_group=self._background_group, 
                image_group=self._image_group
                ))
        self._label = RectangleWithText(Vec2(self._x + 2*margin, self._y + self._height/2 + button_size/2 + margin), Vec2(self._x + self._width - 2*margin, self._y + self._height/2 + button_size*3/4 + margin), border_radius, "Please rate your current frustration", int(self._width/40), ui_color, self._batch, self._background_group)
        self.questions_visible = False

    @property
    def skeleton_visible(self) -> bool:
        """Gets visibility of the skeleton."""
        return self._skeleton.visible

    @skeleton_visible.setter
    def skeleton_visible(self, new_value) -> bool:
        """Sets visibility of skeleton."""
        if new_value != self.skeleton_visible:
            self._skeleton.visible = new_value
            self._background.visible = new_value

    @property
    def questions_visible(self) -> None:
        """Gets visibility of questions."""
        return self._questions[0].visible

    @questions_visible.setter
    def questions_visible(self, new_value) -> bool:
        """Sets visibility of questions."""
        if new_value != self.questions_visible:
            for question in self._questions:
                question.visible = new_value
            self._label.visible = new_value

    @property
    def visible(self) -> bool:
        """Returns visibility of skeleton and questions"""
        return self.questions_visible and self.skeleton_visible

    @visible.setter
    def visible(self, new_value: bool) -> None:
        """Sets visibility of skeleton and questions"""
        self.skeleton_visible = new_value
        self.questions_visible = new_value

    def select(self, keypoints: List[Vec2]) -> bool:
        """
        For selecting a question item. 
        An item is selected if the participant hovers over it with the right hand and raises his left hand at the same time.
        """
        for frustration in Frustration:
            if keypoints[keypoints_to_indices['right_hand']] in self._questions[frustration] and shoulder_between_angles(keypoints, "left_shoulder", pi*1/2, pi):
                self._data_collector.frustration = frustration
                return True
        return False
                

