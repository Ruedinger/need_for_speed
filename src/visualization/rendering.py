# This file hosts custom shapes 
# Some code in this file is inspired by the pyglet project and their shapes file
import math
from math import sqrt, radians, atan2, tau, pi, cos, sin, modf
from pyglet.graphics import Batch, Group
from pyglet.text import Label
from pyglet.math import Vec2
from pyglet.gl import GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA
from pyglet.shapes import Triangle, Line, Circle, ShapeBase, get_default_shader, _get_segment, MultiLine, _point_in_polygon, RoundedRectangle, _RadiusT
from typing import List
from typing import Tuple, Sequence, Optional
from ..util.parameters import font_color

def _rotate_point(center: Vec2, point: Vec2, angle) -> Vec2:
    """Rotates point around center by angle."""
    prev_angle = math.atan2(point.y - center.y, point.x - center.x)
    now_angle = prev_angle + angle
    r = math.dist(point, center)
    return Vec2(center.x + r * math.cos(now_angle), center.y + r * math.sin(now_angle))

class Multiline(MultiLine):
    """Multiline that supports drawing only a fraction t."""
    def __init__(
        self,
        *coordinates: Sequence[Vec2],
        closed: bool = False,
        thickness: float = 1.0,
        t: float = 1.0,
        color: tuple[int, int, int, int] = (255, 255, 255, 255),
        batch: Batch | None = None,
        group: Group | None = None
    ) -> None:
        self._t = 1
        # needed to set t
        self._old_coordinates = list(coordinates)
        super().__init__(*coordinates, closed=closed, thickness=thickness, color=color, batch=batch, group=group)
        self.t = t

    @property
    def t(self) -> float:
        """Returns proportion t that is drawn"""
        return self._t

    @t.setter
    def t(self, new_t: float) -> None:
        """
        For setting the proportion t that is drawn.
        t must be between 0 and 1.
        """
        assert 0 <= new_t <= 1, f"t must be between 0 and 1, is {new_t}"
        self._t = new_t
        if new_t == 0:
            self.visible = False
        elif new_t == 1:
            self._coordinates = self._old_coordinates
            # redraw the shape
            self._vertex_list.delete()
            self._num_verts = (len(self._coordinates) - 1) * 6
            self._create_vertex_list()
            self.visible = True
        else:
            # get percentage of line segments to draw
            frac, whole = modf((len(self._old_coordinates)-1)*new_t)
            whole = int(whole)
            # linear interpolation between the two endpoints of line segment that has to be cropped
            new_endpoint = (1-frac)*self._old_coordinates[whole][0] + frac*self._old_coordinates[whole+1][0], (1-frac)*self._old_coordinates[whole][1] + frac*self._old_coordinates[whole+1][1]
            new_coordinates = self._old_coordinates[:whole+1] + [new_endpoint]
            self._coordinates = new_coordinates
            # redraw the shape
            self._vertex_list.delete()
            self._num_verts = (len(self._coordinates) - 1) * 6
            self._create_vertex_list()
            self.visible = True

    @property
    def polygon(self) -> List[Vec2]:
        """Returns polygon of the outline of Multiline."""
        vertices = []
        prev_normal = None
        for i in range(len(self._coordinates) - 1):
            start = Vec2(self._coordinates[i][0], self._coordinates[i][1])
            end = Vec2(self._coordinates[i+1][0], self._coordinates[i+1][1])
            distance = (end - start).normalize()
            normal = Vec2(distance.y, -distance.x)
            if prev_normal is None:
                vertices += [start + normal*self._thickness/2, start - normal*self._thickness/2]
            else:
                miter = (prev_normal + normal).normalize()
                scale = self._thickness/(2*math.sin(math.acos(distance.dot(miter))))
                vertices += [start + miter*scale, start - miter*scale]
            prev_normal = normal
        vertices += [end + normal*self._thickness/2, end - normal*self._thickness/2]
        # construct polygon from vertices
        tmp = vertices[1::2]
        tmp.reverse()
        return vertices[::2] + tmp + [vertices[0]]

    def fraction_of_point(self, point: Vec2) -> float:
        """
        Computes the fraction of the multiline the input point has covert.
        Assumes the point lies near the multiline (in the movement).
        """
        first_point = min(self._coordinates, key=lambda x: x.distance(point))
        index = self._coordinates.index(first_point)
        if index == len(self._coordinates) - 1:
            second_point = first_point
            first_point = self._coordinates[len(self._coordinates) - 2]
        elif index == 0:
            second_point = self._coordinates[1]
        else:
            second_point = min([self._coordinates[index - 1], self._coordinates[index + 1]], key=lambda x: x.distance(point))
            # introduce bias, first point has to be first in original list
            # this is just a second sorting stage
            # implicitly done in the other cases as well
            if index > self._coordinates.index(second_point):
                first_point, second_point = second_point, first_point

        # do the same thing as with the line
        direction = second_point - first_point
        n = direction.normalize()
        projection = first_point + n*n.dot(point - first_point)

        return (abs(projection-first_point)/abs(direction) + self._coordinates.index(first_point))/(len(self._coordinates) - 1)

class Triangle(Triangle):
    """Wrapper around pyglet triangle because sometimes vectors are easier to manage than individual x- and y-coordinates."""
    def __init__(
            self,
            position1: Vec2,
            position2: Vec2,
            position3: Vec2,
            color: tuple[int, int, int, int] | tuple[int, int, int] = (255, 255, 255, 255),
            batch: Batch | None = None,
            group: Group | None = None
    ) -> None:
        super().__init__(x=position1.x, y=position1.y, x2=position2.x, y2=position2.y, x3=position3.x, y3=position3.y, color=color, batch=batch, group=group)

    @property
    def position(self) -> Vec2:
        """Returns position as Vec2."""
        pos = super().position
        return Vec2(pos[0], pos[1])

    @position.setter
    def position(self, new_position: Vec2) -> None:
        """Translates the whole triangle by new_position."""
        self._x = new_position.x
        self._y = new_position.y
        self._update_translation()

    @property
    def position2(self) -> Vec2:
        """Returns position2."""
        return Vec2(self._x2, self._y2)

    @position2.setter
    def position2(self, new_position2: Vec2) -> None:
        """Sets position2."""
        self._x2 = new_position2.x
        self._y2 = new_position2.y
        self._update_vertices()

    @property
    def position3(self) -> Vec2:
        """Returns position3."""
        return Vec2(self._x3, self._y3)

    @position3.setter
    def position3(self, new_position3: Vec2) -> None:
        """Sets position3."""
        self._x3 = new_position3.x
        self._y3 = new_position3.y
        self._update_vertices()

    @property
    def polygon(self) -> List[Vec2]:
        """Returns triangle as polygon."""
        return [Vec2(self._x, self._y), Vec2(self._x2, self._y2), Vec2(self._x3, self._y3), Vec2(self._x, self._y)]

class Circle(Circle):
    """Circle with Vec2 as position."""
    def __init__(
        self, 
        position: Vec2, 
        radius: float, 
        color: Tuple[int, int, int, int] = (255, 255, 255, 255),
        batch: Batch = None, 
        group: Group = None
    ) -> None:
        super().__init__(x=position[0], y=position[1], radius=radius, segments=200, color=color, batch=batch, group=group)

    @property
    def position(self) -> Vec2:
        """Returns position as Vec2."""
        return Vec2(self._x, self._y)

    @position.setter
    def position(self, point: Vec2) -> None:
        """Sets position to point."""
        self._x, self._y = point
        self._update_translation()

class EllipticArc(ShapeBase):
    """
    Arc with an elliptic shape, the horizontal size is proportional to a and the vertical size is proportional to b.
    Implements further convenience methods project_point and __contains__ which are crucial for movements.
    The angle controls how big the drawn sector is.
    """
    def __init__(
            self,
            position: Vec2,
            a: float,
            b:float,
            segments: int = 20,
            angle: float = tau,
            start_angle: float = pi*3/2,
            closed: bool = False,
            width: float = 1.0,
            t: float = 1.0,
            color: Tuple[int, int, int, int] = (255, 255, 255, 255),
            batch: Batch | None = None,
            group: Group | None = None
    ) -> None:
        self._x = position[0]
        self._y = position[1]
        self._a = a
        self._b = b
        self._segments = segments
        self._t = 1

        # handle both 3 and 4 byte colors
        r, g, b, *a = color
        self._rgba = r, g, b, a[0] if a else 255

        self._width = width
        self._angle = angle
        self._start_angle = start_angle
        # Only set closed if the angle isn't tau
        self._closed = closed if abs(tau - self._angle) > 1e-9 else False
        self._rotation = 0

        # Each segment is now 6 vertices long
        self._num_verts = self._segments * 6 + (6 if self._closed else 0)

        self._batch = batch or Batch()
        program = get_default_shader()
        self._group = self.group_class(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, program, group)

        self._create_vertex_list()

        self.t = t

    @property
    def center_points(self) -> List[Tuple[float, float]]:
        """Returns a list of points which define the segments of the ellipse."""
        x = -self._anchor_x
        y = -self._anchor_y 
        tau_segs = self._angle / self._segments
        start_angle = self._start_angle - radians(self._rotation)

        # Calculate the outer points of the arc:
        points = []
        for i in range(self._segments + 1):
            tmp_angle = i * tau_segs + start_angle
            k = self._a*self._b/sqrt((self._b*cos(tmp_angle))**2 + (self._a*sin(tmp_angle))**2)
            points.append((x + k * cos(tmp_angle), y + k * sin(tmp_angle)))
        return points

    def _create_vertex_list(self) -> None:
        self._vertex_list = self._group.program.vertex_list(
            self._num_verts, self._draw_mode, self._batch, self._group,
            position=('f', self._get_vertices()),
            colors=('Bn', self._rgba * self._num_verts),
            translation=('f', (self._x, self._y) * self._num_verts))

    def _get_vertices(self):
        if not self._visible:
            return (0, 0) * self._num_verts
        else:
            points = self.center_points

            # Create a list of quads from the points
            vertices = []
            prev_miter = None
            prev_scale = None
            for i in range(len(points) - 1):
                prev_point = None
                next_point = None
                if i > 0:
                    prev_point = points[i - 1]
                elif self._closed:
                    prev_point = points[-1]
                elif abs(self._angle - tau) <= 1e-9:
                    prev_point = points[-2]

                if i + 2 < len(points):
                    next_point = points[i + 2]
                elif self._closed:
                    next_point = points[0]
                elif abs(self._angle - tau) <= 1e-9:
                    next_point = points[1]

                prev_miter, prev_scale, *segment = _get_segment(prev_point, points[i], points[i + 1], next_point,
                                                                self._width, prev_miter, prev_scale)
                vertices.extend(segment)

            if self._closed:
                prev_point = None
                next_point = None
                if len(points) > 2:
                    prev_point = points[-2]
                    next_point = points[1]
                prev_miter, prev_scale, *segment = _get_segment(prev_point, points[-1], points[0], next_point,
                                                                self._width, prev_miter, prev_scale)
                vertices.extend(segment)

            return vertices

    def _update_vertices(self) -> None:
        self._vertex_list.position[:] = self._get_vertices()

    @property
    def width(self) -> float:
        """Returns the width of the ellipse."""
        return self._width

    @width.setter
    def width(self, width: float) -> None:
        """For setting the width of the ellipse."""
        self._width = width
        self._update_vertices()

    @property
    def angle(self) -> float:
        """Returns the angle of the ellipse in radian."""
        return self._angle

    @angle.setter
    def angle(self, value: float) -> None:
        """
        For setting the angle of the ellipse in radian.
        """
        tmp_t = self._t
        self._t = 1
        self._angle = value
        self._update_vertices()
        self.t = tmp_t

    @property
    def t(self) -> float:
        """Returns the proportion of the ellipse that is drawn."""
        return self._t

    @t.setter
    def t(self, new_t: float) -> None:
        """
        For changing the proportion t that is drawn.
        t has to be between 0 and 1.
        This only has an effect on drawing, not on the geometric methods.
        """
        assert 0 <= new_t <= 1, f"t must be between 0 and 1, is {new_t}"
        if new_t == 0:
            self.visible = False
        else:
            total_angle = self._angle/self._t
            self._angle = total_angle*new_t
            self._t = new_t
            self._update_vertices()
            self.visible = True

    @property
    def start_angle(self) -> float:
        """Returns the start angle in radian."""
        return self._start_angle

    @start_angle.setter
    def start_angle(self, value: float) -> None:
        """For setting the start angle in radian."""
        self._start_angle = value
        self._update_vertices()

    def __contains__(self, point: Vec2) -> bool:
        """Returns whether point lies in the ellipse. Where the ellipse is only defined by the drawn outer part."""
        polygon = self.polygon
        return _point_in_polygon(polygon, point)

    def transform(self, point: Tuple[float, float]) -> Tuple[float, float]:
        """Helper method for transforming points according to the current position."""
        return point[0] + self._x, point[1] + self._y
    
    @property
    def polygon(self) -> List[Vec2]:
        """Return polygon of the outline of EllipticArc."""
        points = self.center_points
        points = list(map(self.transform, points))

        vertices = []
        prev_normal = None
        for i in range(len(points) - 1):
            start = Vec2(points[i][0], points[i][1])
            end = Vec2(points[i+1][0], points[i+1][1])
            distance = (end - start).normalize()
            normal = Vec2(distance.y, -distance.x)
            if prev_normal is None:
                vertices += [start + normal*self._width/2, start - normal*self._width/2]
            else:
                miter = (prev_normal + normal).normalize()
                scale = self._width/(2*math.sin(math.acos(distance.dot(miter))))
                vertices += [start + miter*scale, start - miter*scale]
            prev_normal = normal
        vertices += [end + normal*self._width/2, end - normal*self._width/2]
        # construct polygon from vertices
        tmp = vertices[1::2]
        tmp.reverse()
        return vertices[::2] + tmp + [vertices[0]]
        
    @property
    def position(self) -> Vec2:
        """Returns the center of the ellipse."""
        return Vec2(self._x, self._y)

    @position.setter
    def position(self, point: Vec2) -> None:
        """Sets the center of the ellipse."""
        self._x, self._y = point
        self._update_translation()

    def point_on_ellipse(self, phi: float) -> Vec2:
        """Returns the point on the arc of the ellipse with angle phi."""
        k = self._a*self._b/sqrt((self._b*cos(phi))**2 + (self._a*sin(phi))**2)
        x = k*cos(phi)
        y = k*sin(phi)

        return Vec2(x + self._x, y + self._y)

    @property
    def circumference(self) -> float:
        """Approximate circumference of an ellipse using Ramanujan's second approximation."""
        a = self._a
        b = self._b
        h = ((a - b) / (a + b))** 2
        circumference = math.pi * (a + b) * (1 + (3 * h) / (10 + math.sqrt(4 - 3 * h)))
        return circumference*self._angle/tau

    def fraction_of_point(self, point: Vec2) -> float:
        """
        Computes the fraction of the arc the input point has covert.
        Assumes point lies near arc (is in movement).
        """
        # compute angle
        point = _rotate_point(self.position, point, radians(self._rotation))
        delta_point_position = Vec2(point.x - self._x + self._anchor_x, point.y - self._y + self._anchor_y)
        # get sign of angle
        angle_sign = int(self._angle/abs(self._angle)) if self._angle != 0 else 0
        # rotate according to start angle
        angle = (angle_sign*(atan2(delta_point_position.y, delta_point_position.x) - self._start_angle)) % tau
        return angle/abs(self._angle)

class Line(Line):
    """
    Line that can return its shape as a polygon and supports only drawing a fraction t of the line.
    """
    def __init__(
        self, 
        start_position: Vec2,
        end_position: Vec2, 
        width: float, 
        t: float = 1.0,
        color: Tuple[int, int, int, int] = (255, 255, 255, 255), 
        batch: Batch = None, 
        group: Group = None
    ) -> None:
        self._t = 1
        super().__init__(x=start_position.x, y=start_position.y, x2=end_position.x, y2=end_position.y, width=width, color=color, batch=batch, group=group)
        self.t = t
        
    @property
    def polygon(self) -> List[Vec2]:
        """Return polygon of the shape of the line."""
        direction = (self.position2 - self.position).normalize()
        normal = Vec2(direction.y, -direction.x)
        points = [
            self.position + normal*self._width/2,
            self.position2 + normal*self._width/2,
            self.position2 - normal*self._width/2,
            self.position - normal*self._width/2,
            self.position + normal*self._width/2,
        ]
        return points

    @property
    def position(self) -> Vec2:
        """Returns position."""
        return Vec2(self._x, self._y)

    @position.setter
    def position(self, point: Vec2) -> None:
        """Sets position."""
        tmp_t = self._t
        self._t = 1
        self._x, self._y = point
        self._update_vertices()
        self._update_translation()
        self.t = tmp_t

    @property
    def position2(self) -> Vec2:
        """Returns position2."""
        return Vec2(self._x2, self._y2)

    @position2.setter
    def position2(self, point: Vec2) -> None:
        """Sets position2."""
        tmp_t = self._t
        self._t = 1
        self._x2, self._y2 = point
        self._update_vertices()
        self._update_translation()
        self.t = tmp_t

    @property
    def t(self) -> float:
        """For getting which proportion of the line is drawn."""
        return self._t

    @t.setter
    def t(self, new_t: float) -> None:
        """
        For changing the proportions t that is drawn.
        t has to be between 0 and 1.
        """
        assert 0 <= new_t <= 1, f"t must be between 0 and 1, is {new_t}"
        if new_t == 0:
            self.visible = False
        else:
            delta = (self.position2 - self.position)/self._t
            self._x2, self._y2 = self.position + delta*new_t
            self._t = new_t
            self._update_vertices()
            self._update_translation()
            self.visible = True

    def fraction_of_point(self, point: Vec2) -> float:
        """
        Computes the fraction of the line the input point has covert.
        """
        # formula for projection
        n = (self.position2 - self.position).normalize()
        projection = self.position + n*n.dot(point - self.position)
        # get fraction from projection
        return abs(projection - self.position)/abs(self.position2-self.position)

class RectangleWithText(RoundedRectangle):
    """Draws a rectangle with rounded corners and centered text."""
    def __init__(
        self, 
        start_position: Vec2,
        end_position: Vec2,
        radius: _RadiusT | tuple[_RadiusT, _RadiusT, _RadiusT, _RadiusT],
        text: Optional[str] = None, 
        font_size: Optional[int] = None,
        color: Tuple[int, int, int, int] = (255, 255, 255, 255),
        batch: Batch = None,
        group: Group = None
    ) -> None:
        height = end_position.y - start_position.y
        super().__init__(x=start_position.x, y=start_position.y, width=end_position.x - start_position.x, height=height, radius=radius, color=color, batch=batch, group=group)
        if text:
            # set a minimal font size of 10
            font_size = max(10, font_size)
            self._label = Label(text, font_name="Roboto", font_size=font_size, color=font_color, x=start_position.x, y=start_position.y + (height - font_size)/2, width=int(self._width), height=font_size, align="center", bold=True, batch=batch, group=group, anchor_y="baseline")
        self._text = text
        self._font_size = font_size

    @property
    def text(self) -> Optional[str]:
        """Returns text if set."""
        return self._text

    @text.setter
    def text(self, new_text: str) -> None:
        """Sets text, assumes font size is set."""
        if self._text:
            self._label.text = new_text
            self._text = new_text
        else:
            if self._font_size is not None:
                self._label = Label(new_text, font_name="Roboto", font_size=self._font_size, color=font_color, x=self._x, y=self._y + (self._height - self._font_size)/2, width=int(self._width), height=self._font_size, align="center", bold=True, batch=self._batch, group=self._group, anchor_y="baseline")
                self._text = new_text
            else:
                raise ValueError("Font size needed to create new text, please set font size first.")

    @property
    def position(self) -> Vec2:
        """Returns position."""
        return Vec2(self._x, self._y)

    @position.setter
    def position(self, point: Vec2) -> None:
        """Sets position and updates text accordingly."""
        old_x = self._x
        old_y = self._y
        self._x, self._y = point.x, point.y
        self._width = old_x + self._width - point.x
        self._height = old_y + self._height - point.y
        self._update_translation()
        self._update_vertices()
        if self._text:
            self._label.x, self._label.y = point.x, point.y
            self._label.width = self._width

    @property
    def position2(self) -> Vec2:
        """Returns position2."""
        return Vec2(self._x + self._width, self._y + self._height)

    @position2.setter
    def position2(self, point: Vec2) -> None:
        """Sets position2 and updates text accordingly."""
        self._width = point.x - self._x
        self._height = point.y - self._y
        self._update_translation()
        self._update_vertices()
        if self._text:
            self._label.width = self._width

    @property
    def visible(self) -> bool:
        """Returns visibility."""
        return super(RectangleWithText, RectangleWithText).visible.__get__(self)

    @visible.setter
    def visible(self, value: bool) -> None:
        """Sets visibility"""
        super(RectangleWithText, RectangleWithText).visible.__set__(self, value)
        if self._text:
            self._label.visible = value

    @property
    def font_size(self) -> Optional[int]:
        """Returns font size if set."""
        return self._font_size

    @font_size.setter
    def font_size(self, new_font_size: int) -> None:
        """Sets font size."""
        # set a minimal font size of 10
        # too small font sizes can cause errors on windows
        new_font_size = max(10, new_font_size)
        if self._font_size is not None:
            self._label.font_size = new_font_size
            self._label.height = new_font_size
            self._label.y = self.position.y + (self.height - new_font_size)/2
        self._font_size = new_font_size


    def delete(self) -> None:
        """Removes all pyglet components from video memory"""
        if self._text:
            self._label.delete()
        super().delete()

class Arrow:
    """Arrow shape, composed from a line and a triangle."""
    def __init__(
        self, 
        start_position: Vec2, # end of arrow without head
        end_position: Vec2, # end of arrow where the head is located
        beta: float = 2.0, # size of head relative to with of line
        width: float = 1.0, # with of the line
        color: Tuple[int, int, int, int] = (255, 255, 255, 255), 
        batch: Batch = None, 
        group: Group = None
    ) -> None:
        self._position = start_position
        self._position2 = end_position
        self._width = width
        delta = (end_position - start_position).normalize()
        normal = Vec2(delta.y, -delta.x)
        self._body = Line(start_position=start_position, end_position=end_position - delta*width*beta, width=width, color=color, batch=batch, group=group)

        self._beta = beta

        # draw triangle as arrow peak
        # triangle is spanned by points a, b, c, where a is the end_position
        b = end_position + normal*width*beta - delta*width*beta
        c = end_position - normal*width*beta - delta*width*beta
        self._head: Triangle = Triangle(end_position, b, c, color, batch, group)

    @property
    def visible(self) -> bool:
        """Returns whether the arrow is visible."""
        return self._body.visible and self._head.visible

    @visible.setter
    def visible(self, value: bool) -> None:
        """Sets the visibility of the arrow."""
        self._body.visible = value
        self._head.visible = value

    @property
    def position(self) -> Vec2:
        """Returns the position of the end of the arrow without peak."""
        return self._position

    @position.setter
    def position(self, point: Vec2) -> None:
        """Sets the position of the end of the arrow without peak."""
        self._position = point
        self.update()

    @property
    def position2(self) -> Vec2:
        """Returns the position of the end of the arrow with peak."""
        return self._position2

    @position2.setter
    def position2(self, point: Vec2) -> None:
        """Sets the position of the end of the arrow with peak."""
        self._position2 = point
        self.update()

    def update(self) -> None:
        """Updates the internal arrow state after a position update."""
        delta = (self._position2 - self._position).normalize()
        normal = Vec2(delta.y, -delta.x)
        self._body.position2 = self._position2 - delta*self._width*self._beta
        self._body.position = self._position

        b = self._position2 + normal*self._width*self._beta - delta*self._width*self._beta
        c = self._position2 - normal*self._width*self._beta - delta*self._width*self._beta
        self._head.position = self._position2
        self._head.position2 = b
        self._head.position3 = c

    def flip(self) -> None:
        """Flips the arrow peak to the other side."""
        self._position, self._position2 = self._position2, self._position
        self.update()

    @property
    def polygon(self) -> List[Vec2]:
        """Returns the shape of the arrow as a polygon."""
        delta = (self._position2 - self._position).normalize()
        normal = Vec2(delta.y, -delta.x)
        points = self._body.polygon
        points = [points[0], points[1]] + [
            self._position2 + normal*self._width*self._beta - delta*self._width*self._beta,
            self._position2,
            self._position2 - normal*self._width*self._beta - delta*self._width*self._beta
            
        ] + [points[2], points[3], points[0]]
        return points

    def delete(self) -> None:
        self._body.delete()
        self._head.delete()