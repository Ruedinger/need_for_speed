# Definition of the classes used in display to show movements, preparations, questions
import math
from functools import cached_property
from enum import IntEnum
from pyglet.shapes import ShapeBase
from pyglet.graphics import Batch, Group
from pyglet.math import Vec2
from pyglet.sprite import Sprite
from pyglet.text import Label
from shapely.geometry import Point, Polygon
from shapely.ops import nearest_points
from pyglet.shapes import _RadiusT, Rectangle, _point_in_polygon
from typing import Tuple, List, Callable
from .rendering import Arrow, EllipticArc, Line, Circle, Multiline, RectangleWithText, Triangle
from resources.resources import frustration_images
from ..util.parameters import font_color, frustration_colors, frustration_highlight_colors, movement_width_m

class Movement:
    """
    Base movement class.
    Implements highlighting, progress, accuracy, visibility and whether a movement is done.
    """
    def __init__(
        self,
    ) -> None:
        # multiple hints for composed shapes
        self._hints: List[ShapeBase] = []
        # highlight which shows tth progress of the movement to the participants
        self._highlight: ShapeBase = Rectangle(0, 0, 0, 0)
        # how much the movement is completed, dependent on other movement
        self._progress: float = 0
        # at which point of the movement the user currently is
        self._fraction_of_movement_completed: float = 0
        # circle which defines when a participant has moved
        # i.e. a participant has only moved if the hand is outside the circle.
        self._start_point = Circle(Vec2(0,0), 1)
        # underlying shape of the movement
        self._true_movement: ShapeBase = Rectangle(0, 0, 0, 0)

    @property
    def visible(self) -> bool:
        """Gets visibility of the movement."""
        res = True
        for hint in self._hints:
            res &= hint.visible
        return res

    @visible.setter
    def visible(self, value: bool) -> None:
        """Sets visibility of the movement."""
        for hint in self._hints:
            hint.visible = value
        if not value:
            self._highlight.visible = False

    def __contains__(self, point: Vec2) -> bool:
        """Returns whether point is inside the shape defined by the movement."""
        polygon = self.polygon
        if _point_in_polygon(polygon, point):
            self._fraction_of_movement_completed = max(0, min(self._true_movement.fraction_of_point(point), 1))
        else:
            self._fraction_of_movement_completed = 0
        return _point_in_polygon(polygon, point)
    
    def update_progress(self, other, tolerance: float) -> None:
        """
        Updates the progress of the movement.
        Progress can only increase if other has a similar progress and the portion of the movement at which the participant is at the moment is not too big compared to progress. That is, the participant needs to do both movements at the same time and cannot skip part of the movement.
        """
        if abs(self._fraction_of_movement_completed - other._progress) < tolerance and abs(self._fraction_of_movement_completed - self._progress) < tolerance:
            self._progress = max(self._progress, self._fraction_of_movement_completed)

    def update_highlight(self):
        """Updates the highlight of the movement."""
        self._highlight.t = self._progress

    def done(self, tolerance: float) -> bool:
        """Returns whether this movement is done."""
        return math.isclose(self._progress, 1, abs_tol=tolerance)
    
    def project_point(self, point: Vec2) -> Vec2:
        """Projects point on the movement, i.e. returns the point in region defined by the movement closest to the input point."""
        poly = Polygon(self.polygon)
        point = Point(point.x, point.y)
        # The points are returned in the same order as the input geometries:
        p1, p2 = nearest_points(poly, point)
        return Vec2(p1.x, p1.y)
    
    def get_accuracy(self, point: Vec2, px_to_m_func: Callable) -> float:
        """Returns the accuracy based on point, accuracy gets lower as point gets further away from the movement."""
        if self.__contains__(point):
            return 1
        else:
            # save euclidean distance of each frame
            projection = self.project_point(point)
            distance_m = px_to_m_func(projection - point)
            # normalize to the width of movements
            normalized_distance = distance_m/movement_width_m
            # clip values to [0, 1]
            return 1 - min(max(0, normalized_distance), 1)
    
    def delete(self) -> None:
        """Removes all pyglet components from video memory"""
        for hint in self._hints:
            hint.delete()
        self._highlight.delete()
        self._start_point.delete()
        self._true_movement.delete()

class LineMovement(Movement):
    """
    A line going from the location specified by bottom left to the location specified by top right.
    Has an arrow peak at the end of the line.
    As with the other movements, the arrow peak counts as part of the movement and the participant also need to complete it to complete the movement.
    """
    def __init__(
        self, 
        bottom_left: Vec2, 
        top_right: Vec2, 
        mirrored: bool = False,
        beta: float = 1.0,
        width: float = 1.0, 
        color: Tuple[int, int, int, int] = (255, 255, 255, 255), 
        highlight_color: Tuple[int, int, int, int] = (255, 255, 255, 255), 
        batch: Batch = None, 
        movement_group: Group = None,
        highlight_group: Group = None
    ) -> None:
        super().__init__()
        self._position = bottom_left
        self._position2 = top_right
        self._width = width
        self._highlight_color = highlight_color
        self._batch = batch
        self._movement_group = movement_group
        self._highlight_group = highlight_group
        self._hints = [Arrow(start_position=bottom_left, end_position=top_right, beta=beta, width=width, color=color, batch=batch, group=movement_group)]
        # for rendering the progress in the movement
        self._highlight = Line(bottom_left, top_right, width/4, 0, highlight_color, batch, highlight_group)
        self._true_movement = Line(bottom_left, top_right, width, batch=batch, group=movement_group)
        self._true_movement.visible = False
        
        self.start_point = Circle(bottom_left, width, batch=batch, group=movement_group)
        self.start_point.visible = False

        if mirrored:
            self.flip()

    def flip(self) -> None:
        """Swaps start and end of the movement."""
        self._progress = 0
        self._hints[0].flip()
        self._position, self._position2 = self._position2, self._position
        self._highlight = Line(self._position, self._position2, self._width/4, 0, self._highlight_color, self._batch, self._highlight_group)
        self._true_movement = Line(self._position, self._position2, self._width, batch=self._batch, group=self._movement_group)
        self._true_movement.visible = False

    @cached_property
    def polygon(self) -> List[Vec2]:
        """Returns the shape of the line movement as a polygon."""
        return self._hints[0].polygon
    
    def calculate_real_world_length(self, px_to_m_func: Callable) -> float:
        """
        Returns the length of this movement in meters, at the z-position of the participant.
        This is only an estimate, but relatively good with a precision of ca. 5 cm.
        """
        return abs(px_to_m_func(self._position2 - self._position))

class ZigZagMovement(Movement):
    """Implements a zigzag movement, consisting of num_elements lines with an arrow head at the end."""
    def __init__(
        self, 
        bottom_left: Vec2, 
        top_right: Vec2, 
        num_elements: int = 3, 
        mirrored: bool = False,
        beta: float = 1.0,
        width: float = 1.0, 
        color: Tuple[int, int, int, int] = (255, 255, 255, 255), 
        highlight_color: Tuple[int, int, int, int] = (255, 255, 255, 255), 
        batch: Batch = None, 
        movement_group: Group = None,
        highlight_group: Group = None
    ) -> None:
        super().__init__()
        if mirrored:
            bottom_left.x, top_right.x = top_right.x, bottom_left.x

        y_step: float = (top_right.y - bottom_left.y)/num_elements
        x = bottom_left[0]
        x2 = top_right[0]
        self._vertices = []
        for i in range(num_elements):
            # draw lines
            start = Vec2(x, i*y_step + bottom_left.y)
            end = Vec2(x2, (i+1)*y_step + bottom_left.y)
            self._vertices.append(start)
            # create bounding boxes
            x, x2 = x2, x
        delta = (end - start).normalize()
        normal = Vec2(delta.y, -delta.x)
        self._vertices.append(end - delta*width*beta)

        self._hints.append(Multiline(*self._vertices, thickness=width, color=color, batch=batch, group=movement_group))
        # draw triangle as arrow peak
        # triangle is spanned by points end, b, c
        b = end + normal*width*beta - delta*width*beta
        c = end - normal*width*beta - delta*width*beta
        self._hints.append(Triangle(end, b, c, color=color, batch=batch, group=movement_group))
        # for highlighting the finished part of the movement
        self._vertices = self._vertices[:-1] + [end]
        self._highlight = Multiline(*self._vertices, thickness=width/4, t=0, color=highlight_color, batch=batch, group=highlight_group)
        self._true_movement = Multiline(*self._vertices, thickness=width, batch=batch, group=movement_group)
        self._true_movement.visible = False

        self.start_point = Circle(self._vertices[0], width, batch=batch, group=movement_group)
        self.start_point.visible = False

    @cached_property
    def polygon(self) -> List[Vec2]:
        """Returns the shape of the zigzag movement as a polygon."""
        line_points = self._hints[0].polygon
        triangle_points = self._hints[1].polygon
        if len(line_points)-1 % 2 == 1:
            raise Exception("This should not happen")
        half = int((len(line_points)-1)/2)
        return line_points[:half] + [triangle_points[1], triangle_points[0], triangle_points[2]] + line_points[half:] + [line_points[0]]
    
    def calculate_real_world_length(self, px_to_m_func: Callable) -> float:
        """
        Returns the length of this movement in meters, at the z-position of the participant.
        This is only an estimate, but relatively good with a precision of ca. 5 cm.
        """
        result = 0
        for i in range(len(self._vertices)-1):
            result += abs(px_to_m_func(self._vertices[i+1] - self._vertices[i]))
        return result

class EllipseMovement(Movement):
    """Implements an ellipse like movement with a arrow head at the end."""
    def __init__(
        self, 
        bottom_left: Vec2,
        top_right: Vec2,
        mirrored: bool = False,
        beta: float = 1.0,
        angle: float = math.pi*3/2,
        width: float = 1.0, 
        color: Tuple[int, int, int, int] = (255, 255, 255, 255), 
        highlight_color: Tuple[int, int, int, int] = (255, 255, 255, 255), 
        batch: Batch = None, 
        movement_group: Group = None,
        highlight_group: Group = None
    ) -> None:
        super().__init__()
        if mirrored:
            angle *= -1

        # create shapes
        position = (bottom_left+top_right)/2
        a = (top_right.x - bottom_left.x)/2
        b = (top_right.y - bottom_left.y)/2

        self._highlight: EllipticArc = EllipticArc(
            position=position, 
            a=a, 
            b=b, 
            segments=200, 
            angle=angle,
            width=width/4, 
            t=0, 
            color=highlight_color, 
            batch=batch, 
            group=highlight_group
        )

        self._true_movement = EllipticArc(
            position=position, 
            a=a, 
            b=b, 
            angle=angle,
            width=width, 
            batch=batch, 
            group=highlight_group
        )
        self._true_movement.visible = False

        small_angle = angle - width*beta/self._highlight.circumference*math.tau
        ellipse = EllipticArc(
            position=position, 
            a=a, 
            b=b, 
            segments=200, 
            angle=small_angle,
            width=width, 
            color=color, 
            batch=batch, 
            group=movement_group
        )
        
        # draw triangle as arrow peak
        # triangle starts where ellipse ends and follows the arc of the ellipse
        # the triangle is part of the movement
        end_of_ellipse = self._highlight.point_on_ellipse(small_angle + ellipse.start_angle)
        relative_point = end_of_ellipse - position
        tangent = Vec2(1, (-relative_point.x*b**2)/(relative_point.y*a**2))
        normal = Vec2(-tangent.y, tangent.x).normalize()
        pos1: Vec2 = self._highlight.point_on_ellipse(self._highlight.start_angle + angle)
        pos2: Vec2 = end_of_ellipse + normal*width*beta
        pos3: Vec2 = end_of_ellipse - normal*width*beta
        head = Triangle(pos1, pos2, pos3, color=color, batch=batch, group=movement_group)

        self._hints = [ellipse, head]

        self.start_point = Circle(position-Vec2(0, b), width, batch=batch, group=movement_group)
        self.start_point.visible = False
    
    @cached_property
    def polygon(self) -> List[Vec2]:
        """Returns the shape of the ellipse movement as a polygon."""
        ellipse_points = self._hints[0].polygon
        triangle_points = self._hints[1].polygon
        if len(ellipse_points)-1 % 2 == 1:
            raise Exception("This should not happen")
        half = int((len(ellipse_points)-1)/2)
        return ellipse_points[:half] + [triangle_points[1], triangle_points[0], triangle_points[2]] + ellipse_points[half:] + [ellipse_points[0]]
    
    def calculate_real_world_length(self, px_to_m_func: Callable) -> float:
        """
        Returns the length of this movement in meters, at the z-position of the participant.
        This is only an estimate, but relatively good with a precision of ca. 5 cm.
        """
        result = 0
        points = self._highlight.center_points
        for i in range(len(points)-1):
            result += abs(px_to_m_func(Vec2(points[i+1][0] - points[i][0], points[i+1][1] - points[i][1])))
        return result

class Preparation:
    """Implements a preparation, which is represented a rectangle with a L or R in the middle on the screen."""
    def __init__(
        self,
        bottom_left: Vec2,
        top_right: Vec2,
        radius: _RadiusT | tuple[_RadiusT, _RadiusT, _RadiusT, _RadiusT],
        left_hand: bool,
        color: Tuple[int, int, int, int] = (255, 255, 255, 255), 
        highlight_color: Tuple[int, int, int, int] = (255, 255, 255, 255), 
        batch: Batch = None,
        group: Group = None
    ) -> None:
        self._hint = RectangleWithText(bottom_left, top_right, radius, "L" if left_hand else "R", top_right.y-bottom_left.y, color, batch, group)
        self._highlight = RectangleWithText(bottom_left, top_right, radius, None, None, color=highlight_color, batch=batch, group=group)

    @property
    def bottom_left(self) -> Vec2:
        """Returns location of the bottom left corner of the preparation."""
        return self._hint.position

    @bottom_left.setter
    def bottom_left(self, new_bottom_left: Vec2) -> None:
        """Sets the location of the bottom left corner of the preparation."""
        self._hint.position = new_bottom_left
        self._hint.font_size = abs(self.top_right.y - new_bottom_left.y)
        self._highlight.position = new_bottom_left

    @property
    def top_right(self) -> Vec2:
        """Returns the location of the top right corner of the preparation."""
        return self._hint.position2

    @top_right.setter
    def top_right(self, new_top_right: Vec2) -> None:
        """Sets the location of the top right corner of the preparation."""
        self._hint.position2 = new_top_right
        self._hint.font_size = abs(new_top_right.y - self.bottom_left.y)
        self._highlight.position2 = new_top_right

    @property
    def visible(self) -> bool:
        """Returns visibility of this preparation."""
        return self._hint.visible

    @visible.setter
    def visible(self, value: bool) -> None:
        """Sets visibility for this preparation."""
        self._hint.visible = value
        if not value:
            self._highlight.visible = False

    def __contains__(self, point: Vec2) -> bool:
        """Returns whether point lies inside the shape of this preparation."""
        self._highlight.visible = point in self._hint

        return point in self._hint

    def delete(self) -> None:
        """Removes all pyglet components from video memory"""
        self._hint.delete()
        self._highlight.delete()

class Frustration(IntEnum):
    """Specifies possible frustrations levels for the in-session frustration questions."""
    NO_FRUSTRATION = 0
    LITTLE_FRUSTRATION = 1
    MEDIUM_FRUSTRATION = 2
    HIGH_FRUSTRATION = 3
    MAXIMAL_FRUSTRATION = 4

class QuestionItem:
    """
    Implements a question item for the in-session frustration questions. 
    Possible frustrations levels are specified by the Frustration class.
    """
    frustration_descriptions = [
        "No Frustration",
        "Low Frustration",
        "Medium Frustration",
        "High Frustration",
        "Maximal Frustration"
    ]
    def __init__(
        self,
        bottom_left: Vec2,
        top_right: Vec2,
        radius: _RadiusT | tuple[_RadiusT, _RadiusT, _RadiusT, _RadiusT],
        frustration_level: Frustration,
        batch: Batch = None,
        background_group: Group = None,
        image_group: Group = None
    ) -> None:
        label_height = (top_right.x - bottom_left.x)/10
        self._label = Label(self.frustration_descriptions[frustration_level], font_name="Roboto", font_size=(top_right.x - bottom_left.x)/15, bold=True, color=font_color, x=bottom_left.x, y=bottom_left.y + label_height/2, width=top_right.x - bottom_left.x, height=label_height, align="center", batch=batch, group=image_group)
        image = frustration_images[frustration_level]
        sprite = Sprite(image, bottom_left.x, bottom_left.y + 2*label_height, batch=batch, group=image_group)
        sprite.update(scale_x=(top_right.x - bottom_left.x)/image.width, scale_y=(top_right.y - bottom_left.y - 2*label_height)/image.height)
        self._hints = [RectangleWithText(bottom_left, top_right, radius, None, None, frustration_colors[frustration_level], batch, background_group), sprite]
        self._highlight = RectangleWithText(bottom_left, top_right, radius, None, None, frustration_highlight_colors[frustration_level], batch, background_group)

    @property
    def visible(self) -> bool:
        """Return whether this question item is visible."""
        res = True
        for hint in self._hints:
            res &= hint.visible
        return res

    @visible.setter
    def visible(self, value: bool) -> None:
        """Sets visibility for this question item."""
        for hint in self._hints:
            hint.visible = value
        self._label.visible = value
        if not value:
            self._highlight.visible = False

    def __contains__(self, point: Vec2) -> bool:
        """Returns whether point lies inside the shape of this question item."""
        self._highlight.visible = point in self._hints[0]

        return point in self._hints[0]

    def delete(self) -> None:
        """Removes all pyglet components from video memory"""
        for hint in self._hints:
            hint.delete()
        self._label.delete()
        self._highlight.delete()