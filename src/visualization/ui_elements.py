# contains many elements of the graphical user interface
from collections import deque
from math import pi, floor
from statistics import fmean
from typing import Tuple, List, Dict, Iterable
from pyglet.graphics import Batch, Group
from pyglet.text import Label, HTMLLabel, DocumentLabel
from pyglet.math import Vec2
from pyglet.sprite import Sprite
from pyglet.media import Player
from pyglet.shapes import Circle, RoundedRectangle, Arc
from overrides import override
from ..util.parameters import margin, font_color, skeleton_line_color, skeleton_point_color, seconds_per_meter, mode, ApplicationMode
from ..util.logging_config import logger
from .rendering import Circle as MyCircle, Line, RectangleWithText
from ..util.interfaces import Subject, PeriodicTask, Observer
from ..logic.model import Model
from ..logic.application_state import ApplicationState
from ..util.matcher import Matcher
from ..logic.notification import Reset, Pause, OutOfTime, StateUpdate
from resources.resources import skeleton_down, skeleton_middle, skeleton_up, clock_sound, error_sound
from ..logic.skeleton_kinematics import num_of_keypoints, keypoints_to_indices
from ..logic.data_collector import DataCollector
from ..logic.instruction import MovementType
from ..logic.timer import Timer
from ..util.constants import base_to_n, n_to_base

class ProgressBar:
    """A progress bar for visualizing a number between 0 and 1."""
    def __init__(
        self,
        x: float, 
        y: float, 
        width: float, 
        height: float, 
        border_radius: float,
        offset: float = 5,
        border_color: Tuple[int, int, int, int] = (255, 255, 255, 255), 
        batch: Batch = None, 
        group: Group = None,
        display_group: Group = None
    ) -> None:
        self._height = height
        self._border = RoundedRectangle(x=x, y=y, width=width, height=height, radius=border_radius, segments=200, color=border_color, batch=batch, group=group)
        self._center = RoundedRectangle(x=x + offset, y=y + offset, width=width - 2*offset, height=height - 2*offset, radius=border_radius, segments=200, color=(255, 255, 255, 255), batch=batch, group=group)
        self._display = RoundedRectangle(x=x + offset, y=y + offset, width=width - 2*offset, height=height - 2*offset, radius=border_radius, segments=200, color=(0, 255, 0, 255), batch=batch, group=display_group)
        font_size = width/3
        self._progress_display = Label("100%", font_name="Roboto", font_size=width/5, color=font_color, x=x, y=y + font_size/2, width=width, height=height, align="center", anchor_y="center", batch=batch, group=display_group)
        self._progress = 1
        self._border_radius = border_radius
        self._offset = offset

    @property
    def progress(self) -> float:
        """Returns the current progress."""
        return self._progress

    @progress.setter
    def progress(self, new_progress) -> None:
        """Sets a new progress."""
        assert 0 <= new_progress <= 1, f"progress should be between 0 and 1, is {new_progress}"
        self._progress = new_progress
        self._display.height = new_progress*(self._height -2*self._offset)
        self._display.visible = self._display.height > self._border_radius
        self._progress_display.text = f"{new_progress*100:.0f}%"
        self._display.color = (int((1-new_progress)*255), int(new_progress*255), 0, 255)

    @property
    def visible(self) -> bool:
        """Returns whether the progress bar is visible."""
        return self._border.visible

    @visible.setter
    def visible(self, value: bool) -> None:
        """Sets the visibility of the progress bar."""
        self._border.visible = value
        self._center.visible = value
        self._display.visible = value
        self._progress_display.visible = value

accuracy_matcher = Matcher()

class AccuracyDisplay(Observer, PeriodicTask):
    """
    Displays the accuracy of the participant if needed.
    Accuracy for each hand is displayed separately.
    """
    def __init__(
        self, 
        x: float,
        y: float,
        width: float,
        height: float,
        border_radius: float,
        data_collector: DataCollector,
        model: Model,
        color: Tuple[int, int, int, int] = (255, 255, 255, 255), 
        batch: Batch = None, 
        group: Group = None,
        display_group: Group = None
    ) -> None:
        self._data_collector = data_collector
        self._model = model
        left_display = ProgressBar(x=x + width/8, y=y + height/5 + margin, width=width/4, height=height*4/5 -margin, border_radius=border_radius, offset=5, border_color=(0, 0, 0, 255), batch=batch, group=group, display_group=display_group)
        right_display = ProgressBar(x=x + width*5/8, y=y + height/5 + margin, width=width/4, height=height*4/5 -margin, border_radius=border_radius, offset=5, border_color=(0, 0, 0, 255), batch=batch, group=group, display_group=display_group)
        self._displays = [left_display, right_display]
        left_label = Label("Left", font_name="Roboto", font_size=width/10, color=font_color, x=x+width/4, y=y+height/10, width=width//4, height=height/10, align="center", batch=batch, group=group, anchor_x="center", anchor_y="bottom")
        right_label = Label("Right", font_name="Roboto", font_size=width/10, color=font_color, x=x+width*3/4, y=y+height/10, width=width//4, height=height/10, align="center", batch=batch, group=group, anchor_x="center", anchor_y="bottom")
        center_label = Label("Accuracy", font_name="Roboto", font_size=width/10, color=font_color, x=x, y=y, width=width, height=height/10, align="center", batch=batch, group=group, bold=True, anchor_y="bottom")
        self._background = RoundedRectangle(x=x, y=y, width=width, height=height*1/5, radius=border_radius, segments=200, color=color, batch=batch, group=group)
        self._text_elements = [left_label, right_label, center_label]
        self._accuracy_multiplier: float = 1.0
        self._was_correct: bool = False

    @accuracy_matcher.base_case
    def update(self, subject: Subject) -> None:
        """Base update method for notifications."""
        raise ValueError(f"Expected subject of type StateUpdate, Reset, or OutOfTime, got {type(subject)}")

    def process(self) -> None:
        """
        Updates the displayed accuracies according to the accuracies last collected by the data collector. 
        This accounts for latency, as the accuracies are also updated with delay.
        """
        current_accuracies = self._data_collector.accuracies
        discounted_accuracies = current_accuracies[0]*self._accuracy_multiplier, current_accuracies[1]*self._accuracy_multiplier
        self._displays[0].progress, self._displays[1].progress = discounted_accuracies
        if fmean(discounted_accuracies) < 0.7 and self._was_correct:
            error_sound.play()
        self._was_correct = fmean(discounted_accuracies) >= 0.7

    @accuracy_matcher.case(lambda self, subject: isinstance(subject, Reset))
    def reset_update(self, subject: Reset) -> None:
        """Resets the state of this instance of the AccuracyDisplay."""
        self.visible = False
        self._displays[0].progress, self._displays[1].progress = 0, 0
        self._accuracy_multiplier = 1.0
        self._was_correct = False

    @accuracy_matcher.case(lambda self, subject: isinstance(subject, OutOfTime))
    def time_update(self, subject: OutOfTime) -> None:
        """For lowering the displayed accuracy when the participant is too slow."""
        self._accuracy_multiplier = 1 - subject.badness

    @accuracy_matcher.case(lambda self, state: isinstance(state, StateUpdate))
    def state_update(self, state: StateUpdate) -> None:
        """Sets the AccuracyDisplay visible in the right states."""
        self.visible = self._model.state in [ApplicationState.COORDINATION_TEST, ApplicationState.CONDITION]
        self._was_correct: bool = False

    @property
    def visible(self) -> bool:
        """Returns whether the accuracy display is visible."""
        return self._background.visible

    @visible.setter
    def visible(self, new_visible: bool) -> None:
        """Sets the visibility of the accuracy display."""
        if new_visible != self.visible:
            for label in self._text_elements:
                label.visible = new_visible
            for display in self._displays:
                display.visible = new_visible
            self._background.visible = new_visible

class FinalScreen(Observer):
    """Final screen of the application. Informs participants of the actual purpose of the study."""
    def __init__(
        self, 
        x: float, 
        y: float, 
        width: float, 
        height: float,
        border_radius: float, 
        model: Model,
        color: Tuple[int, int, int, int] = (255, 255, 255, 255), 
        batch: Batch = None, 
        group: Group = None
    ) -> None:
        self._model = model
        self._background = RoundedRectangle(x=x, y=y, width=width, height=height, radius=border_radius, segments=200, color=color, batch=batch, group=group)
        closing_text = """
        <h1>Thanks for your participation</h1>
        <p>This was an experiment for evaluating the impact of latency on motor performance and frustration. If you are interested in the results of analysis, please contact me (Jonas).
        <p>Please do NOT inform other participants on the purpose of this study.
        """
        self._label = HTMLLabel(closing_text, x=x + margin, y=y + margin, width=width -2*margin, height=height -2*margin, anchor_x='left', anchor_y='bottom', multiline=True, batch=batch, group=group)
        self._label.set_style("font_name", "Roboto")
        self._label.set_style("font_size",  width/60)
        self._label.color = font_color

    @override
    def update(self, subject: Subject) -> None:
        """For showing the screen when the participant has performed all movements."""
        self._label.visible = self._model.state == ApplicationState.CLOSING
        self._background.visible = self._model.state == ApplicationState.CLOSING

class TitleScreen(Observer, PeriodicTask):
    """First screen of the application, shows narrative of experiment and asks for confirmation with a T-pose."""
    _experiment_description = """
    <h1>Welcome</h1>
    <p>This is an experiment to probe different body pose estimation algorithms. 
    <p>Therefore, you will perform some movements multiple times. Feedback precision of the system may depend on the algorithm.
    <p>The right display will show the instructions you have to perform. 
    <p>You control the system with your hands. Please keeping hands open and hold them parallel to the screen.
    <h1>Raise your arms to the side and put them down again to start the calibration.</h1>
    """
    _experiment_description_calibration = _experiment_description + """
    <h1>Calibration started. Please do not move.</h1>
    """
    def __init__(
        self,
        x: float, 
        y: float,
        width: float,
        height: float,
        border_radius: float,
        model: Model,
        color: Tuple[int, int, int, int] = (255, 255, 255, 255),
        batch: Batch = None, 
        group: Group = None,
        display_group: Group = None,
    ) -> None:
        logger.info("Initializing TitleScreen")
        self._width = width
        self._model = model

        self._background = RoundedRectangle(x=x, y=y, width=width, height=height, radius=border_radius, segments=200, color=color, batch=batch, group=group)
        self._experiment_description_label = HTMLLabel(self._experiment_description, x=x + margin, y=y + margin*3/2 + height/2, width=width - 2*margin, height=height/2 - 3/2*margin, anchor_x='left', anchor_y='bottom', multiline=True, batch=batch, group=group)
        self.update_label(self._experiment_description_label)

        # one margin for the left and right border, respectively
        # one margin between the skeleton and the clock
        # also have one margin between the text on top and the bottom part
        picture_scale = min(width/2 - 3/2*margin, height/2 - 3/2*margin)
        half_width = (width - 3*margin)/2
        total_width = width - 3*margin
        skeleton_1 = Sprite(skeleton_down, x + margin + (half_width - picture_scale)/2, y + margin, batch=batch, group=group)
        # /500 because image size is 500x500
        skeleton_1.update(scale_y=picture_scale/500)
        skeleton_1.visible = False
        skeleton_2 = Sprite(skeleton_middle, x + margin + (half_width - picture_scale)/2, y + margin, batch=batch, group=group)
        skeleton_2.update(scale_y=picture_scale/500)
        skeleton_2.visible = False
        skeleton_3 = Sprite(skeleton_up, x + margin + (half_width - picture_scale)/2, y + margin, batch=batch, group=group)
        skeleton_3.update(scale_y=picture_scale/500)
        skeleton_3.visible = False
        self._skeleton_sprites = [skeleton_1, skeleton_2, skeleton_3]

        self._clock = Clock(x=x + total_width*3/4 + 2*margin, y=y + margin + picture_scale/2, border_width=0, display_width=picture_scale/16, total_radius=picture_scale/2, maximal_time=10, count_down=True, batch=batch, group=group)
        self._clock.visible = False

    def update_label(self, label: DocumentLabel) -> None:
        """Updates label style."""
        label.set_style("font_name", "Roboto")
        label.set_style("font_size",  self._width/60)
        label.color = font_color

    def process(self) -> None:
        """
        For updating the visuals according to the current ApplicationState, Instruction.
        """
        instruction_timer = self._model.instruction_timer
        current_instruction = self._model.current_instruction

        for i in range(len(self._skeleton_sprites)):
            # -4 because DOWN das index 4 in the MovementType enum
            if i == current_instruction.movements[0] - 4:
                self._skeleton_sprites[i].visible = True
            else:
                self._skeleton_sprites[i].visible = False
        if instruction_timer.is_running:
            self._clock.update_time(instruction_timer.remaining_time_s)
        if self._model.state == ApplicationState.CALIBRATION:
            self._clock.visible = current_instruction.timer != -1
            if current_instruction.timer == -1:
                if self._experiment_description_label.text != self._experiment_description:
                    self._experiment_description_label.text = self._experiment_description
                    self.update_label(self._experiment_description_label)
            else:
                if self._experiment_description_label.text != self._experiment_description_calibration:
                    self._experiment_description_label.text = self._experiment_description_calibration
                    self.update_label(self._experiment_description_label)

    def update(self, subject: Subject) -> None:
        """Sets instance visible in the right states."""
        self.visible = self._model.state == ApplicationState.CALIBRATION

    @property
    def visible(self) -> bool:
        """Returns whether the title screen is visible."""
        return self._background.visible

    @visible.setter
    def visible(self, new_visible: bool) -> None:
        """For setting visibility of the title screen."""
        if new_visible != self.visible:
            self._background.visible = new_visible
            self._experiment_description_label.visible = new_visible
            self._clock.visible = new_visible
            for i in range(3):
                self._skeleton_sprites[i].visible = new_visible

class Clock:
    """Displays a clock and plays sound when needed"""

    _stop_watch_description = """
    <h1>Required time: {:.0f} s</h1>
    """
    _timer_description = """
    <h1>Remaining time: {:.0f} s</h1>
    """

    def __init__(
        self, 
        x: float,
        y: float,
        border_width: float,
        display_width: float,
        total_radius: float,
        maximal_time: float,
        count_down: bool,
        foreground_color: Tuple[int, int, int, int] = (255, 255, 255, 255),
        background_color: Tuple[int, int, int, int] = (0, 0, 0, 255),
        alert_color: Tuple[int, int, int, int] = (255, 0, 0, 255),
        batch: Batch = None,
        group: Group = None,
    ) -> None:
        assert total_radius > border_width + display_width
        self._player = Player()
        self._player.play()
        self._last_update: int = 0
        self._sound_speed: float = 1
        
        self._inner_radius = total_radius - border_width - display_width
        self._seconds_label = HTMLLabel("Text is being loaded\nThis should go fast", width=self._inner_radius*2, height=self._inner_radius, x=x-self._inner_radius, y=y-self._inner_radius/2, anchor_x='left', anchor_y='bottom', multiline=True, batch=batch, group=group)
        self.update_label_style(self._seconds_label)
        self._background = Circle(x=x, y=y, radius=total_radius, segments=200, color=background_color, batch=batch, group=group)
        self._display = Arc(x=x, y=y, radius=total_radius - border_width - display_width/2, segments=200, thickness=display_width, angle=0, start_angle=pi/2, color=(0, 255, 0, 255), batch=batch, group=group)
        self._foreground = Circle(x=x, y=y, radius=self._inner_radius, segments=200, color=foreground_color, batch=batch, group=group)
        
        # for alter effect
        self._foreground_color = foreground_color
        self._alert_color = alert_color
        
        if count_down:
            self._current_label = self._timer_description
        else:
            self._current_label = self._stop_watch_description

        self.maximal_time = maximal_time
        self._count_down = count_down

    def update_label_style(self, label: HTMLLabel) -> None:
        """Sets style of label"""
        label.set_style("font_name", "Roboto")
        label.color = font_color
        label.font_size = self._inner_radius/4

    @property
    def count_down(self) -> bool:
        """Returns whether the clock counts down or up."""
        return self._count_down

    @count_down.setter
    def count_down(self, new_count_down) -> None:
        """Sets whether the clock should count down or up."""
        self._count_down = new_count_down
        if new_count_down:
            self._current_label = self._timer_description
        else:
            self._current_label = self._stop_watch_description

    def update_time(self, new_time: float) -> None:
        """
        Updates the rendered time based on the input time.
        new_time should be in seconds.
        This function should be updated continuously, otherwise sound might not work or be misaligned.
        """
        new_text = self._current_label.format(floor(new_time))
        if new_text != self._seconds_label.text:
            self._seconds_label.text = new_text
            self.update_label_style(self._seconds_label)

        progress = max(0, min(new_time/self.maximal_time, 1))
        self._display.color = (int(progress*255), int((1-progress)*255), 0, 255)
        self._display.angle = -2*pi*progress

        if abs(floor(new_time*base_to_n) -  self._last_update) > + base_to_n/self._sound_speed:
            self._last_update = floor(new_time*base_to_n)
            self._player.queue(clock_sound)
            self._player.play()
            if progress == 1 and not self._count_down:
                if self._foreground.color == self._foreground_color:
                    self._foreground.color = self._alert_color
                else:
                    self._foreground.color = self._foreground_color

    @property
    def sound_speed(self) -> float:
        """Returns sound speed, which indicates sound interval and pitch."""
        return self._sound_speed

    @sound_speed.setter
    def sound_speed(self, new_speed: float) -> None:
        """
        Updates sound speed and pitch.
        Uses linear interpolation to set pitch, assuming pitch is 1 when speed is 1 and pitch is 1,5 when speed is 2.
        """
        self._player.pitch = 1 + new_speed/4
        self._sound_speed = new_speed

    def reset(self) -> None:
        """Resets instance state."""
        self._last_update = 0
        self.current_time = 0
        self._foreground.color = self._foreground_color

    @property
    def visible(self) -> bool:
        """Returns whether the clock is visible."""
        return self._foreground.visible

    @visible.setter
    def visible(self, new_visible: bool) -> None:
        """Sets the visibility of the clock instance."""
        self._seconds_label.visible = new_visible
        self._foreground.visible = new_visible
        self._display.visible = new_visible
        self._background.visible = new_visible

panel_matcher = Matcher()

class DebugInfo(Observer):
    """Displays additional information about instructions in ApplicationsState.DEBUG."""
    debug_information = """
    <p><font size=+2>Movement Id: {}</font></p>
    <p><font size=+2>Difficulty: {}</font></p>
    <p><font size=+2>Latency: {}</font></p>
    <p><font size=+2>Movements: {}</font></p>
    <p><font size=+2>Mirrored: {}</font></p>
    <p><font size=+2>Real world movement size: {}</font></p>
    """
    def __init__(
        self, 
        x: float, 
        y: float, 
        width: float, 
        height: float, 
        border_radius: float, 
        model: Model,
        color: Tuple[int, int, int, int] = (255, 255, 255, 255), 
        batch: Batch = None, 
        group: Group = None,
    ) -> None:
        self._model = model

        self._background = RoundedRectangle(x=x, y=y, width=width, height=height, radius=border_radius, segments=200, color=color, batch=batch, group=group)
        width = width -2*margin
        height = height -2*margin
        x = x + margin
        y = y + margin
        self._debug_label = HTMLLabel("", width=width, height=height, x=x, y=y, anchor_x='left', anchor_y='bottom', multiline=True, batch=batch, group=group)
        self._last_state: ApplicationState = None

    def update_label_style(self, label: HTMLLabel) -> None:
        """Sets style of label"""
        label.set_style("font_name", "Roboto")
        label.color = font_color

    def update(self, subject: Subject) -> None:
        """Updates the on state change."""
        state = self._model.state
        if state != self._last_state:
            self._last_state = state
            match state:
                case ApplicationState.CALIBRATION:
                    self._debug_label.visible = False
                    self._background.visible = False
                case ApplicationState.FAMILIARIZE:
                    movements = self._model.current_instruction.movements
                    self._debug_label.text = self.debug_information.format(self._model.instruction_index, "", "", str(MovementType(movements[0]).name) +"\n"+ str(MovementType(movements[1]).name), self._model.current_instruction.mirrored, self._model.real_world_movement_size)
                    self._debug_label.visible = True
                    self._background.visible = True
                case ApplicationState.COORDINATION_TEST:
                    movements = self._model.current_instruction.movements
                    self._debug_label.text = self.debug_information.format(self._model.instruction_index, "", "", str(MovementType(movements[0]).name) +"\n"+ str(MovementType(movements[1]).name), self._model.current_instruction.mirrored, self._model.real_world_movement_size)
                    self._debug_label.visible = True
                    self._background.visible = True
                case ApplicationState.CONDITION:
                    movements = self._model.current_instruction.movements
                    self._debug_label.text = self.debug_information.format(self._model.instruction_index, self._model.current_instruction.difficulty, self._model.current_instruction.latency, str(MovementType(movements[0]).name) +"\n"+ str(MovementType(movements[1]).name), self._model.current_instruction.mirrored, self._model.real_world_movement_size)
                    self._debug_label.visible = True
                    self._background.visible = True
                case ApplicationState.PREPARATION:
                    self._debug_label.visible = False
                    self._background.visible = False
                case ApplicationState.QUESTIONNAIRE:
                    self._debug_label.visible = False
                    self._background.visible = False
            self.update_label_style(self._debug_label)

class Panel(Observer, PeriodicTask):
    """Panel on the right side of the screen. Displays timer, written instructions."""
    # define written descriptions for all movements in HTML
    single_handed_instruction = """
    <h1><strong><font size=+3>Instructions</font></strong></h1>
    <p><font size=+3>{}</font></p>
    """
    two_handed_instruction = """
    <h1><strong><font size=+3>Instructions</font></strong></h1>
    <p><font size=+3><strong>Left arm: </strong>{}</font></p>
    <p><font size=+3><strong>Right arm: </strong>{}</font></p>
    """
    zigzag = "Move your hand in a zigzag"
    ellipse = "Move your hand in an ellipse"
    horizontal_line = "Move your hand horizontal along the arrow"
    vertical_line = "Move your hand vertical along the arrow"
    priming = "Put your hands in the respective boxes to start the next exercise. Only start the next exercise if you feel ready."
    questionnaire = "Lower your arms to start the questionnaire. \nPlease give your current frustrations level. To select, please hover with your right hand over the respective rectangle and raise your left hand"

    # indices need to map to the MovementType definitions
    movement_descriptions: List[str] = [zigzag, ellipse, horizontal_line, vertical_line]
    def __init__(
        self, 
        x: float, 
        y: float, 
        width: float, 
        height: float, 
        border_radius: float, 
        data_collector: DataCollector,
        model: Model,
        color: Tuple[int, int, int, int] = (255, 255, 255, 255), 
        batch: Batch = None, 
        group: Group = None,
        display_group: Group = None
    ) -> None:
        self._data_collector = data_collector
        self._model = model

        self._background = RoundedRectangle(x=x, y=y, width=width, height=height, radius=border_radius, segments=200, color=color, batch=batch, group=group)
        width = width -2*margin
        height = height -2*margin
        x = x + margin
        y = y + margin

        self._clock = Clock(x=x+width/2, y=y + height/3 - width/2, border_width=0, display_width=width/16, total_radius=width/2, maximal_time=30, count_down=True, batch=batch, group=group)

        self._current_instruction_text: str = None
        self._label = HTMLLabel("Text is being loaded\nThis should go fast", width=width, height=height*2/3, x=x, y=y+height/3, anchor_x='left', anchor_y='bottom', multiline=True, batch=batch, group=group)
        self.update_label_style(self._label)

    def update_instruction_description(self, new_instruction_text: str) -> None:
        """Updates instruction description if needed."""
        if new_instruction_text != self._current_instruction_text:
            self._current_instruction_text = new_instruction_text
            self._label.text = new_instruction_text
            self.update_label_style(self._label)

    def update_label_style(self, label: HTMLLabel) -> None:
        """Sets style of label"""
        label.set_style("font_name", "Roboto")
        label.color = font_color

    @property
    def visible(self) -> bool:
        """Returns whether the panel is visible."""
        return self._background.visible

    @visible.setter
    def visible(self, new_visible: bool) -> None:
        """Sets the visibility of the panel."""
        self._background.visible = new_visible
        self._label.visible = new_visible
        self._clock.visible = new_visible
    
    @panel_matcher.base_case
    def update(self, subject: Subject) -> None:
        """Base update method for notifications."""
        raise ValueError(f"Expected subject of type OutOfTime or StateUpdate. Got {type(subject)}")

    def process(self) -> None:
        """
        For updating the panel with new descriptions if needed.
        """
        match self._model.state:                
            case ApplicationState.COORDINATION_TEST:
                self._clock.maximal_time = self._model.current_instruction.timer*n_to_base
                movements = self._model.current_instruction.movements
                self.update_instruction_description(self.two_handed_instruction.format(self.movement_descriptions[movements[0]], self.movement_descriptions[movements[1]]))
                self._clock.count_down = self._model.current_instruction.timer != -1
                self._clock.update_time(self._model.instruction_timer.remaining_time_s)
            case ApplicationState.FAMILIARIZE:
                self._clock.visible = False
                movements = self._model.current_instruction.movements
                self.update_instruction_description(self.two_handed_instruction.format(self.movement_descriptions[movements[0]], self.movement_descriptions[movements[1]]))
            case ApplicationState.CONDITION:
                self._clock.maximal_time = self._model.real_world_movement_size*seconds_per_meter
                movements = self._model.current_instruction.movements
                self.update_instruction_description(self.two_handed_instruction.format(self.movement_descriptions[movements[0]], self.movement_descriptions[movements[1]]))
                self._clock.count_down = self._model.current_instruction.timer != -1
                self._clock.update_time(self._data_collector.time_in_movement*n_to_base)
            case ApplicationState.PREPARATION:
                self._clock.visible = False
                self.update_instruction_description(self.single_handed_instruction.format(self.priming))
            case ApplicationState.QUESTIONNAIRE:
                self._clock.visible = False
                self.update_instruction_description(self.single_handed_instruction.format(self.questionnaire))

    @panel_matcher.case(lambda self, subject: isinstance(subject, OutOfTime))
    def timer_update(self, subject: OutOfTime) -> None:
        """Updates clock sound speed if participant is too slow."""
        self._clock.sound_speed = 1 + subject.badness

    @panel_matcher.case(lambda self, subject: isinstance(subject, StateUpdate))
    def state_update(self, state: StateUpdate) -> None:
        """Sets instance visible in the right states."""
        self.visible = self._model.state in [ApplicationState.FAMILIARIZE, ApplicationState.COORDINATION_TEST, ApplicationState.CONDITION, ApplicationState.PREPARATION, ApplicationState.QUESTIONNAIRE]
        self._clock.reset()

center_label_matcher = Matcher()

class CenterLabel(Observer, PeriodicTask):
    """Label at the center of the screen which notifies the participant about the current state of the system."""
    def __init__(
        self, 
        x: float,
        y: float,
        width: float,
        height: float,
        radius: float,
        model: Model,
        color: Tuple[int, int, int, int] = (255, 255, 255, 255),
        batch: Batch = None,
        group: Group = None
    ) -> None:
        self._model = model

        self._label = RectangleWithText(Vec2(x, y), Vec2(x+width, y+height), radius, "Empty", font_size=30, color=color, batch=batch, group=group)
        self._label.visible = False
        self._display_timer = Timer(3*base_to_n)

    def update_label(self, new_text: str) -> None:
        """For updating the label text and setting a timer so it disappears after a while."""
        self._label.text = new_text
        self._label.visible = True
        self._display_timer.start()

    @center_label_matcher.base_case
    def update(self, subject: Subject) -> None:
        """Base update method for notifications."""
        raise ValueError(f"Expected subject of type Reset, Pause, or OutOfTime. Got {type(subject)}")

    def process(self) -> None:
        """For notifying the participant when they are not seen by the camera."""
        if not self._model.skeleton_detected:
            self.update_label("Your upper body and face must be on screen")

        if self._display_timer.done:
            self._label.visible = False

    @center_label_matcher.case(lambda self, subject: isinstance(subject, Pause))
    def pause_update(self, subject: Pause) -> None:
        """For notifying the participant when the application was paused/resumed."""
        if subject.paused:
            self.update_label("The application is paused, press SPACE to resume")
        else:
            self.update_label("The application was resumed")

    @center_label_matcher.case(lambda self, subject: isinstance(subject, Reset))
    def reset_update(self, subject: Reset) -> None:
        """For notifying the participant when the application was reset."""
        self.update_label("Resetting")

    @center_label_matcher.case(lambda self, subject: isinstance(subject, OutOfTime))
    def time_update(self, subject: OutOfTime) -> None:
        """For notifying the participant when they are too slow."""
        if subject.badness > 0.2:
            self.update_label("Move faster!")

class Skeleton(PeriodicTask):
    """
    Renders the skeleton from mediapipe pose estimation.
    Some customizations are made to the skeleton.
    For details see the skeleton kinematics file or the paper.
    """
    def __init__(
        self, 
        x: float,
        y: float,
        width: float,
        height: float,
        radius: float, 
        model: Model,
        batch: Batch = None, 
        point_group: Group = None, 
        line_group: Group = None
    ) -> None:
        self._x = x
        self._y = y
        self._model = model

        head = keypoints_to_indices["head"]
        left_shoulder = keypoints_to_indices["left_shoulder"]
        right_shoulder = keypoints_to_indices["right_shoulder"]
        neck = keypoints_to_indices["neck"]
        left_elbow = keypoints_to_indices["left_elbow"]
        right_elbow = keypoints_to_indices["right_elbow"]
        left_hand = keypoints_to_indices["left_hand"]
        right_hand = keypoints_to_indices["right_hand"]
        left_hip = keypoints_to_indices["left_hip"]
        right_hip = keypoints_to_indices["right_hip"]
        pelvis = keypoints_to_indices["pelvis"]
        left_knee = keypoints_to_indices["left_knee"]
        right_knee = keypoints_to_indices["right_knee"]
        left_ankle = keypoints_to_indices["left_ankle"]
        right_ankle = keypoints_to_indices["right_ankle"]

        self.base_skeleton = {
            head: (neck, ),
            left_shoulder: (neck, left_elbow, left_hip),
            right_shoulder: (neck, right_elbow, right_hip),
            neck: (pelvis, ),
            left_elbow: (left_hand, ),
            right_elbow: (right_hand, ),
            left_hand: (),
            right_hand: (),
            left_hip: (pelvis, left_knee),
            right_hip: (pelvis, right_knee),
            pelvis: (),
            left_knee: (left_ankle, ),
            right_knee: (right_ankle, ),
            left_ankle: (),
            right_ankle: (),
        }

        self.circles: Iterable[MyCircle] = deque()
        for i in range(num_of_keypoints):
            circle = MyCircle(Vec2(self._x, self._y), radius=radius, color=skeleton_point_color, batch=batch, group=point_group)
            circle.visible = False
            self.circles.append(circle)

        self.lines: Dict[int, Dict[int, Line]] = dict()
        for i in range(num_of_keypoints):
            self.lines[i] = dict()
            for j in self.base_skeleton[i]:
                line = Line(Vec2(self._x, self._y), Vec2(self._x, self._y), width=radius, color=skeleton_line_color, batch=batch, group=line_group)
                line.visible = False
                self.lines[i][j] = line

    def process(self) -> None:
        """For updating skeleton visuals with new pose estimations."""
        keypoints: List[Vec2] = self._model.keypoints
        for i in range(num_of_keypoints):
            self.circles[i].position = keypoints[i]
            self.circles[i].visible = self._model.skeleton_detected
            for j in self.base_skeleton[i]:
                self.lines[i][j].position = keypoints[i]
                self.lines[i][j].position2 = keypoints[j]
                self.lines[i][j].visible = self._model.skeleton_detected