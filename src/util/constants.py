# constants for converting between metric prefix'
m_to_n = 10**6
n_to_m = 10**-6
n_to_base = 10**-9
base_to_n = 10**9
m_to_base = 10**-3
base_to_m = 10**3