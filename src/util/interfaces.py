# this file defines the observer pattern
from __future__ import annotations
from collections import deque
from time import perf_counter_ns
from cProfile import Profile
from abc import ABC, abstractmethod
from typing import Iterable
from .parameters import ApplicationMode, mode

class Subject(ABC):
    """
    The Subject interface declares a set of methods for managing Observers.
    """

    @abstractmethod
    def attach(self, observer: Observer) -> None:
        """
        Attach an observer to the subject.
        """
        pass

    @abstractmethod
    def detach(self, observer: Observer) -> None:
        """
        Detach an observer from the subject.
        """
        pass

    @abstractmethod
    def notify(self) -> None:
        """
        Notify all observers about an event.
        """
        pass

class Observer(ABC):
    """
    The Observer interface declares the update method, used by subjects.
    """

    @abstractmethod
    def update(self, subject: Subject) -> None:
        """
        Receive update from subject.
        """
        pass

class PeriodicTask:
    """
    The PeriodicTask interface declares the dt_update method, used to schedule update classes regularly.
    It is also used to collect performance data whenever needed.
    """
    def __init_subclass__(cls, *args, **kwargs):
        """
        For initializing subclasses and adding fields for collecting performance data.
        """
        super().__init_subclass__(*args, **kwargs)
        # add variables for profiling 
        if mode == ApplicationMode.DELTA_TIMES:
            cls.execution_times: Iterable[float] = deque()
        elif mode == ApplicationMode.PROFILE:
            cls.profile: Profile = Profile()

    def dt_update(self, dt: float) -> None:
        """
        Periodically scheduled function. Collects performance data when needed.
        """
        # start profile capture
        if mode == ApplicationMode.DELTA_TIMES:
            start = perf_counter_ns()
        elif mode == ApplicationMode.PROFILE:
            self.profile.enable()
        # execute function
        self.process()
        # end profile capture
        if mode == ApplicationMode.DELTA_TIMES:
            self.execution_times.append(perf_counter_ns() - start)
        elif mode == ApplicationMode.PROFILE:
            self.profile.disable()

    @abstractmethod
    def process(self) -> None:
        """
        Custom component of the dt_update function.
        """
        pass

class Completable(PeriodicTask):
    """An Observer which can be completed."""
    def __init__(
        self,
    ) -> None:
        # Whether the object is completed
        self.done: bool = False
