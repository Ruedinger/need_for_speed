# this file contains important parameters for the program
# for configuration on the experimental conditions, see instructions.py
from typing import Tuple, List
from enum import Enum
from pyglet.canvas import get_display

class ApplicationMode(Enum):
    """
    All supported modes in which the application can run.
    In production mode, only the minimal user interface is displayed.
    In debug mode, additional debug information is displayed.
    Delta times and profile are two different approaches for profiling the application.
    """
    PRODUCTION = 0
    DEBUG = 1
    DELTA_TIMES = 2
    PROFILE = 3

mode = ApplicationMode.PROFILE

# path of the camera/video
video_path = 0

# directory to save data
data_dir = r"data/"

# directory to save performance profiles
# delta times and profile mode
profile_dir = r"profiles/"

# the mediapipe model
model_path = r'mediapipe_landmarkers/pose_landmarker_lite.task'

# sets the window margin in pixels
margin: int = 20

# toggles if application is fullscreen
fullscreen: bool = True

# for adjusting the maximal framerate
maximal_frame_rate: float = 60.0

# window size, not used with fullscreen enabled
window_width: int = 500
window_height: int = 500
if fullscreen:
    display = get_display()
    screen = display.get_default_screen()
    window_width = screen.width
    window_height = screen.height

# camera resolution
frame_width: int = 640
frame_height: int = 480

frames_per_second: float = 30.0

# parameters for calibrating movement size
# size of movements is constant in meters and gets projected to the respective size in pixels
movement_size_m = 0.5 # arm length in m
movement_width_m = 0.2 # tolerance for hand movements

# for showing the participant that they take too long to perform the movement
# accuracy will degrade if participant takes longer until movement is completed
# takes real world size of movement into account
seconds_per_meter: float = 10

# border radius of ui elements with round corners
border_radius: int = 30

# color settings
ui_color: Tuple[int, int, int, int] = (208, 217, 224, 150)
font_color: Tuple[int, int, int, int] = (0, 0, 0, 255)
default_alpha: int = 120
highlight_alpha: int = 200

def get_highlight_color(original_color: Tuple[int, int, int]):
    # 255 is maximal color intensity
    offset = 255 - max(original_color)
    highlight_color = []
    for i in range(3):
        highlight_color.append(original_color[i] + offset)
    highlight_color.append(highlight_alpha)
    return tuple(highlight_color)

# colors are fit for color blind participants according to https://www.datylon.com/blog/data-visualization-for-colorblind-readers#what-colors-can-they-see
instruction_colors: List[Tuple[int, int, int, int]] = [(240, 80, 57, default_alpha), (31, 68, 156, default_alpha)]
instruction_highlight_colors: List[Tuple[int, int, int, int]] = list(map(get_highlight_color, instruction_colors))
frustration_colors: List[Tuple[int, int, int, int]] = [(160, 189, 216, default_alpha), (186, 225, 242, default_alpha), (221, 221, 221, default_alpha), (249, 216, 183, default_alpha), (227, 153, 149, default_alpha)]
frustration_highlight_colors: List[Tuple[int, int, int, int]] = [(199, 228, 255, highlight_alpha), (186, 225, 255, highlight_alpha), (255, 255, 255, highlight_alpha), (255, 224, 189, highlight_alpha), (255, 181, 177, highlight_alpha)]
skeleton_point_color: Tuple[int, int, int, int] = (50, 50, 200, 255)
skeleton_line_color: Tuple[int, int, int, int] = (255, 255, 255, 255)