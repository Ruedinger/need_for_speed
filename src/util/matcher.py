class Matcher:
    """
    This class implements pattern matching on function arguments.
    """
    def __init__(self):
        self._cases = []

    def case(self, condition):
        """
        For adding function which should be called when the base function is called and condition is fulfilled.
        This implements a bit more then just pattern matching on overloaded functions. 
        The name of the added function is not relevant.
        """
        def decorator(func):
            self._cases.append((condition, func))
            return func
        return decorator

    def base_case(self, func):
        """For registering a base function to be matched against."""
        def wrapped(*args, **kwargs):
            for condition, case_func in self._cases:
                if condition(*args, **kwargs):
                    return case_func(*args, **kwargs)
            return func(*args, **kwargs)
        return wrapped
