from math import pi, isclose, cos, sin
import numpy as np
from pyglet.math import Vec2
from src.logic.skeleton_kinematics import angle_between_joints

def rotate(v: Vec2, angle: float) -> Vec2:
    return Vec2(v.x*cos(angle) - v.y*sin(angle), v.x*sin(angle) + v.y*cos(angle))

def test_head_angle_1():
    head = Vec2(0, 300)
    left_shoulder = Vec2(30, 250)
    right_shoulder = Vec2(-30, 250)
    left_elbow = Vec2(50, 210)
    right_elbow = Vec2(-50, 210)
    left_hand = Vec2(60, 150)
    right_hand = Vec2(-60, 150)
    left_hip = Vec2(20, 160)
    right_hip = Vec2(-20, 160)
    left_knee = Vec2(35, 80)
    right_knee = Vec2(-35, 80)
    left_ankle = Vec2(35, 0)
    right_ankle = Vec2(-35, 0)
    pelvis = (left_hip + right_hip)/2
    neck = (left_shoulder + right_shoulder)/2

    skeleton = [head,
    neck,
    left_shoulder,
    right_shoulder,
    left_elbow,
    right_elbow,
    left_hand,
    right_hand,
    pelvis,
    left_hip,
    right_hip,
    left_knee,
    right_knee,
    left_ankle,
    right_ankle]

    rotations = np.linspace(0, 2*pi, 9)
    for angle in rotations:
        assert isclose(angle_between_joints(list(map(lambda x: rotate(x, angle), skeleton)), "pelvis", "neck", "head"), pi)

def test_head_angle_2():
    head = Vec2(50, 300)
    left_shoulder = Vec2(30, 250)
    right_shoulder = Vec2(-30, 250)
    left_elbow = Vec2(50, 210)
    right_elbow = Vec2(-50, 210)
    left_hand = Vec2(60, 150)
    right_hand = Vec2(-60, 150)
    left_hip = Vec2(20, 160)
    right_hip = Vec2(-20, 160)
    left_knee = Vec2(35, 80)
    right_knee = Vec2(-35, 80)
    left_ankle = Vec2(35, 0)
    right_ankle = Vec2(-35, 0)
    pelvis = (left_hip + right_hip)/2
    neck = (left_shoulder + right_shoulder)/2

    skeleton = [head,
    neck,
    left_shoulder,
    right_shoulder,
    left_elbow,
    right_elbow,
    left_hand,
    right_hand,
    pelvis,
    left_hip,
    right_hip,
    left_knee,
    right_knee,
    left_ankle,
    right_ankle]

    assert isclose(angle_between_joints(skeleton, "pelvis", "neck", "head"), pi*3/4)

def test_head_angle_3():
    head = Vec2(-50, 300)
    left_shoulder = Vec2(30, 250)
    right_shoulder = Vec2(-30, 250)
    left_elbow = Vec2(50, 210)
    right_elbow = Vec2(-50, 210)
    left_hand = Vec2(60, 150)
    right_hand = Vec2(-60, 150)
    left_hip = Vec2(20, 160)
    right_hip = Vec2(-20, 160)
    left_knee = Vec2(35, 80)
    right_knee = Vec2(-35, 80)
    left_ankle = Vec2(35, 0)
    right_ankle = Vec2(-35, 0)
    pelvis = (left_hip + right_hip)/2
    neck = (left_shoulder + right_shoulder)/2

    skeleton = [head,
    neck,
    left_shoulder,
    right_shoulder,
    left_elbow,
    right_elbow,
    left_hand,
    right_hand,
    pelvis,
    left_hip,
    right_hip,
    left_knee,
    right_knee,
    left_ankle,
    right_ankle]

    assert isclose(angle_between_joints(skeleton, "pelvis", "neck", "head"), pi*3/4)


def test_torso_angle_1():
    head = Vec2(0, 300)
    left_shoulder = Vec2(30, 250)
    right_shoulder = Vec2(-30, 250)
    left_elbow = Vec2(50, 210)
    right_elbow = Vec2(-50, 210)
    left_hand = Vec2(60, 150)
    right_hand = Vec2(-60, 150)
    left_hip = Vec2(20, 160)
    right_hip = Vec2(-20, 160)
    left_knee = Vec2(35, 80)
    right_knee = Vec2(-35, 80)
    left_ankle = Vec2(35, 0)
    right_ankle = Vec2(-35, 0)
    pelvis = (left_hip + right_hip)/2
    neck = (left_shoulder + right_shoulder)/2

    skeleton = [head,
    neck,
    left_shoulder,
    right_shoulder,
    left_elbow,
    right_elbow,
    left_hand,
    right_hand,
    pelvis,
    left_hip,
    right_hip,
    left_knee,
    right_knee,
    left_ankle,
    right_ankle]

    rotations = np.linspace(0, 2*pi, 9)
    for angle in rotations:
        assert isclose(angle_between_joints(list(map(lambda x: rotate(x, angle), skeleton)), "left_hip", "pelvis", "neck"), pi/2)

def test_torso_angle_2():
    head = Vec2(115, 300)
    left_shoulder = Vec2(120, 250)
    right_shoulder = Vec2(60, 250)
    left_elbow = Vec2(50, 210)
    right_elbow = Vec2(-50, 210)
    left_hand = Vec2(60, 150)
    right_hand = Vec2(-60, 150)
    left_hip = Vec2(20, 160)
    right_hip = Vec2(-20, 160)
    left_knee = Vec2(35, 80)
    right_knee = Vec2(-35, 80)
    left_ankle = Vec2(35, 0)
    right_ankle = Vec2(-35, 0)
    pelvis = (left_hip + right_hip)/2
    neck = (left_shoulder + right_shoulder)/2

    skeleton = [head,
    neck,
    left_shoulder,
    right_shoulder,
    left_elbow,
    right_elbow,
    left_hand,
    right_hand,
    pelvis,
    left_hip,
    right_hip,
    left_knee,
    right_knee,
    left_ankle,
    right_ankle]

    assert isclose(angle_between_joints(skeleton, "left_hip", "pelvis", "neck"), pi/4)

def test_torso_angle_3():
    head = Vec2(115, 300)
    left_shoulder = Vec2(-60, 250)
    right_shoulder = Vec2(-120, 250)
    left_elbow = Vec2(50, 210)
    right_elbow = Vec2(-50, 210)
    left_hand = Vec2(60, 150)
    right_hand = Vec2(-60, 150)
    left_hip = Vec2(20, 160)
    right_hip = Vec2(-20, 160)
    left_knee = Vec2(35, 80)
    right_knee = Vec2(-35, 80)
    left_ankle = Vec2(35, 0)
    right_ankle = Vec2(-35, 0)
    pelvis = (left_hip + right_hip)/2
    neck = (left_shoulder + right_shoulder)/2

    skeleton = [head,
    neck,
    left_shoulder,
    right_shoulder,
    left_elbow,
    right_elbow,
    left_hand,
    right_hand,
    pelvis,
    left_hip,
    right_hip,
    left_knee,
    right_knee,
    left_ankle,
    right_ankle]

    assert isclose(angle_between_joints(skeleton, "left_hip", "pelvis", "neck"), pi*3/4)