from math import sin, cos, sqrt, pi
from random import randint
from collections import deque
from statistics import median, fmean
from pyglet.math import Vec2
import numpy as np
from src.logic.calibrator import Calibrator
from src.logic.instruction import Calibration, MovementType
from src.util.parameters import movement_size_m, movement_width_m

def test_init():
    calibrator = Calibrator(None)
    assert calibrator._wait_times == deque()
    assert calibrator._wait_times_index == 0
    assert not calibrator._measurement_timer.is_running
    assert calibrator._intermediate_results.shape == (0, 2)
    assert (calibrator._intermediate_results == np.zeros((0,2), dtype=float)).all()
    assert calibrator._world_intermediate_results.shape == (0, 2)
    assert (calibrator._world_intermediate_results == np.zeros((0,2), dtype=float)).all()
    assert calibrator._dist_real_dist_virtual_x is None
    assert calibrator._dist_real_dist_virtual_y is None
    assert calibrator._horizontal_movement_size_px is None
    assert calibrator._vertical_movement_size_px is None
    assert calibrator._diagonal_movement_size_px is None
    assert calibrator._movement_width is None

def test_init_measurement():
    calibrator = Calibrator(None)
    wait_times = [5]*3+[2]*4
    calibration = Calibration(_movements=[MovementType.SIDE, MovementType.SIDE], _timer=30, _mirrored=False, _wait_times=wait_times, _aggregation=median)
    calibrator.init_measurement(calibration)
    assert calibrator._wait_times == wait_times
    assert calibrator._wait_times_index == 0
    assert calibrator._measurement_timer.is_running
    assert calibrator._measurement_results.shape == (len(wait_times)*4, 2)
    assert (calibrator._measurement_results == np.zeros((len(wait_times)*4, 2), dtype=float)).all()
    assert calibrator._world_measurement_results.shape == (len(wait_times)*4, 2)
    assert (calibrator._world_measurement_results == np.zeros((len(wait_times)*4, 2), dtype=float)).all()

def test_calibrate():
    calibrator = Calibrator(None)
    # generate virtual coordinates
    calibrator._intermediate_results = np.random.rand(4, 2)*200
    calibrator._world_intermediate_results = np.random.rand(4, 2) - 0.5

    px_left_shoulder = calibrator._intermediate_results[0]
    px_right_shoulder = calibrator._intermediate_results[1]
    px_neck = calibrator._intermediate_results[2]
    px_pelvis = calibrator._intermediate_results[3]

    meter_left_shoulder = calibrator._world_intermediate_results[0]
    meter_right_shoulder = calibrator._world_intermediate_results[1]
    meter_neck = calibrator._world_intermediate_results[2]
    meter_pelvis = calibrator._world_intermediate_results[3]

    calibrator.calibrate(np.median)

    assert calibrator.horizontal_movement_size_px == int(np.linalg.norm(px_left_shoulder - px_right_shoulder)/np.linalg.norm(meter_left_shoulder - meter_right_shoulder)*movement_size_m)
    assert calibrator.vertical_movement_size_px == int(np.linalg.norm(px_neck - px_pelvis)/np.linalg.norm(meter_neck - meter_pelvis)*movement_size_m)

    k = calibrator.horizontal_movement_size_px*calibrator.vertical_movement_size_px/sqrt((calibrator.vertical_movement_size_px*cos(pi/4))**2 + (calibrator.horizontal_movement_size_px*sin(pi/4))**2)
    x = k*cos(pi/4)
    y = k*sin(pi/4)
    assert calibrator.diagonal_movement_size_px == int(abs(Vec2(x, y)))
    assert calibrator.movement_width == int(fmean([np.linalg.norm(px_left_shoulder - px_right_shoulder)/np.linalg.norm(meter_left_shoulder - meter_right_shoulder), np.linalg.norm(px_neck - px_pelvis)/np.linalg.norm(meter_neck - meter_pelvis)])*movement_width_m)

def test_reset():
    calibrator = Calibrator(None)
    calibrator._wait_times.append(5)
    calibrator._wait_times_index = 3
    calibrator._measurement_timer.start()
    calibrator._measurement_results = np.random.rand(8, 2) * 200
    calibrator._world_measurement_results = np.random.rand(8, 2) - 0.5
    calibrator._intermediate_results = np.random.rand(8, 2) * 200
    calibrator._world_intermediate_results = np.random.rand(8, 2) - 0.5
    calibrator.calibrate(np.mean)
    calibrator.reset_update(None)
    assert calibrator._wait_times == deque()
    assert calibrator._wait_times_index == 0
    assert not calibrator._measurement_timer.is_running
    assert calibrator._intermediate_results.shape == (0, 2)
    assert (calibrator._intermediate_results == np.zeros((0,2), dtype=float)).all()
    assert calibrator._world_intermediate_results.shape == (0, 2)
    assert (calibrator._world_intermediate_results == np.zeros((0,2), dtype=float)).all()
    assert calibrator._dist_real_dist_virtual_x is None
    assert calibrator._dist_real_dist_virtual_y is None
    assert calibrator._horizontal_movement_size_px is None
    assert calibrator._vertical_movement_size_px is None
    assert calibrator._diagonal_movement_size_px is None
    assert calibrator._movement_width is None
    