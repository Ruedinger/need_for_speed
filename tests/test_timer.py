from time import sleep
from math import isclose
from src.logic.timer import Timer
from src.util.constants import base_to_n

def test_start():
    timer = Timer(2)
    assert not timer.is_running
    timer.start()
    assert timer.is_running

def test_stop():
    timer = Timer(2)
    timer.start()
    assert timer.is_running
    timer.stop()
    assert not timer.is_running

def test_passed_time():
    timer = Timer(2)
    assert timer.passed_time == 0
    timer.start()
    sleep(2)
    assert isclose(timer.passed_time, 2*base_to_n, rel_tol=0.01)

def test_done():
    timer = Timer(2*base_to_n)
    assert not timer.done
    timer.start()
    sleep(2)
    assert timer.done