from itertools import groupby
from typing import List
from src.logic.instruction import latencies, Difficulty, num_movements, num_repetitions, Condition, generate_conditions

def test_conditions():
    condition_instructions: List[Condition] = generate_conditions()
    latency_groups = []
    data = sorted(condition_instructions, key=lambda x: x.latency)
    for k, g in groupby(data, lambda x: x.latency):
        latency_groups.append(list(g))
    difficulty_groups = []
    for group in latency_groups:
        data = sorted(group, key=lambda x: int(x.difficulty))
        for k, g in groupby(data, lambda x: int(x.difficulty)):
            difficulty_groups.append(list(g))
    assert len(difficulty_groups) == len(latencies)*len(Difficulty)
    for group in difficulty_groups:
        assert len(group) == num_movements*num_repetitions