# tests for the data collector class
from statistics import fmean
from time import sleep
from pathlib import Path
from collections import deque
from random import randint, random
from math import isclose, pi
from src.logic.data_collector import class_based_accuracy_with_dt, class_based_accuracy_without_dt, distance_based_accuracy_with_dt, distance_based_accuracy_without_dt, accumulate_angles, DataCollector
from src.logic.application_state import first_state
from src.logic.instruction import Condition, MovementType, Difficulty
from src.util.constants import n_to_base
from src.logic.notification import Reset

def clear(path: str):
    directory = Path(path)
    for file in directory.iterdir():
        file.unlink()
    directory.rmdir()

def r_float():
    return round(random(), 2)

def test_class_based_accuracy_without_dt():
    accuracies = [([1, 1], 0), ([0.5, 0.7], 1), ([1, 0.8], 0.75)]
    assert isclose(class_based_accuracy_without_dt(accuracies)[0], 0.666666666, abs_tol=10**-3)
    assert isclose(class_based_accuracy_without_dt(accuracies)[1], 0.333333333, abs_tol=10**-3)

def test_class_based_accuracy_with_dt():
    accuracies = [([1, 1], 0), ([0.5, 0.7], 1), ([1, 0.8], 0.75)]
    assert isclose(class_based_accuracy_with_dt(accuracies)[0], 0.428571429, abs_tol=10**-3)
    assert isclose(class_based_accuracy_with_dt(accuracies)[1], 0, abs_tol=10**-3)

def test_distance_based_accuracy_without_dt():
    accuracies = [([1, 1], 0), ([0.5, 0.7], 1), ([1, 0.8], 0.75)]
    assert isclose(distance_based_accuracy_without_dt(accuracies)[0], 0.833333333, abs_tol=10**-3)
    assert isclose(distance_based_accuracy_without_dt(accuracies)[1], 0.833333333, abs_tol=10**-3)

def test_distance_based_accuracy_with_dt():
    accuracies = [([1, 1], 0), ([0.5, 0.7], 1), ([1, 0.8], 0.75)]
    assert isclose(distance_based_accuracy_with_dt(accuracies)[0], 0.714285714, abs_tol=10**-3)
    assert isclose(distance_based_accuracy_with_dt(accuracies)[1], 0.742857143, abs_tol=10**-3)

def test_class_based_accuracy_without_dt_rand():
    iterations = randint(0, 200)
    accuracies = []
    for i in range(iterations):
        accuracies.append(([r_float(), r_float()], r_float()))
    if iterations:
        ones_left = len(list(filter(lambda x: x[0][0] == 1, accuracies)))
        ones_right = len(list(filter(lambda x: x[0][1] == 1, accuracies)))
        assert isclose(class_based_accuracy_without_dt(accuracies)[0], ones_left/iterations)
        assert isclose(class_based_accuracy_without_dt(accuracies)[1], ones_right/iterations)
    else:
        assert isclose(class_based_accuracy_without_dt(accuracies)[0], 0)
        assert isclose(class_based_accuracy_without_dt(accuracies)[1], 0)

def test_class_based_accuracy_with_dt_rand():
    iterations = randint(0, 200)
    accuracies = []
    for i in range(iterations):
        accuracies.append(([r_float(), r_float()], r_float()))
    if iterations:
        accumulated_times_left = sum([x[1] if x[0][0] == 1 else 0 for x in accuracies])
        accumulated_times_right = sum([x[1] if x[0][1] == 1 else 0 for x in accuracies])
        accumulated_times = sum([x[1] for x in accuracies])
        assert isclose(class_based_accuracy_with_dt(accuracies)[0], accumulated_times_left/accumulated_times)
        assert isclose(class_based_accuracy_with_dt(accuracies)[1], accumulated_times_right/accumulated_times)
    else:
        assert isclose(class_based_accuracy_with_dt(accuracies)[0], 0)
        assert isclose(class_based_accuracy_with_dt(accuracies)[1], 0)

def test_distance_based_accuracy_without_dt_rand():
    iterations = randint(0, 20)
    accuracies = []
    for i in range(iterations):
        accuracies.append(([r_float(), r_float()], r_float()))
    if iterations:
        distances_left = 0
        distances_right = 0
        for accuracy in accuracies:
            distances_left += accuracy[0][0]
            distances_right += accuracy[0][1]
        
        assert isclose(distance_based_accuracy_without_dt(accuracies)[0], distances_left/iterations)
        assert isclose(distance_based_accuracy_without_dt(accuracies)[1], distances_right/iterations)
    else:
        assert isclose(distance_based_accuracy_without_dt(accuracies)[0], 0)
        assert isclose(distance_based_accuracy_without_dt(accuracies)[1], 0)

def test_distance_based_accuracy_with_dt_rand():
    iterations = randint(0, 200)
    accuracies = []
    for i in range(iterations):
        accuracies.append(([r_float(), r_float()], r_float()))
    if iterations:
        accumulated_times_left = 0
        accumulated_times_right = 0
        accumulated_times = 0
        for accuracy in accuracies:
            accumulated_times_left += accuracy[0][0]*accuracy[1]
            accumulated_times_right += accuracy[0][1]*accuracy[1]
            accumulated_times += accuracy[1]

        assert isclose(distance_based_accuracy_with_dt(accuracies)[0], accumulated_times_left/accumulated_times)
        assert isclose(distance_based_accuracy_with_dt(accuracies)[1], accumulated_times_right/accumulated_times)
    else:
        assert isclose(distance_based_accuracy_with_dt(accuracies)[0], 0)
        assert isclose(distance_based_accuracy_with_dt(accuracies)[1], 0)

def test_accumulate_angles():
    iterations = randint(0, 200)
    angles = []
    for i in range(iterations):
        angles.append(r_float()*pi)

    if angles:
        accumulated_angles = 0
        for i in range(len(angles)-1):
            accumulated_angles += abs(angles[i] - angles[i+1])
        assert isclose(accumulate_angles(angles), accumulated_angles)
    else: 
        assert accumulate_angles(angles) == 0

def test_data_collector_init():
    data_collector = DataCollector(None, None, "test_data")
    assert data_collector._model is None
    assert data_collector._movement_time_notification is None
    assert data_collector.num_repetitions == 0
    assert data_collector._head_angles == deque()
    assert data_collector._torso_angles == deque()
    assert data_collector.frustration == 0
    assert data_collector._accuracies == deque()
    assert 0 <= data_collector._ident <= 999999
    assert data_collector._last_instruction_index == 0
    assert data_collector._last_state == first_state

def test_save_coordination_data():
    directory = "test_data"
    clear(directory)
    data_collector = DataCollector(None, None, directory)
    coordination_file = Path(directory) / "coordination.csv"
    data_collector._last_instruction_index = 10
    data_collector._head_angles = deque([pi, -pi, pi])
    data_collector._torso_angles = deque([0, pi/2, pi])
    data_collector.num_repetitions = 11
    ident = data_collector._ident
    data_collector._accuracy_timer.start()
    sleep(1)
    data_collector.accuracies = [1, 1]
    sleep(1)
    data_collector.accuracies = [0.5, 0.7]
    sleep(2)
    data_collector.accuracies = [0.8, 0.3]
    data_collector._accuracy_timer.stop()
    accuracy = fmean(distance_based_accuracy_with_dt(data_collector._accuracies))
    time = sum([measurement[1] for measurement in data_collector._accuracies])*n_to_base
    data_collector.save_coordination_data(distance_based_accuracy_with_dt)
    text = open(coordination_file).readlines()
    assert text == ['id,movement,time,accuracy,head bend,torso bend,completions\n', f'{ident},10,{time},{accuracy},{4*pi},{pi},11\n']

def test_save_condition_data():
    directory = "test_data"
    clear(directory)
    data_collector = DataCollector(None, None, directory)
    condition_file = Path(directory) / "condition.csv"
    data_collector._last_instruction_index = 4
    data_collector._last_instruction = Condition(_movements=[MovementType.ELLIPSE, MovementType.HORIZONTAL_LINE], _timer=-1, _mirrored=False, _latency=65, _difficulty=Difficulty.COMPLEX)
    data_collector.num_repetitions = 11
    ident = data_collector._ident
    data_collector._accuracy_timer.start()
    sleep(1)
    data_collector.accuracies = [1, 1]
    sleep(1)
    data_collector.accuracies = [1, 0.7]
    sleep(2)
    data_collector.accuracies = [0.2, 0.7]
    data_collector._accuracy_timer.stop()
    accuracy = fmean(distance_based_accuracy_with_dt(data_collector._accuracies))
    time = sum([measurement[1] for measurement in data_collector._accuracies])*n_to_base
    data_collector.save_condition_data(distance_based_accuracy_with_dt)
    text = open(condition_file).readlines()
    assert text == ['id,movement,latency,difficulty,time,accuracy\n', f'{ident},4,65,Difficulty.COMPLEX,{time},{accuracy}\n']
    
def test_save_questionnaire_data():
    directory = "test_data"
    clear(directory)
    data_collector = DataCollector(None, None, directory)
    questionnaire_file = Path(directory) / "questionnaire.csv"
    frustration = randint(0, 4)
    data_collector.frustration = frustration
    ident = data_collector._ident
    data_collector.save_questionnaire_data()
    text = open(questionnaire_file).readlines()
    assert text == ['id,frustration\n', f'{ident},{frustration}\n']
    
def test_reset():
    data_collector = DataCollector(None, None, "test_data")
    data_collector.num_repetitions = 40
    data_collector._head_angles = deque([pi, -pi, pi])
    data_collector._torso_angles = deque([0, pi/2, pi])
    data_collector._frustration = 3
    assert 0 <= data_collector._ident <= 999999
    data_collector.reset_update(Reset())
    assert not data_collector._accuracy_timer.is_running
    assert data_collector._model is None
    assert data_collector._movement_time_notification is None
    assert data_collector.num_repetitions == 0
    assert data_collector._head_angles == deque()
    assert data_collector._torso_angles == deque()
    assert data_collector.frustration == 0
    assert data_collector._accuracies == deque()
    assert data_collector._last_instruction_index == 0
    assert data_collector._last_state == first_state

test_save_questionnaire_data()