from math import pi
from pyglet.math import Vec2
from src.visualization.rendering import Multiline, Triangle, Circle, EllipticArc, Line, RectangleWithText, Arrow

def test_multiline():
    multiline = Multiline(Vec2(0, 0), Vec2(300, 300), Vec2(600, 30), closed=True, thickness=20, t=1, color=(200, 50, 50))
    assert multiline.t == 1
    assert len(multiline.polygon) == 7
    multiline.t = 0.4
    assert multiline.t == 0.4
    assert len(multiline.polygon) == 5

def test_triangle():
    triangle = Triangle(Vec2(50, 50), Vec2(600, 400), Vec2(80, 44))
    assert triangle.position == Vec2(50, 50)
    assert triangle.position2 == Vec2(600, 400)
    assert triangle.position3 == Vec2(80, 44)
    triangle.position = Vec2(100, 80)
    triangle.position2 = Vec2(40, 33)
    triangle.position3 = Vec2(79, 27)
    assert triangle.position == Vec2(100, 80)
    assert triangle.position2 == Vec2(40, 33)
    assert triangle.position3 == Vec2(79, 27)

    assert len(triangle.polygon) == 4

def test_circle():
    circle = Circle(Vec2(100, 100), 200)
    assert circle.position == Vec2(100, 100)

def test_elliptic_arc():
    ellipse = EllipticArc(position=Vec2(100, 120), a=200, b=100, segments=300, angle=pi, start_angle=0, closed=False, width=30, t=1)
    # initialization and getters
    assert len(ellipse.center_points) == 301
    assert ellipse.width == 30
    assert ellipse.angle == pi
    assert ellipse.t == 1
    assert ellipse.start_angle == 0
    assert ellipse.position == Vec2(100, 120)
    assert ellipse.color == (255, 255, 255, 255)
    assert ellipse.point_on_ellipse(0).x == 300 # position.x + a
    assert ellipse.point_on_ellipse(pi/2).y == 220 # position.y + b
    assert ellipse.point_on_ellipse(pi).x == -100
    assert ellipse.point_on_ellipse(pi*3/2).y == 20
    assert ellipse.point_on_ellipse(pi*2).x == 300
    assert ellipse.fraction_of_point(Vec2(300, 120)) == 0
    # setters
    ellipse.t = 0.5
    assert ellipse.t == 0.5
    assert ellipse.angle == pi/2
    ellipse.t = 1
    ellipse.width = 1.3
    ellipse.angle = 15/7*pi
    ellipse.start_angle = pi/2
    ellipse.position = Vec2(0, 0)
    assert ellipse.width == 1.3
    assert ellipse.angle == 15/7*pi
    assert ellipse.start_angle == pi/2
    assert ellipse.position == Vec2(0, 0)

def test_line():
    line = Line(Vec2(20, 40), Vec2(1000, 70), 786, 0.5)
    assert line.position == Vec2(20, 40)
    assert line.position2 == Vec2(510, 55)
    assert line.t == 0.5

def test_rectangle():
    rectangle = RectangleWithText(Vec2(30, 34), Vec2(86, 60), 52)
    assert rectangle.text is None
    assert rectangle.position == Vec2(30, 34)
    assert rectangle.position2 == Vec2(86, 60)
    assert rectangle.visible
    rectangle.visible = False
    assert rectangle.font_size is None
    assert not rectangle.visible
    try:
        rectangle.text = "This is a string"
    except ValueError:
        pass
    else:
        assert False
    rectangle.font_size = 20
    rectangle.text = "This is a string"
    assert rectangle.font_size == 20
    assert rectangle.text == "This is a string"

def test_arrow():
    arrow = Arrow(Vec2(20, 40), Vec2(40, 20), beta=3, width=50)
    assert arrow.visible
    arrow.visible = False
    assert arrow.position == Vec2(20, 40)
    assert arrow.position2 == Vec2(40, 20)
    arrow.flip()
    assert arrow.position == Vec2(40, 20)
    assert arrow.position2 == Vec2(20, 40)
    assert not arrow.visible
    assert len(arrow.polygon) == 8
    arrow.position = Vec2(60, 90)
    arrow.flip()
    assert arrow.position == Vec2(20, 40)
    assert arrow.position2 == Vec2(60, 90)