# file for importing resources (images, sound, etc.) 
# all resources where the source is not explicit are designed by myself
import pyglet
import sys

if "pytest" in sys.modules:
    error_sound = None
    success_sound = None
    clock_sound = None

    skeleton_down = None
    skeleton_middle = None
    skeleton_up = None
    skeleton_horizontal_calibration = None
    skeleton_vertical_calibration = None

    emojis = None
    no_frustration = None
    low_frustration = None
    medium_frustration = None
    high_frustration = None
    maximal_frustration = None
    frustration_images = [no_frustration, low_frustration, medium_frustration, high_frustration, maximal_frustration]

else:
    pyglet.resource.path = ['resources']
    pyglet.resource.reindex()

    # taken from https://mixkit.co/free-sound-effects/
    error_sound = pyglet.resource.media(r"error.mp3", streaming=False)
    success_sound = pyglet.resource.media(r"success.mp3", streaming=False)
    clock_sound = pyglet.resource.media(r"clock.mp3", streaming=False)

    skeleton_down = pyglet.resource.image(r"skeleton_down.png")
    skeleton_middle = pyglet.resource.image(r"skeleton_middle.png")
    skeleton_up = pyglet.resource.image(r"skeleton_up.png")
    skeleton_horizontal_calibration = pyglet.resource.image(r"skeleton_horizontal_calibration.png")
    skeleton_vertical_calibration = pyglet.resource.image(r"skeleton_vertical_calibration.png")

    # All emojis designed by OpenMoji: https://openmoji.org/ – the open-source emoji and icon project. License: CC BY-SA 4.0: https://creativecommons.org/licenses/by-sa/4.0/#
    emojis = pyglet.resource.image(r"smileys-emotion.png")
    emoji_sequence = pyglet.image.ImageGrid(emojis, 10, 16)
    no_frustration = emoji_sequence[147]
    low_frustration = emoji_sequence[155]
    medium_frustration = emoji_sequence[114]
    high_frustration = emoji_sequence[120]
    maximal_frustration = emoji_sequence[72]
    frustration_images = [no_frustration, low_frustration, medium_frustration, high_frustration, maximal_frustration]