# need for speed
This repository hosts the code for the bachelor thesis "Need for Speed: Evaluating Impact of Latency on Motor Performance in Immersive Learning Environments"

To execute, run 'python main.py' after installing the requirements.
Press q to quit, r to restart, SPACE to pause, up and down arrows to adjust difficulty.

Requires python 3.10 or 3.11.

Note that pyglet has sometimes issues working correctly in virtual environments.