from collections import deque
from pathlib import Path
from typing import Iterable
from time import perf_counter_ns
import cv2
from PIL import Image
from pyglet.font import add_directory
from pyglet.clock import schedule_interval
from pyglet.app import run
from src.visualization.canvas import Canvas
from src.logic.model import Model
from src.logic.instruction import Difficulty, MovementType, Instruction, Calibration, Condition
from src.logic.application_state import ApplicationState
from src.logic.notification import Reset, Pause, MovementSync, OutOfTime, ParticipantMoved, StateUpdate
from src.logic.data_collector import DataCollector
from src.logic.webcam_video_stream import WebcamVideoStream
from src.util.constants import n_to_base, m_to_base
from src.logic.task_scheduler import TaskScheduler
from src.util.parameters import mode, ApplicationMode, video_path, maximal_frame_rate, profile_dir, frame_width, frame_height, frames_per_second, data_dir
if mode == ApplicationMode.PROFILE:
    from cProfile import Profile
elif mode == ApplicationMode.DELTA_TIMES:
    import pickle
import pyglet
pyglet.options['debug_gl'] = False

def update(dt: float) -> None:
    if mode == ApplicationMode.DELTA_TIMES:
        update_delta_times.append(dt)
        start = perf_counter_ns()
    elif mode == ApplicationMode.PROFILE:
        camera_read_profile.enable()
    success, frame, time = cap.read()
    if mode == ApplicationMode.DELTA_TIMES:
        camera_read_times.append(perf_counter_ns() - start)
    elif mode == ApplicationMode.PROFILE:
        camera_read_profile.disable()
    if success:
        if mode == ApplicationMode.DELTA_TIMES:
            start = perf_counter_ns()
        elif mode == ApplicationMode.PROFILE:
            preprocessing_profile.enable()
        flipped_frame = cv2.flip(frame, 1)
        raw_frame = Image.fromarray(flipped_frame).tobytes()
        if mode == ApplicationMode.DELTA_TIMES:
            preprocessing_times.append(perf_counter_ns() - start)
        elif mode == ApplicationMode.PROFILE:
            preprocessing_profile.disable()

        if mode == ApplicationMode.DELTA_TIMES:
            start = perf_counter_ns()
        elif mode == ApplicationMode.PROFILE:
            image_render_profile.enable()
        canvas.background.set_data("BGR", -3*video_width, raw_frame)
        model.process_frame(frame, time)
        if mode == ApplicationMode.DELTA_TIMES:
            image_render_times.append(perf_counter_ns() - start)
        elif mode == ApplicationMode.PROFILE:
            image_render_profile.disable()

if __name__ == '__main__':
    # initialize camera
    cap = WebcamVideoStream(video_path, frame_width, frame_height, frames_per_second)
    video_width = cap.width
    video_height = cap.height
    cap.start()

    add_directory("Roboto/")

    # initialize classes
    scheduler = TaskScheduler()
    reset = Reset()
    pause = Pause()
    sync = MovementSync()
    out_of_time = OutOfTime()
    participant_moved = ParticipantMoved()
    state_update = StateUpdate()
    model = Model(state_update, scheduler)
    scheduler.add_task(model, [ApplicationState.CALIBRATION, ApplicationState.FAMILIARIZE, ApplicationState.COORDINATION_TEST, ApplicationState.QUESTIONNAIRE, ApplicationState.PREPARATION, ApplicationState.CONDITION, ApplicationState.CLOSING])
    data_collector = DataCollector(movement_time_notification=out_of_time, model=model, directory_str=data_dir)
    scheduler.add_task(data_collector, [ApplicationState.COORDINATION_TEST, ApplicationState.PREPARATION, ApplicationState.QUESTIONNAIRE, ApplicationState.CONDITION, ApplicationState.CLOSING])
    state_update.attach(data_collector)
    reset.attach(data_collector)
    pause.attach(data_collector)
    participant_moved.attach(data_collector)
    reset.attach(model)
    pause.attach(model)
    participant_moved.attach(model)
    canvas = Canvas(
        task_scheduler=scheduler,
        model=model,
        reset=reset,
        pause=pause,
        sync=sync,
        movement_time=out_of_time,
        participant_moved=participant_moved,
        state_update=state_update,
        data_collector=data_collector,
        video_width=video_width,
        video_height=video_height
        )
    state_update.notify()

    # update the application with the specified frame rate
    schedule_interval(update, 1/maximal_frame_rate)

    if mode == ApplicationMode.PROFILE:
        # initialize variables for collecting performance data
        camera_read_profile = Profile()
        preprocessing_profile = Profile()
        image_render_profile = Profile()

    elif mode == ApplicationMode.DELTA_TIMES:
        # initialize variables for collecting delta times
        update_delta_times: Iterable[float] = deque()
        camera_read_times: Iterable[float] = deque()
        preprocessing_times: Iterable[float] = deque()
        image_render_times: Iterable[float] = deque()

    # let pyglet do it's stuff
    run(1/maximal_frame_rate)

    # kill camera thread
    cap.stop()

    # save profile data
    if mode == ApplicationMode.PROFILE:
        directory = Path(profile_dir)
        if not directory.exists():
            directory.mkdir()
        camera_read_profile.dump_stats(directory / "camera_read_profile")
        preprocessing_profile.dump_stats(directory / "preprocessing_profile")
        model.profile.dump_stats(directory / "check_done_profile")
        for task in scheduler.task_profiles:
            scheduler.task_profiles[task].dump_stats(directory / (task+"_profile"))
    elif mode == ApplicationMode.DELTA_TIMES:
        directory = Path(profile_dir)
        if not directory.exists():
            directory.mkdir()
        for task in scheduler.task_execution_times:
            with open(directory / (task+"_times"), 'wb') as fd:
                execution_times = list(map(lambda x: x*n_to_base, scheduler.task_execution_times[task]))
                pickle.dump(execution_times, fd)
        with open(directory / 'draw_delta_times', 'wb') as fd:
            pickle.dump(canvas.window_draw_delta_times, fd)
        with open(directory / 'update_delta_times', 'wb') as fd:
            pickle.dump(update_delta_times, fd)
        with open(directory / 'camera_read_times', 'wb') as fd:
            camera_read_times = list(map(lambda x: x*n_to_base, camera_read_times))
            pickle.dump(camera_read_times, fd)
        with open(directory / 'preprocessing_times', 'wb') as fd:
            preprocessing_times = list(map(lambda x: x*n_to_base, preprocessing_times))
            pickle.dump(preprocessing_times, fd)
        with open(directory / 'image_render_times', 'wb') as fd:
            image_render_times = list(map(lambda x: x*n_to_base, image_render_times))
            pickle.dump(image_render_times, fd)
        with open(directory / 'inference_times', 'wb') as fd:
            inference_times = list(map(lambda x: x*m_to_base, model.inference_times))
            pickle.dump(inference_times, fd)
        with open(directory / 'check_done_times', 'wb') as fd:
            check_done_times = list(map(lambda x: x*n_to_base, model.execution_times))
            pickle.dump(check_done_times, fd)


