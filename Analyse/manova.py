from statsmodels.multivariate.manova import MANOVA
from statsmodels.stats.diagnostic import acorr_lm
import pingouin as pg
from pandas import read_csv
from scipy.stats import rankdata, norm
import numpy as np
from itertools import product
from statistics import fmean, median
np.set_printoptions(threshold=np.inf, suppress=True)

# load data
condition =  read_csv('/home/jonas/Studium/Bachelorarbeit/Analyse/data/condition_w_expertise.csv', sep=",")
numpy_data = condition.to_numpy()
independent_variables = numpy_data[:, 0:3]
dependent_variables = numpy_data[:, 3:6]

# three-way inverse normal transform MANOVA
# "siehe Varianzanalysen - Prüfen der Voraussetzungen und nichtparametrische Methoden sowie praktische Anwendungen mit R und SPSS" von der Universität zu Köln
n = dependent_variables.shape[0]
rank_dependent_variables = rankdata(dependent_variables, axis=0, method="min")
transformed_dependent_variables = norm.ppf(rank_dependent_variables/(n+1))

#print(acorr_lm(resid=transformed_dependent_variables))

manova = MANOVA(transformed_dependent_variables, independent_variables)
print(manova.mv_test())

# games-howell post hoc for each dependent variable
condition["condition"] = "latency: " + condition["latency"].astype(str) + ", difficulty: " + condition["difficulty"].astype(str) + ", expertise: " + condition["expertise"].astype(str)

# a = []
# b = []
# for index, row in condition.iterrows():
#     if row["latency"] == 0 and row["expertise"] == 0 and row["difficulty"] == 0:
#         a.append(row["accuracy"])
#     if row["latency"] == 70 and row["expertise"] == 0 and row["difficulty"] == 0:
#         b.append(row["accuracy"])

# print("low: ", fmean(a), "high: ", fmean(b))

# a = []
# b = []
# for index, row in condition.iterrows():
#     if row["latency"] == 0 and row["expertise"] == 1 and row["difficulty"] == 0:
#         a.append(row["accuracy"])
#     if row["latency"] == 120 and row["expertise"] == 1 and row["difficulty"] == 0:
#         b.append(row["accuracy"])

# print("low: ", fmean(a), "high: ", fmean(b))

# a = []
# b = []
# for index, row in condition.iterrows():
#     if row["latency"] == 0 and row["expertise"] == 0 and row["difficulty"] == 1:
#         a.append(row["accuracy"])
#     if row["latency"] == 120 and row["expertise"] == 0 and row["difficulty"] == 1:
#         b.append(row["accuracy"])

# print("low: ", fmean(a), "high: ", fmean(b))

# a = []
# b = []
# for index, row in condition.iterrows():
#     if row["latency"] == 0 and row["expertise"] == 1 and row["difficulty"] == 1:
#         a.append(row["accuracy"])
#     if row["latency"] == 420 and row["expertise"] == 1 and row["difficulty"] == 1:
#         b.append(row["accuracy"])

# print("low: ", fmean(a), "high: ", fmean(b))

# a = []
# b = []
# for index, row in condition.iterrows():
#     if row["latency"] == 120 and row["expertise"] == 0 and row["difficulty"] == 0:
#         a.append(row["time"])
#     if row["latency"] == 120 and row["expertise"] == 0 and row["difficulty"] == 1:
#         b.append(row["time"])

# print("simple: ", fmean(a), "complex: ", fmean(b))

# a = []
# b = []
# for index, row in condition.iterrows():
#     if row["latency"] == 120 and row["expertise"] == 0 and row["difficulty"] == 0:
#         a.append(row["time"])
#     if row["latency"] == 120 and row["expertise"] == 0 and row["difficulty"] == 1:
#         b.append(row["time"])

# print("simple: ", fmean(a), "complex: ", fmean(b))

# a = []
# b = []
# for index, row in condition.iterrows():
#     if row["latency"] == 0 and row["expertise"] == 0 and row["difficulty"] == 1:
#         a.append(row["time"])
#     if row["latency"] == 120 and row["expertise"] == 0 and row["difficulty"] == 1:
#         b.append(row["time"])

# print("low: ", fmean(a), "high: ", fmean(b))

# a = []
# b = []
# for index, row in condition.iterrows():
#     if row["latency"] == 0 and row["expertise"] == 1 and row["difficulty"] == 1:
#         a.append(row["time"])
#     if row["latency"] == 70 and row["expertise"] == 1 and row["difficulty"] == 1:
#         b.append(row["time"])

# print("low: ", fmean(a), "high: ", fmean(b))

# a = []
# b = []
# for index, row in condition.iterrows():
#     if row["latency"] == 0 and row["expertise"] == 1 and row["difficulty"] == 0:
#         a.append(row["frustration"])
#     if row["latency"] == 420 and row["expertise"] == 1 and row["difficulty"] == 0:
#         b.append(row["frustration"])

# print("small", median(a), "big", median(b))

a = []
b = []
for index, row in condition.iterrows():
    if row["latency"] == 0 and row["expertise"] == 0 and row["difficulty"] == 1:
        a.append(row["frustration"])
    if row["latency"] == 420 and row["expertise"] == 0 and row["difficulty"] == 1:
        b.append(row["frustration"])

print("small", median(a), "big", median(b))

a = []
b = []
for index, row in condition.iterrows():
    if row["latency"] == 0 and row["expertise"] == 1 and row["difficulty"] == 1:
        a.append(row["frustration"])
    if row["latency"] == 420 and row["expertise"] == 1 and row["difficulty"] == 1:
        b.append(row["frustration"])

print("small", median(a), "big", median(b))


# accuracy_results = pg.pairwise_gameshowell(condition, dv="accuracy", between="condition").round(3)
# print("accuracy:\n", accuracy_results.to_string())
# time_results = pg.pairwise_gameshowell(condition, dv="time", between="condition").round(3)
# print("\ntime:\n", time_results.to_string())
frustration_results = pg.pairwise_gameshowell(condition, dv="frustration", between="condition").round(3)
print("\nfrustration:\n", frustration_results.to_string())

