from pandas import read_csv, Series, DataFrame
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import numpy as np

# load data
coordination = read_csv('/home/jonas/Studium/Bachelorarbeit/Analyse/data/coordination.csv', sep=",")
google_forms = read_csv('/home/jonas/Studium/Bachelorarbeit/Analyse/data/google_forms.csv', sep=",")

FFB_Mot = google_forms.copy()

del FFB_Mot["Use_System_Frequently"]
del FFB_Mot["System_Complex"]
del FFB_Mot["System_Easy_Use"]
del FFB_Mot["Need_Tech_Support"]
del FFB_Mot["System_Functions_Integrated"]
del FFB_Mot["System_Inconsistency"]
del FFB_Mot["Learn_System_Quickly"]
del FFB_Mot["System_Cumbersome"]
del FFB_Mot["Confident_Using_System"]
del FFB_Mot["Learn_Before_Use"]
del FFB_Mot["User_ID"]

mapping = {
    "I am not able to carry out this activity": 1,
    "I have big problems carrying out this activity": 2,
    "I have moderate problems carrying out this activity": 3,
    "I have little problems carrying out this activity": 4,
    "I have no problem carrying out this activity": 5
}

FFB_Mot["Stairs_Without_Handrail"] = FFB_Mot["Stairs_Without_Handrail"].replace(mapping)
FFB_Mot["One_Leg_Stand"] = FFB_Mot["One_Leg_Stand"].replace(mapping)
FFB_Mot["Dribble_Ball_Briskly"] = FFB_Mot["Dribble_Ball_Briskly"].replace(mapping)
FFB_Mot["Jump_Over_Fence"] = FFB_Mot["Jump_Over_Fence"].replace(mapping)
FFB_Mot["Ride_Bike_Hands_Free"] = FFB_Mot["Ride_Bike_Hands_Free"].replace(mapping)
FFB_Mot["Cartwheel"] = FFB_Mot["Cartwheel"].replace(mapping)
FFB_Mot["Somersault"] = FFB_Mot["Somersault"].replace(mapping)

def remove_duplicates(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]

del coordination["movement"]
del coordination["time"]

#coordination["accuracy"] = np.log(coordination["accuracy"])

tmp = []

# compute columns wise mean of rows 
ids = remove_duplicates(coordination.id)
for participant_id in ids:
    idx = np.where(participant_id == coordination.id)
    tmp.append(np.mean(coordination.iloc[idx], axis=0))

unique_data = DataFrame(tmp)
unique_data["ffb-mot"] = FFB_Mot.sum(axis=1)
del unique_data["id"]

X = StandardScaler().fit_transform(unique_data)  # Standardize the data

scores = []
max_clusters = 5

for num_clusters in range(2, max_clusters+1):
    kmeans = KMeans(n_clusters=num_clusters)
    kmeans.fit(X)
    scores.append(kmeans.inertia_)
    unique_data["label"] = kmeans.predict(X)
    print(unique_data)

plt.bar(range(2, max_clusters+1), scores)
plt.show()