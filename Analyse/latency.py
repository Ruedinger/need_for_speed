import matplotlib.pyplot as plt
import seaborn as sns
from pandas import DataFrame
sns.set(font_scale=1.2)

df = DataFrame()

measurements = list(map(lambda x: x*10**3, [0.083333333, 0.095833333, 0.066666667, 0.0875, 0.079166667, 0.091666667, 0.070833333, 0.070833333, 0.1, 0.0875, 0.120833333, 0.133333333, 0.129166667, 0.154166667, 0.141666667, 0.116666667, 0.145833333, 0.129166667, 0.125, 0.141666667, 0.195833333, 0.145833333, 0.175, 0.183333333, 0.15, 0.191666667, 0.179166667, 0.183333333, 0.1875, 0.166666667, 0.216666667, 0.229166667, 0.220833333, 0.2375, 0.195833333, 0.208333333, 0.233333333, 0.241666667, 0.225, 0.216666667]))
df["artificial latency (ms)"] = [0]*10 + [40]*10 + [90]*10 + [140]*10
df["ETE latency (ms)"] = measurements

fig = plt.figure(figsize=(6,3))
ax = sns.barplot(df, orient="h", x="ETE latency (ms)", y="artificial latency (ms)", capsize=.2, errorbar=("sd", 1), edgecolor="0", facecolor=(153/255,153/255,1,1), err_kws={"color": "0", "linewidth": 0.5}, linewidth=.5)
ax.bar_label(ax.containers[0], fmt=lambda x: round(x), label_type="center")
plt.xticks(range(0,300,50))
fig.subplots_adjust(top=1, bottom=0.2, right=0.97, left=0.2)
# plt.show()
plt.savefig("frame-counting.pdf")