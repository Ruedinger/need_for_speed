import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
import seaborn as sns
from statistics import fmean, median
rcParams.update({'figure.autolayout': True})
sns.set(font_scale=1.3)

google_forms = pd.read_csv('/home/jonas/Studium/Bachelorarbeit/Analyse/data/google_forms.csv', sep=",")

del google_forms["Stairs_Without_Handrail"]
del google_forms["One_Leg_Stand"]
del google_forms["Dribble_Ball_Briskly"]
del google_forms["Jump_Over_Fence"]
del google_forms["Ride_Bike_Hands_Free"]
del google_forms["Cartwheel"]
del google_forms["Somersault"]
del google_forms["User_ID"]

mapping = {
    "Strongly disagree": 0,
    "Disagree": 1,
    "Neither agree or disagree": 2,
    "Agree": 3,
    "Strongly agree": 4
}

final = google_forms.copy()

final["I think that I would like to use this system frequently."] = google_forms["Use_System_Frequently"].replace(mapping)
final["I found the system unnecessarily complex."] = google_forms["System_Complex"].replace(mapping)
final["I thought the system was easy to use."] = google_forms["System_Easy_Use"].replace(mapping)
final["I think that I would need the support of a technical person to be able to use this system."] = google_forms["Need_Tech_Support"].replace(mapping)
final["I found the various functions in this system were well integrated."] = google_forms["System_Functions_Integrated"].replace(mapping)
final["I thought there was too much inconsistency in this system."] = google_forms["System_Inconsistency"].replace(mapping)
final["I would imagine that most people would learn to use this system very quickly."] = google_forms["Learn_System_Quickly"].replace(mapping)
final["I found the system very cumbersome to use."] = google_forms["System_Cumbersome"].replace(mapping)
final["I felt very confident using the system."] = google_forms["Confident_Using_System"].replace(mapping)
final["I needed to learn a lot of things before\nI could get going with this system."] = google_forms["Learn_Before_Use"].replace(mapping)

final.dropna(inplace=True)

def establish_percentile_method(method, og_percentile):
    def new_percentile(*args, **kwargs):
        kwargs['method'] = method
        return og_percentile(*args, **kwargs)
    np.percentile = new_percentile
    return og_percentile

establish_percentile_method('closest_observation', np.percentile)
fig = plt.figure(figsize=(13,6))
ax = sns.boxplot(final, orient="h", order=["I think that I would like to use this system frequently.", "I thought the system was easy to use.", "I found the various functions in this system were well integrated.", "I would imagine that most people would learn to use this system very quickly.", "I felt very confident using the system.", "I found the system unnecessarily complex.", "I think that I would need the support of a technical person to be able to use this system.", "I thought there was too much inconsistency in this system.", "I found the system very cumbersome to use.", "I needed to learn a lot of things before I could get going with this system."], medianprops={"linewidth": 4})
ax.set_xticks(range(5), ("Strongly\ndisagree", "Disagree", "Neither agree\nor disagree", "Agree", "Strongly\nagree"))
plt.xticks(rotation=60)
#fig.subplots_adjust(left=0.4, right=0.99, top=1, bottom=0.2)
plt.savefig("sus-box.pdf")

final = google_forms.copy()

final["Q1"] = google_forms["Use_System_Frequently"].replace(mapping)
final["Q2"] = google_forms["System_Complex"].replace(mapping)
final["Q3"] = google_forms["System_Easy_Use"].replace(mapping)
final["Q4"] = google_forms["Need_Tech_Support"].replace(mapping)
final["Q5"] = google_forms["System_Functions_Integrated"].replace(mapping)
final["Q6"] = google_forms["System_Inconsistency"].replace(mapping)
final["Q7"] = google_forms["Learn_System_Quickly"].replace(mapping)
final["Q8"] = google_forms["System_Cumbersome"].replace(mapping)
final["Q9"] = google_forms["Confident_Using_System"].replace(mapping)
final["Q10"] = google_forms["Learn_Before_Use"].replace(mapping)

final.dropna(inplace=True)

def establish_percentile_method(method, og_percentile):
    def new_percentile(*args, **kwargs):
        kwargs['method'] = method
        return og_percentile(*args, **kwargs)
    np.percentile = new_percentile
    return og_percentile

establish_percentile_method('closest_observation', np.percentile)
fig = plt.figure(figsize=(8,7))
ax = sns.boxplot(final, orient="h", order=["Q1", "Q3", "Q5", "Q7", "Q9", "Q2", "Q4", "Q6", "Q8", "Q10"], medianprops={"linewidth": 4})
ax.set_xticks(range(5), ("Strongly\ndisagree", "Disagree", "Neither agree\nor disagree", "Agree", "Strongly\nagree"))
plt.xticks(rotation=30)
plt.savefig("sus-box.pdf")

# scores = []
# for index, row in final.iterrows():
#     score = 0
#     score += row["I think that I would like to use this system frequently."]
#     score += row["I thought the system was easy to use."]
#     score += row["I found the various functions\nin this system were well integrated."]
#     score += row["I would imagine that most people\nwould learn to use this system very quickly."]
#     score += row["I felt very confident using the system."]
#     score -= 5
#     score += 5 - row["I found the system unnecessarily complex."]
#     score += 5 - row["I think that I would need the support\nof a technical person to be able to use this system."]
#     score += 5 - row["I thought there was too much\ninconsistency in this system."]
#     score += 5 - row["I found the system very cumbersome to use."]
#     score += 5 - row["I needed to learn a lot of things before\nI could get going with this system."]
#     if score*2.5 > 100:
#         print(row)
#     scores.append(score*2.5)

# print(fmean(scores), median(scores))