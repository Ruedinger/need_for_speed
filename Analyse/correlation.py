import sys
import pandas as pd
from numpy import genfromtxt, mean, set_printoptions, inf, corrcoef
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(font_scale=2)


questionnaire = pd.read_csv('/home/jonas/Studium/Bachelorarbeit/Analyse/data/questionnaire.csv', sep=",")
condition =  pd.read_csv('/home/jonas/Studium/Bachelorarbeit/Analyse/data/condition_w_expertise.csv', sep=",")
coordination = pd.read_csv('/home/jonas/Studium/Bachelorarbeit/Analyse/data/coordination.csv', sep=",")

fig = plt.figure(figsize=(8, 6))
mat = condition.corr(method="spearman").round(2)
sns.heatmap(mat, vmin=-1, vmax=1, annot=True, fmt="0.2f", square=True, cmap="vlag", cbar=False)
plt.yticks(rotation=0)
fig.subplots_adjust(top=1, bottom=0, left=0.2, right=1)
plt.savefig("correlation-expertise.pdf")