import matplotlib.pyplot as plt
import seaborn as sns
from pandas import read_csv
import numpy as np
sns.set(font_scale=3)

# load data
condition =  read_csv('/home/jonas/Studium/Bachelorarbeit/Analyse/data/condition_w_expertise.csv', sep=",")

# plot statistical significant differences in boxplot
# for index, row in condition.iterrows():
#     condition.at[index,'condition'] = "latency: " + str(int(row["latency"] + 83)) + ", difficulty: " + ("simple" if row["difficulty"] == 0 else "complex") + ", expertise: " + ("unskilled" if row["expertise"] == 0 else "skilled")

for index, row in condition.iterrows():
    name = []
    if row["difficulty"] == 0 and row["expertise"] == 0:
        name.append("A")
    elif row["difficulty"] == 0 and row["expertise"] == 1:
        name.append("B")
    elif row["difficulty"] == 1 and row["expertise"] == 0:
        name.append("C")
    elif row["difficulty"] == 1 and row["expertise"] == 1:
        name.append("D")
    match row["latency"]:
        case 0:
            name.append("0")
        case 70:
            name.append("1")
        case 120:
            name.append("2")
        case 420:
            name.append("3")
    condition.at[index,'condition'] = "".join(name)

levels = sorted(list(np.unique(condition["condition"])), key=lambda x: x[1])

colors =np.array([[0, 84, 159], [246, 168, 0], [97, 33, 88], [87, 171, 39]])/255

# time
fig = plt.figure(figsize=(22, 14))
ax = sns.boxplot(data=condition, x="condition", y="time", order=levels, palette=colors)
ax.set_ylabel("time (s)")
plt.tight_layout()

# statistical annotation
def add_annotation(text: str, left_pos: int, right_pos: int, y: float = 21, h: float = 0.5):
    col = 'k'
    plt.plot([left_pos, left_pos, right_pos, right_pos], [y, y+h, y+h, y], lw=1.5, c=col)
    plt.text((left_pos + right_pos)*.5, y+h, text, ha='center', va='bottom', color=col)

add_annotation("<0.001", 0, 12, 30, 1)
add_annotation("<0.001", 1, 13, 34, 1)
add_annotation("0.001", 2, 10, 38, 1)
add_annotation("<0.001", 3, 7, 42, 1)

add_annotation("0.034", 0, 2, 46, 1)
add_annotation("<0.001", 4, 6, 46, 1)
add_annotation("<0.001", 8, 10, 46, 1)

add_annotation("<0.001", 4, 10, 50, 1)

plt.savefig("post-hoc-boxplot-time.pdf")

# accuracy
fig = plt.figure(figsize=(22, 14))
ax = sns.boxplot(data=condition, x="condition", y="accuracy", order=levels, palette=colors)
ax.set_yticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
plt.tight_layout()

# statistical annotation

add_annotation("0.009", 0, 4, 1.05, 0.01)
add_annotation("0.001", 1, 5, 1.1, 0.01)
add_annotation("<0.001", 2, 10, 1.15, 0.01)
add_annotation("0.001", 3, 11, 1.2, 0.01)

add_annotation("0.934", 4, 10, 1.25, 0.01)


plt.savefig("post-hoc-boxplot-accuracy.pdf")

# frustration
def establish_percentile_method(method, og_percentile):
    def new_percentile(*args, **kwargs):
        kwargs['method'] = method
        return og_percentile(*args, **kwargs)
    np.percentile = new_percentile
    return og_percentile

establish_percentile_method('closest_observation', np.percentile)

fig = plt.figure(figsize=(22, 14))
ax = sns.boxplot(data=condition, x="condition", y="frustration", order=levels, palette=colors)
ax.set_yticks(range(5))
ax.set_yticklabels(["No Frustration", "Little Frustration", "Medium Frustration", "High Frustration", "Maximal Frustration"])
plt.tight_layout()

# statistical annotation

#add_annotation("0.021", 0, 12, 4.25, 0.25)
add_annotation("0.001", 1, 13, 4.75, 0.25)
add_annotation("<0.001", 2, 14, 5.25, 0.25)
add_annotation("<0.001", 3, 11, 5.75, 0.25)
#add_annotation("0.021", 8, 9, 6.25, 0.25)

plt.savefig("post-hoc-boxplot-frustration.pdf")