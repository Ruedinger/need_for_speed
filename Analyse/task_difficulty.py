from pandas import read_csv
from statistics import median
from scipy.stats import mannwhitneyu, ttest_ind, wilcoxon
from numpy import array

condition = read_csv('/home/jonas/Studium/Bachelorarbeit/Analyse/data/condition.csv', sep=",")

x = []
y = []

for index, row in condition.iterrows():
    name = []
    if row["difficulty"] == 0 and row["latency"] == 70:
        x.append(row["time"])
    if row["difficulty"] == 1 and row["latency"] == 70:
        y.append(row["time"])

x = condition[condition["difficulty"] == 0]
y = condition[condition["difficulty"] == 1]
print(x.shape)

print(wilcoxon(x["time"], y["time"], alternative="less", method="auto"))