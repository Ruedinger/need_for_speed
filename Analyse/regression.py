import pandas as pd
import sys
import numpy as np
from sklearn.linear_model import LinearRegression
from scipy.stats import pearsonr, spearmanr
import scipy.stats as stats
import statsmodels.api as sm
import statsmodels.formula.api as smf
import statsmodels
from statsmodels.graphics.regressionplots import plot_leverage_resid2
import matplotlib.pyplot as plt

np.set_printoptions(linewidth=np.inf, threshold=sys.maxsize)

# load data
condition =  pd.read_csv('/home/jonas/Studium/Bachelorarbeit/Analyse/data/condition_w_expertise.csv', sep=",")
numpy_data = condition.to_numpy()
independent_variables = numpy_data[:, 0:3]
dependent_variables = numpy_data[:, 3:5]

# test for linear relationship
print("pearson")
print(pearsonr(independent_variables[:,0], dependent_variables[:,0]))
print(pearsonr(independent_variables[:,1], dependent_variables[:,0]))
print(pearsonr(independent_variables[:,2], dependent_variables[:,0]))
print(pearsonr(independent_variables[:,0], dependent_variables[:,1]))
print(pearsonr(independent_variables[:,1], dependent_variables[:,1]))
print(pearsonr(independent_variables[:,2], dependent_variables[:,1]))


# if no linear relationship, test spearman
print("spearman")
print(spearmanr(independent_variables[:,0], dependent_variables[:,0]))
print(spearmanr(independent_variables[:,1], dependent_variables[:,0]))
print(spearmanr(independent_variables[:,2], dependent_variables[:,0]))
print(spearmanr(independent_variables[:,0], np.exp(dependent_variables[:,1]**2)))
print(spearmanr(independent_variables[:,1], np.exp(dependent_variables[:,1]**2)))
print(spearmanr(independent_variables[:,2], np.exp(dependent_variables[:,1]**2)))

# fit model
# adjust according to findings above
# this also includes an ANOVA, but the requirements are not completely met, as shown below

model1 = smf.ols(formula="time ~ latency + difficulty + expertise", data=condition)
results1 = model1.fit()
print(results1.summary())

model2 = smf.ols(formula="accuracy ~ latency + difficulty + expertise", data=condition)
results2 = model2.fit()
print(results2.summary())

# visualization
# plt.figure(figsize=(10, 8))
# ax = plt.axes(projection="3d")
# fig = ax.scatter3D(condition["latency"], condition["difficulty"], condition["time"], alpha=0.5)
# ax.set_xlabel("latency")
# ax.set_ylabel("difficulty")
# ax.set_zlabel("time")
# x_surf = np.arange(0, 420, 1)
# y_surf = np.arange(0, 1.1, 0.1)
# X, Y = np.meshgrid(x_surf, y_surf)
# exog = pd.DataFrame({"latency": X.ravel(), "difficulty": Y.ravel()})
# Z = results1.predict(exog = exog).values.reshape(X.shape)
# ax.plot_surface(X, Y, Z, rstride=1, cstride=1, alpha = 0.2)
# plt.show()

# plt.figure(figsize=(10, 8))
# ax = plt.axes(projection="3d")
# fig = ax.scatter3D(condition["latency"], condition["difficulty"], condition["accuracy"], alpha=0.5)
# ax.set_xlabel("latency")
# ax.set_ylabel("difficulty")
# ax.set_zlabel("accuracy")
# x_surf = np.arange(0, 420, 1)
# y_surf = np.arange(0, 1.1, 0.1)
# X, Y = np.meshgrid(x_surf, y_surf)
# exog = pd.DataFrame({"latency": X.ravel(), "difficulty": Y.ravel()})
# Z = results2.predict(exog = exog).values.reshape(X.shape)
# ax.plot_surface(X, Y, Z, rstride=1, cstride=1, alpha = 0.2)
# plt.show()

# test if residual are normally distributed
# residual are normally distributed according to Jarque-Bera test in summary
plt.figure(figsize=(9,9))
stats.probplot(results1.resid, dist="norm", plot=plt)
plt.show()
plt.figure(figsize=(9,9))
stats.probplot(results2.resid, dist="norm", plot=plt)
plt.xlabel("Theoretical quantiles", fontsize=15)
plt.ylabel("Ordered Values", fontsize=15)
plt.xticks(range(-3,4),fontsize=15)
plt.yticks([-0.4,-0.2,0,0.2,0.4],fontsize=15)
plt.title("Quantile-Quantile Plot", fontsize=17)
plt.savefig("qq-plot-accuracy.pdf")

# test heteroskedasticity
# we have homoscedasticity with these p-values
print(statsmodels.stats.diagnostic.het_breuschpagan(results1.resid, results1.model.exog))
print(statsmodels.stats.diagnostic.het_breuschpagan(results2.resid, results2.model.exog))

# plot leverage of data points
# individual data points should not have a too high leverage
# looks okay-ish
fig = sm.graphics.influence_plot(results1, criterion="cooks")
fig.tight_layout(pad=1.0)
plt.show()

fig = sm.graphics.influence_plot(results2, criterion="cooks")
fig.tight_layout(pad=1.0)
plt.show()

# test multicollinearity
# no multicollinearity because correlation of independent variables is 0
# also confirmed by regression without polynomial terms
print("Condition number:", np.linalg.cond(results1.model.exog))
print("Condition number:", np.linalg.cond(results2.model.exog))

# test for autocorrelation
# want that h0 cannot be discarded
print(statsmodels.stats.diagnostic.acorr_lm(resid=results1.resid))
print(statsmodels.stats.diagnostic.acorr_lm(resid=results2.resid))
