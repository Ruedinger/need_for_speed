import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.tree import DecisionTreeClassifier, plot_tree, export_graphviz
import sys
np.set_printoptions(threshold=sys.maxsize,linewidth=np.nan)

# load data
condition =  pd.read_csv('/home/jonas/Studium/Bachelorarbeit/Analyse/data/condition_w_expertise.csv', sep=",")
# adapt data to journal paper
condition = condition[condition.latency != 70]

condition["latency"] += 83
print(condition.quantile(1/3),"\n"*3,condition.quantile(2/3))

for index, row in condition.iterrows():
    if row["time"] <= 4.966522:
        condition.at[index,"time_range"] = 0
    elif row["time"] <= 8.614443:
        condition.at[index,"time_range"] = 1
    else:
        condition.at[index,"time_range"] = 2

    if row["accuracy"] <= 0.893335:
        condition.at[index,"accuracy_range"] = 0
    elif row["accuracy"] <= 0.988733:
        condition.at[index,"accuracy_range"] = 1
    else:
        condition.at[index,"accuracy_range"] = 2

numpy_data = condition.to_numpy()
independent_variables = numpy_data[:, 0].reshape(-1, 1)
dependent_variables = numpy_data[:, 5:8]

# Fit regression model
regr_3 = DecisionTreeClassifier(max_depth=4, criterion="entropy", min_samples_leaf=5)
regr_3.fit(independent_variables, dependent_variables)

# visualize tree
plot_tree(regr_3, filled=True, feature_names=["latency"],  class_names=["time", "accuracy", "frustration"])
plt.show()

leaf_indices = regr_3.apply(independent_variables)
    
# Group samples by leaf index
leaf_samples = {}
for leaf in np.unique(leaf_indices):
    samples_in_leaf = np.where(leaf_indices == leaf)[0]
    leaf_samples[leaf] = samples_in_leaf
    print(f"Leaf {leaf} has {len(samples_in_leaf)} samples:\nmedian:\n{condition.iloc[samples_in_leaf].median()}\n")

# Predict
# X_test = np.array([420, 1, 0]).reshape(1, -1)
# y_3 = regr_3.predict(X_test)
# print(y_3)