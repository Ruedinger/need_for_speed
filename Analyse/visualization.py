import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
import seaborn as sns
rcParams.update({'figure.autolayout': True})
sns.set(font_scale=1.6)

questionnaire = pd.read_csv('/home/jonas/Studium/Bachelorarbeit/Analyse/data/questionnaire.csv', sep=",")
condition = pd.read_csv('/home/jonas/Studium/Bachelorarbeit/Analyse/data/condition_w_expertise.csv', sep=",")
google_forms = pd.read_csv('/home/jonas/Studium/Bachelorarbeit/Analyse/data/google_forms.csv', sep=",")

colors =np.array([[0, 84, 159], [246, 168, 0], [97, 33, 88], [87, 171, 39]])/255

def establish_percentile_method(method, og_percentile):
    def new_percentile(*args, **kwargs):
        kwargs['method'] = method
        return og_percentile(*args, **kwargs)
    np.percentile = new_percentile
    return og_percentile

establish_percentile_method('closest_observation', np.percentile)

def remove_duplicates(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]

# give categorical variables the right labels
new_difficulty = pd.Categorical(condition.difficulty)
new_difficulty = new_difficulty.rename_categories(["Simple", "Complex"])
condition.difficulty = new_difficulty

new_skill = pd.Categorical(condition.expertise)
new_skill = new_skill.rename_categories(["Unskilled", "Skilled"])
condition.expertise = new_skill

new_frustration = pd.Categorical(condition.frustration)
new_frustration = new_frustration.rename_categories(["No Frustration", "Little Frustration", "Medium Frustration", "High Frustration", "Maximal Frustration"])
condition.frustration = new_frustration

# partition condition data based on skill
skilled_participants = condition.loc[condition['expertise'] == "Skilled"]
unskilled_participants = condition.loc[condition['expertise'] == "Unskilled"]

# partition condition data based on difficulty
unskilled_participants_simple = unskilled_participants.loc[condition['difficulty'] == "Simple"]
unskilled_participants_complex = unskilled_participants.loc[condition['difficulty'] == "Complex"]

skilled_participants_simple = skilled_participants.loc[condition['difficulty'] == "Simple"]
skilled_participants_complex = skilled_participants.loc[condition['difficulty'] == "Complex"]

# plot time
fig, axes = plt.subplots(2, 2, sharex="all", sharey="all")
fig.tight_layout()
fig.set_figheight(12)
fig.set_figwidth(12)
sns.boxplot(unskilled_participants_simple, y="time", x="latency", hue="latency", ax=axes[0,0], legend=False, medianprops={"linewidth": 4}, palette=colors)
sns.boxplot(unskilled_participants_complex, y="time", x="latency", hue="latency", ax=axes[0,1], legend=False, medianprops={"linewidth": 4}, palette=colors)
sns.boxplot(skilled_participants_simple, y="time", x="latency", hue="latency", ax=axes[1,0], legend=False, medianprops={"linewidth": 4}, palette=colors)
sns.boxplot(skilled_participants_complex, y="time", x="latency", hue="latency", ax=axes[1,1], legend=False, medianprops={"linewidth": 4}, palette=colors)
axes[0,0].set_title("Unskilled, Simple")
axes[0,0].set_ylabel("time (s)")
axes[0,1].set_title("Unskilled, Complex")
axes[1,0].set_title("Skilled, Simple")
axes[1,0].set_ylabel("time (s)")
axes[1,0].set_xticklabels([83, 153, 203, 503])
axes[1,0].set_xlabel("latency (ms)")
axes[1,1].set_title("Skilled, Complex")
axes[1,1].set_xticklabels([83, 153, 203, 503])
axes[1,1].set_xlabel("latency (ms)")
fig.subplots_adjust(top=0.9)

#fig.suptitle("Grouped Time vs Latency")

for ax in axes.flat:
    ax.label_outer()

plt.subplots_adjust(wspace=0.1, hspace=0.1)

plt.savefig("grouped-boxplots-time.pdf")

# plot accuracy
fig, axes = plt.subplots(2, 2, sharex="all", sharey="all")
fig.tight_layout()
fig.set_figheight(12)
fig.set_figwidth(12)
sns.boxplot(unskilled_participants_simple, y="accuracy", x="latency", hue="latency", ax=axes[0,0], legend=False, medianprops={"linewidth": 4}, palette=colors)
sns.boxplot(unskilled_participants_complex, y="accuracy", x="latency", hue="latency", ax=axes[0,1], legend=False, medianprops={"linewidth": 4}, palette=colors)
sns.boxplot(skilled_participants_simple, y="accuracy", x="latency", hue="latency", ax=axes[1,0], legend=False, medianprops={"linewidth": 4}, palette=colors)
sns.boxplot(skilled_participants_complex, y="accuracy", x="latency", hue="latency", ax=axes[1,1], legend=False, medianprops={"linewidth": 4}, palette=colors)
axes[0,0].set_title("Unskilled, Simple")
axes[0,1].set_title("Unskilled, Complex")
axes[1,0].set_title("Skilled, Simple")
axes[1,0].set_xticklabels([83, 153, 203, 503])
axes[1,0].set_xlabel("latency (ms)")
axes[1,1].set_title("Skilled, Complex")
axes[1,1].set_xticklabels([83, 153, 203, 503])
axes[1,1].set_xlabel("latency (ms)")
fig.subplots_adjust(top=0.9)

#fig.suptitle("Grouped Accuracy vs Latency")

for ax in axes.flat:
    ax.label_outer()

plt.subplots_adjust(wspace=0.1, hspace=0.1)

plt.savefig("grouped-boxplots-accuracy.pdf")

# add independent variables to questionnaire
latencies = list(condition.latency[0:80:5])
difficulties = list(condition.difficulty[0:80:5])
expertise = list(condition.expertise[::5])
N = len(questionnaire)
questionnaire['latency'] = pd.Series(np.tile(latencies, N//len(latencies))).iloc[:N]
questionnaire['difficulty'] = pd.Series(np.tile(difficulties, N//len(difficulties))).iloc[:N]
questionnaire['expertise'] = expertise

# partition questionnaire data based on skill
skilled_participants = questionnaire.loc[questionnaire['expertise'] == "Skilled"]
unskilled_participants = questionnaire.loc[questionnaire['expertise'] == "Unskilled"]

# partition questionnaire data based on difficulty
unskilled_participants_simple = unskilled_participants.loc[questionnaire['difficulty'] == "Simple"]
unskilled_participants_complex = unskilled_participants.loc[questionnaire['difficulty'] == "Complex"]

skilled_participants_simple = skilled_participants.loc[questionnaire['difficulty'] == "Simple"]
skilled_participants_complex = skilled_participants.loc[questionnaire['difficulty'] == "Complex"]

# plot frustration
fig, axes = plt.subplots(2, 2, sharex="all", sharey="all")
fig.tight_layout()
fig.set_figheight(12)
fig.set_figwidth(13)
sns.boxplot(unskilled_participants_simple, y="frustration", x="latency", hue="latency", ax=axes[0,0], legend=False, medianprops={"linewidth": 4}, palette=colors)
sns.boxplot(unskilled_participants_complex, y="frustration", x="latency", hue="latency", ax=axes[0,1], legend=False, medianprops={"linewidth": 4}, palette=colors)
sns.boxplot(skilled_participants_simple, y="frustration", x="latency", hue="latency", ax=axes[1,0], legend=False, medianprops={"linewidth": 4}, palette=colors)
sns.boxplot(skilled_participants_complex, y="frustration", x="latency", hue="latency", ax=axes[1,1], legend=False, medianprops={"linewidth": 4}, palette=colors)
axes[0,0].set_title("Unskilled, Simple")
axes[0,0].set_yticks(range(5))
axes[0,0].set_yticklabels(["No Frustration", "Little Frustration", "Medium Frustration", "High Frustration", "Maximal Frustration"])
axes[0,1].set_title("Unskilled, Complex")
axes[1,0].set_title("Skilled, Simple")
axes[1,0].set_xticklabels([83, 153, 203, 503])
axes[1,0].set_xlabel("latency (ms)")
axes[1,0].set_yticks(range(5))
axes[1,0].set_yticklabels(["No Frustration", "Little Frustration", "Medium Frustration", "High Frustration", "Maximal Frustration"])
axes[1,1].set_title("Skilled, Complex")
axes[1,1].set_xticklabels([83, 153, 203, 503])
axes[1,1].set_xlabel("latency (ms)")
fig.subplots_adjust(top=0.9, left=0.23)

#fig.suptitle("Grouped Frustration vs Latency")

for ax in axes.flat:
    ax.label_outer()

plt.subplots_adjust(wspace=0.1, hspace=0.1)

plt.savefig("grouped-boxplots-frustration.pdf")