from pandas import read_csv, Series, DataFrame
from sklearn.decomposition import FactorAnalysis
from sklearn.preprocessing import StandardScaler
from factor_analyzer.factor_analyzer import calculate_kmo
from factor_analyzer import FactorAnalyzer
from scipy.optimize import minimize
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
sns.set(font_scale=2)

# load data
condition = read_csv('/home/jonas/Studium/Bachelorarbeit/Analyse/data/condition.csv', sep=",")
coordination = read_csv('/home/jonas/Studium/Bachelorarbeit/Analyse/data/coordination.csv', sep=",")
in_session_questionnaire = read_csv('/home/jonas/Studium/Bachelorarbeit/Analyse/data/questionnaire.csv', sep=",")
google_forms = read_csv('/home/jonas/Studium/Bachelorarbeit/Analyse/data/google_forms.csv', sep=",")

FFB_Mot = google_forms.copy()

del FFB_Mot["Use_System_Frequently"]
del FFB_Mot["System_Complex"]
del FFB_Mot["System_Easy_Use"]
del FFB_Mot["Need_Tech_Support"]
del FFB_Mot["System_Functions_Integrated"]
del FFB_Mot["System_Inconsistency"]
del FFB_Mot["Learn_System_Quickly"]
del FFB_Mot["System_Cumbersome"]
del FFB_Mot["Confident_Using_System"]
del FFB_Mot["Learn_Before_Use"]
del FFB_Mot["User_ID"]

mapping = {
    "I am not able to carry out this activity": 1,
    "I have big problems carrying out this activity": 2,
    "I have moderate problems carrying out this activity": 3,
    "I have little problems carrying out this activity": 4,
    "I have no problem carrying out this activity": 5
}

FFB_Mot["Stairs_Without_Handrail"] = FFB_Mot["Stairs_Without_Handrail"].replace(mapping)
FFB_Mot["One_Leg_Stand"] = FFB_Mot["One_Leg_Stand"].replace(mapping)
FFB_Mot["Dribble_Ball_Briskly"] = FFB_Mot["Dribble_Ball_Briskly"].replace(mapping)
FFB_Mot["Jump_Over_Fence"] = FFB_Mot["Jump_Over_Fence"].replace(mapping)
FFB_Mot["Ride_Bike_Hands_Free"] = FFB_Mot["Ride_Bike_Hands_Free"].replace(mapping)
FFB_Mot["Cartwheel"] = FFB_Mot["Cartwheel"].replace(mapping)
FFB_Mot["Somersault"] = FFB_Mot["Somersault"].replace(mapping)

def remove_duplicates(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]

del coordination["movement"]
del coordination["time"]

tmp = []

# compute columns wise mean of rows 
ids = remove_duplicates(coordination.id)
for participant_id in ids:
    idx = np.where(participant_id == coordination.id)
    tmp.append(np.mean(coordination.iloc[idx], axis=0))

unique_data = DataFrame(tmp)
unique_data["ffb-mot"] = FFB_Mot.sum(axis=1)

unique_data_id = unique_data.copy()

del unique_data["id"]

X = StandardScaler().fit_transform(unique_data)  # Standardize the data
print(X.shape)

print("\nKMO: ", calculate_kmo(X),"\n")

factors = 2
#  a list of 2 tuples containing titles for and instances of or class
# fas = [
#     ("FA no rotation", FactorAnalysis(n_components = factors)),
#     ("FA varimax", FactorAnalysis(n_components = factors, rotation="varimax")),
#     ("FA quartimax", FactorAnalysis(n_components=factors, rotation="quartimax"))
# ]  

# #  Let's prepare some plots on one canvas (subplots)
# fig, axes = plt.subplots(ncols=len(fas), figsize=(14, 9))


# # And loop over the variants of our analysis `fas`, zipped with the 
# # plot axes `axes`
# for ax, (title, fa) in zip(axes, fas):
#     #  Fit the model to the standardized data
#     fa = fa.fit(X)
#     #  and transpose the component (loading) matrix
#     factor_matrix = fa.components_.T
#     #  Plot the data as a heat map
#     im = ax.imshow(factor_matrix, cmap="RdBu_r", vmax=1, vmin=-1)
#     #  and add the corresponding value to the center of each cell
#     for (i,j), z in np.ndenumerate(factor_matrix):
#         ax.text(j, i, str(z.round(2)), ha="center", va="center")
#     #  Tell matplotlib about the metadata of the plot
#     ax.set_yticks(np.arange(len(unique_data.columns)))
#     if ax.get_subplotspec().is_first_col():
#         ax.set_yticklabels(unique_data.columns)
#     else:
#         ax.set_yticklabels([])
#     ax.set_title(title)
#     ax.set_xticks(range(factors))
#     ax.set_xticklabels([f"Factor {i}" for i in range(factors)])
#     #  and squeeze the axes tight, to save space
#     plt.tight_layout()
    
# #  and add a colorbar
# cb = fig.colorbar(im, ax=axes, location='right', label="loadings")
# #  show us the plot
# #plt.show()

# fig, axes = plt.subplots(ncols=len(fas), figsize=(14, 9))

# for ax, (title, fa) in zip(axes, fas):
#     communality = Series(np.square(fa.components_.T).sum(axis=1), index=unique_data.columns)
#     communality.plot(
#         kind="bar",
#         ylabel="communality",
#         ax=ax,
#     )
#     ax.set_title(title)
# #plt.show()

# fig, axes = plt.subplots(ncols=len(fas), figsize=(14, 9))

# for ax, (title, fa) in zip(axes, fas):
#     uniqueness = Series(fa.noise_variance_, index=unique_data.columns)

#     lambda_ = fa.components_
#     psi = np.diag(uniqueness)
#     s = np.corrcoef(np.transpose(X))
#     sigma = np.matmul(lambda_.T, lambda_) + psi
#     residuals = (s - sigma)

#     im = ax.imshow(residuals, cmap="RdBu_r", vmin=-1, vmax=1, aspect="equal")
#     ax.tick_params(axis="x", bottom=False, labelbottom=False, top=True, labeltop=True)
#     ax.set_xticks(range(len(unique_data.columns)))
#     ax.set_xticklabels(unique_data.columns)
#     ax.set_yticks(range(len(unique_data.columns)))
#     ax.set_yticklabels(unique_data.columns)
#     ax.set_title(title)
#     for (i,j), z in np.ndenumerate(residuals):
#         ax.text(j, i, str(z.round(3)), ha="center", va="center")
#     plt.tight_layout()

# cb = fig.colorbar(im, ax=axes, location='right', label="residual magnitude")

# #plt.show()

################################################################################


fa = FactorAnalysis(n_components=2, rotation="varimax")

fa = fa.fit(X)


#  Plot the data as a heat map
mat = DataFrame(fa.components_, ["Factor 1", "Factor 2"], unique_data.columns)

fig = plt.figure(figsize=(10,5.5))
ax = sns.heatmap(mat, vmin=-1, vmax=1, annot=True, fmt="0.2f", square=True, cmap="vlag", cbar_kws={'label': "loading"})
plt.xticks(rotation=45)
plt.yticks(rotation=0)
plt.yticks([0.5,1.5], ["postural control", "task proficiency"])
fig.subplots_adjust(top=0.95, bottom=0.05, left=0.28, right=0.92)
plt.savefig("loadings-thesis.pdf")


fig = plt.figure(figsize=(10, 5))
communality = Series(np.square(fa.components_.T).sum(axis=1), index=unique_data.columns)
sns.barplot(communality, orient="h")
plt.xlabel("communality")
plt.ylabel("observed variables")
fig.subplots_adjust(left=0.2, right=1, top=1, bottom=0.15)
plt.savefig("communality.pdf")

Y = fa.fit_transform(X)
# a = 1
skilled_indices_postural_control = np.nonzero(Y[:, 0] >= np.median(Y[:, 0]))[0]
unskilled_indices_postural_control = np.nonzero(Y[:, 0] < np.median(Y[:, 0]))[0]
skilled_indices_task_proficiency = np.nonzero(Y[:, 1] >= np.median(Y[:, 1]))[0]
unskilled_indices_task_proficiency = np.nonzero(Y[:, 1] < np.median(Y[:, 1]))[0]
# skilled_indices_overall = np.nonzero(a*Y[:, 0] + (1-a)*Y[:, 1] >= np.median(a*Y[:, 0] + (1-a)*Y[:, 1]))[0]
# unskilled_indices_overall = np.nonzero(a*Y[:, 0] + (1-a)*Y[:, 1] < np.median(a*Y[:, 0] + (1-a)*Y[:, 1]))[0]

# def to_minimize(a: float):
#     skilled_indices = np.nonzero(a*Y[:, 0] + (1-a)*Y[:, 1] >= np.median(a*Y[:, 0] + (1-a)*Y[:, 1]))[0]
#     unskilled_indices = np.nonzero(a*Y[:, 0] + (1-a)*Y[:, 1] < np.median(a*Y[:, 0] + (1-a)*Y[:, 1]))[0]

#     return (abs(X[skilled_indices].mean(axis=0) - X[unskilled_indices].mean(axis=0))).mean()

# tmp = DataFrame()
# tmp["weight"] = np.linspace(0,1,10000)
# tmp["average variance"] = list(map(to_minimize, tmp["weight"])) #evaluate f for each point in xvals 

# fig = plt.figure(figsize=(10, 5))
# ax = sns.lineplot(tmp, x="weight", y="average variance")
# ax.set_xticks([0,0.2,0.4,0.5,0.6,0.8,1])
# fig.subplots_adjust(right=1, bottom=0.2, left=0.15, top=1)
# plt.show()

unique_data.loc[skilled_indices_postural_control, "expertise_postural_control"] = 1
unique_data.loc[unskilled_indices_postural_control, "expertise_postural_control"] = 0
unique_data.loc[skilled_indices_task_proficiency, "expertise_task_proficiency"] = 1
unique_data.loc[unskilled_indices_task_proficiency, "expertise_task_proficiency"] = 0

fig, axes = plt.subplots(2,5)
#fig.suptitle("User Expertise Comparison")
fig.set_figheight(8)
fig.set_figwidth(15)
#fig.tight_layout()

axes[0,2].set_title("Expertise Grouping with Postural Control")
sns.barplot(unique_data, y="accuracy", hue="expertise_postural_control", ax=axes[0, 0], legend=None, capsize=.2, errorbar=("sd", 1))
#axes[0].set_ylabel("accuracy", fontsize=25)
sns.barplot(unique_data, y="head bend", hue="expertise_postural_control", ax=axes[0, 1], legend=None, capsize=.2, errorbar=("sd", 1))
#axes[1].set_ylabel("head bend", fontsize=25)
axes[0, 1].set_yticks(range(0,26,4))
sns.barplot(unique_data, y="torso bend", hue="expertise_postural_control", ax=axes[0, 2], legend=None, capsize=.2, errorbar=("sd", 1))
#axes[2].set_ylabel("torso bend", fontsize=25)
sns.barplot(unique_data, y="completions", hue="expertise_postural_control", ax=axes[0, 3], legend=None, capsize=.2, errorbar=("sd", 1))
#axes[3].set_ylabel("completions", fontsize=25)
sns.barplot(unique_data, y="ffb-mot", hue="expertise_postural_control", ax=axes[0, 4], legend=None, capsize=.2, errorbar=("sd", 1))
#axes[4].set_ylabel("ffb-mot", fontsize=25)

axes[1,2].set_title("Expertise Grouping with Task Proficiency")
sns.barplot(unique_data, y="accuracy", hue="expertise_task_proficiency", ax=axes[1, 0], legend=None, capsize=.2, errorbar=("sd", 1))
#axes[0].set_ylabel("accuracy", fontsize=25)
sns.barplot(unique_data, y="head bend", hue="expertise_task_proficiency", ax=axes[1, 1], legend=None, capsize=.2, errorbar=("sd", 1))
#axes[1].set_ylabel("head bend", fontsize=25)
axes[1, 1].set_yticks(range(0,24,4))
sns.barplot(unique_data, y="torso bend", hue="expertise_task_proficiency", ax=axes[1, 2], legend=None, capsize=.2, errorbar=("sd", 1))
#axes[2].set_ylabel("torso bend", fontsize=25)
sns.barplot(unique_data, y="completions", hue="expertise_task_proficiency", ax=axes[1, 3], legend=None, capsize=.2, errorbar=("sd", 1))
#axes[3].set_ylabel("completions", fontsize=25)
sns.barplot(unique_data, y="ffb-mot", hue="expertise_task_proficiency", ax=axes[1, 4], legend=None, capsize=.2, errorbar=("sd", 1))
#axes[4].set_ylabel("ffb-mot", fontsize=25)

fig.subplots_adjust(top=0.95, bottom=0.05, left=0.09, right=1, wspace=0.5)
plt.savefig("expertise-grouping.pdf")

skilled_ids = []
unskilled_ids = []

for index in skilled_indices_postural_control:
    skilled_ids.append(ids[index])

for index in unskilled_indices_postural_control:
    unskilled_ids.append(ids[index])

important_data = DataFrame()
#important_data["id"] = condition["id"]
important_data["latency"] = condition["latency"]
important_data["difficulty"] = condition["difficulty"]
important_data.loc[condition['id'].isin(skilled_ids), "expertise"] = 1
important_data.loc[condition['id'].isin(unskilled_ids), "expertise"] = 0
important_data["time"] = condition["time"]
important_data["accuracy"] = condition["accuracy"]

multiplied = []

for answer in in_session_questionnaire["frustration"]:
    multiplied += [answer]*5

important_data["frustration"] = multiplied
important_data.to_csv('/home/jonas/Studium/Bachelorarbeit/Analyse/data/condition_w_expertise.csv', index=False)
