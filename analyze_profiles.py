from pathlib import Path
from pstats import Stats
import pickle
from statistics import fmean, variance, quantiles, StatisticsError

directory_str = "profiles/"

directory = Path(directory_str)
    
for file in directory.iterdir():
    if file.as_posix().endswith("profile"):
        try:
            stats = Stats(file.as_posix())
        except TypeError:
            pass
        else:
            stats.sort_stats('tottime')
            print(str(file))
            stats.print_stats(0.1)
    elif file.as_posix().endswith("times"):
        with open(file, 'rb') as fd:
            profile = pickle.load(fd)
            print(str(file))
            try:
                print("mean: ", fmean(profile), "variance: ", variance(profile), "quartiles: ", quantiles(profile))
            except StatisticsError:
                pass