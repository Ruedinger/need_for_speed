from functools import partial
import numpy as np
import matplotlib.pyplot as plt

max_dist = np.sqrt(1080**2 + 1920**2)
total_time = 30
error_time = [10, 20, 30]
delta_time = [0.1]*300
distance = [[50]*15+[0]*15, [50]*7+[0]*8, [50]*5+[0]*5]

def gen_dist(mag):
    return [mag]*150+[0]*150

width = [40, 60, 80]

def sig(x):
    return 1/(1+np.exp(-x))

def tanh(x):
    return 2*sig(x) -1

def exp_utility(x, c):
    if c == 0:
        return x/(max_dist)
    return (1 - np.exp(-c*x/(max_dist)))/(1 - np.exp(-c))

def err1(tt, et):
    return 1 - et/tt

def err2(tt, dt, dist, w, f):
    res = 0
    for dt1, d in zip(dt, dist):
        res += f(d-w)*dt1
    return res/tt

print("simple error without distance")
for e in error_time:
    print(err1(total_time, e))
        
# polling frequency not important
print("make errors half the time")
print("complex error with exp utility")
for m in range(0, int(max_dist), 100):
    for w in width:
        print("error magnitude: ", m, " width: ", w)
        distance2 = gen_dist(m)
        print(err2(total_time, delta_time, distance2, w, partial(exp_utility, c=1)))

print("\n\nmake errors all the time")
print("complex error with exp utility")
for m in range(0, int(max_dist), 100):
    for w in width:
        print("error magnitude: ", m, " width: ", w)
        distance2 = [m]*300
        print(err2(total_time, delta_time, distance2, w, partial(exp_utility, c=3)))

x = np.linspace(0, int(max_dist), int(max_dist)) 
plt.plot(x, exp_utility(x, -11)) 
plt.show() 
